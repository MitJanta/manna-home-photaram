<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your donation. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache & Route is cleared";
});

// ------------------------ Front-End -------------------------------------
Route::get('/', 'frontend\Home@index')->name('index');
Route::get('/home', 'frontend\Home@index')->name('home');

Route::get('/about', 'frontend\About@index')->name('about');

Route::get('/news_acts/{page_num}', 'frontend\NewsActs@index')->name('news_acts');
Route::get('/news_acts/detail/{page_num}/{id}', 'frontend\NewsActs@detail')->name('news_acts_detail');
Route::get('/news_acts/detail/{id}/gallery', 'frontend\NewsActs@gallery')->name('news_acts_gallery');

Route::get('/contact', 'frontend\Contact@index')->name('contact');

Route::get('/donation', 'frontend\Donation@index')->name('donation');

Route::group(['middleware'=>['XSS']], function () { // ดัก script ใน Form
    Route::post('/contact_submit', 'frontend\Contact@contactSubmit')->name('contact_submit');
    Route::post('/donation_submit', 'frontend\Donation@donationSubmit')->name('donation_submit');
});

// ------------------------ .end Front-End -------------------------------------

// ------------------------ Back-End -------------------------------------
//Auth Routes
Auth::routes();
Route::prefix('/@backend')->name('auth.')->namespace('Admin\Auth')->group(function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login.auth');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

// Admin manage route
Route::prefix('/@backend')->name('admin.')->namespace('Admin')->middleware('auth:admin')->group(function () {
    Route::get('/reset_password', 'ResetPasswordController@showResetPasswordForm')->name('reset_password');
    Route::post('/reset_password/update', 'ResetPasswordController@update')->name('reset_password.update');
    // Super Admin only
    Route::get('/admin_manage', 'ManageAdminsController@index')->name('admin_manage');
    Route::get('/admin_manage/create_form', 'ManageAdminsController@createForm')->name('admin_manage.create_form');
    Route::post('/admin_manage/create', 'ManageAdminsController@create')->name('admin_manage.create');
    Route::get('/admin_manage/edit_form/{id}', 'ManageAdminsController@editForm')->name('admin_manage.edit_form');
    Route::post('/admin_manage/edit', 'ManageAdminsController@edit')->name('admin_manage.edit');
    Route::get('/admin_manage/logging/{id}', 'LogsController@index')->name('admin_manage.logging');
});

Route::prefix('/@backend')->name('backend.')->namespace('backend')->middleware('auth:admin')->group(function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    // Home route
    Route::get('/home/banner_edit_form', 'HomeController@bannerEditForm')->name('home_banner_edit_form');
    Route::post('/home/banner_edit', 'HomeController@bannerEdit')->name('home_banner_edit');
    Route::get('/home/about_edit_form', 'HomeController@aboutEditForm')->name('home_about_edit_form');
    Route::post('/home/about_edit', 'HomeController@aboutEdit')->name('home_about_edit');
    Route::get('/home/help_edit_form', 'HomeController@helpEditForm')->name('home_help_edit_form');
    Route::post('/home/help_edit', 'HomeController@helpEdit')->name('home_help_edit');
    Route::get('/home/divider_edit_form', 'HomeController@dividerEditForm')->name('home_divider_edit_form');
    Route::post('/home/divider_edit', 'HomeController@dividerEdit')->name('home_divider_edit');
    // About us route
    Route::get('/about_us/banner_edit_form', 'AboutController@aboutBannerEditForm')->name('about_banner_edit_form');
    Route::post('/about_us/banner_edit', 'AboutController@aboutBannerEdit')->name('about_banner_edit');
    Route::get('/about_us/who_we_are_edit_form', 'AboutController@aboutWhoWeAreEditForm')->name('about_who_we_are_edit_form');
    Route::post('/about_us/who_we_are_edit', 'AboutController@aboutWhoWeAreEdit')->name('about_who_we_are_edit');
    Route::get('/about_us/what_we_do_edit_form', 'AboutController@aboutWhatWeDoEditForm')->name('about_what_we_do_edit_form');
    Route::post('/about_us/what_we_do_edit', 'AboutController@aboutWhatWeDoEdit')->name('about_what_we_do_edit');
    // News route
    Route::get('/news/banner_edit_form', 'NewsController@newsBannerEditForm')->name('news_banner_edit_form');
    Route::post('/news/banner_edit', 'NewsController@newsBannerEdit')->name('news_banner_edit');
    Route::get('/news_manage/{page_num}', 'NewsController@newsManage')->name('news_manage');
    Route::get('/news_manage/{page_num}/add_form', 'NewsController@addNewsForm')->name('news_add_form');
    Route::post('/news_manage/add', 'NewsController@addNews')->name('news_add');
    Route::get('/news_manage/{page_num}/edit_form/{id}', 'NewsController@newsEditForm')->name('news_edit_form');
    Route::post('/news_manage/edit', 'NewsController@newsEdit')->name('news_edit');
    // News route - dropzone upload image
    Route::post('/news_manage/upload_news_gallery', 'NewsController@uploadNewsImageDropzone')->name('upload_gallery_image');
    Route::post('/news_manage/load_news_gallery_list', 'NewsController@setImageListPreview')->name('load_news_gallery_list');
    Route::post('/news_manage/remove_news_gallery', 'NewsController@removeNewsGalleryImage')->name('delete_news_image_attache');
    // .end dropzone upload image
    Route::get('/news_manage/{page_num}/hide/{id}', 'NewsController@hideNews')->name('hide_news');
    Route::get('/news_manage/{page_num}/show/{id}', 'NewsController@showNews')->name('show_news');
    Route::get('/news_manage/{page_num}/remove/{id}', 'NewsController@removeNews')->name('remove_news');
    // Contact us route
    Route::get('/contact_us/banner_edit_form', 'ContactController@contactBannerEditForm')->name('contact_banner_edit_form');
    Route::post('/contact_us/banner_edit', 'ContactController@contactBannerEdit')->name('contact_banner_edit');
    Route::get('/contact_us/data_edit_form', 'ContactController@contactDataEditForm')->name('contact_data_edit_form');
    Route::post('/contact_us/data_edit', 'ContactController@contactDataEdit')->name('contact_data_edit');
    // Footer us route
    Route::get('/footer/edit_form', 'ContactController@footerEditForm')->name('footer_edit_form');
    Route::post('/footer/edit', 'ContactController@footerEdit')->name('footer_edit');
    // Donation route
    Route::get('/donation/banner_edit_form', 'DonationController@donationBannerEditForm')->name('donation_banner_edit_form');
    Route::post('/donation/banner_edit', 'DonationController@donationBannerEdit')->name('donation_banner_edit');
    Route::get('/donation/how_you_can_help_edit_form', 'DonationController@donationHowYouCanHelpEditForm')->name('donation_how_you_can_help_edit_form');
    Route::post('/donation/how_you_can_help_edit', 'DonationController@donationHowYouCanHelpEdit')->name('donation_how_you_can_help_edit');
    // Contact Person route
    Route::get('/contact_submit/list/{filter?}', 'ContactSubmitController@contactList')->name('contact_list');
    // Donation route
    Route::get('/donation_submit/list/{filter?}', 'DonationSubmitController@donationList')->name('donation_list');
    Route::get('/donation_submit/detail/{id}/{filter?}', 'DonationSubmitController@donationDetail')->name('donation_detail');

    // File Upload
    Route::get('/file_upload', 'FileUploadController@index')->name('file_upload');
    Route::post('/file_upload/upload', 'FileUploadController@fileUpload')->name('file_upload.upload');
    Route::get('/file_upload/download/{id}', 'FileUploadController@downloadFile')->name('file_upload.download');
    Route::get('/file_upload/remove/{id}', 'FileUploadController@removeFile')->name('file_upload.remove');
});

// ------------------------ .end Back-End -------------------------------------
