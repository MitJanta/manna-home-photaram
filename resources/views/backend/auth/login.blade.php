<!DOCTYPE html>
<html lang="th" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_th . ' : ' . config('app.name', 'Manna') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap 4.5.0 CSS -->
  	<link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/bootstrap-4.5.0/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="{{ config('app.url') }}assets/plugins/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ config('app.url') }}assets/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page" style="background-color: #fff1d1;">
  <div class="bg-image w-100 h-100 red-box" style="position: absolute;z-index: -1;left: 0;top: 0;background-image: url('{{ config('app.url') }}assets/images/photo-4.jpg');background-size: cover;background-position: center;filter: blur(8px);-webkit-filter: blur(8px);">
    .
  </div>

  <div class="login-box">
    <div class="login-logo">
      <!-- <a href="../../index2.html"><b>Admin</b>UAE</a> -->
      <img src="{{ config('app.url') }}assets/images/favicon/android-icon-192x192.png" style="max-width: 250px;width: 250px; margin-top:-50px" alt="Logo" title="uae_logo">
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body bg-light mt-n2">
        <p class="login-box-msg">ลงชื่อเข้าใช้งานระบบจัดการข้อมูลเว็บ Manna Home Photharam</p>

        <form action="{{ route('auth.login.auth') }}" method="POST">
          @csrf

          <div class="input-group mb-3">
            <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror"
                   placeholder='อีเมล' value="{{ old('email') }}" autocomplete="email" autofocus
                   oninvalid="this.setCustomValidity('กรุณากรอกอีเมลให้ถูกต้อง')" oninput="this.setCustomValidity('')" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
            @error('email')
                <span class="invalid-feedback ml-2" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="input-group mb-3">
            <input id="password" type="password" class="form-control @error('password') ? is-invalid : is-valid @enderror @if(session('error')) ? is-invalid : is-valid @endif"
                   name="password" autocomplete="current-password" placeholder="รหัสผ่าน"
                   oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่าน')" oninput="this.setCustomValidity('')" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
            @error('password')
                <span class="invalid-feedback ml-2" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <span class="invalid-feedback ml-2" role="alert">
                <strong>{{ session('error') }}</strong>
            </span>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="icheck-warning">
                <input class="form-check-input" type="checkbox" name="remember"  id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">
                  จดจำรหัสผ่าน
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6 mt-md-0 mt-3">
              <button type="submit" class="btn btn-warning btn-block"><b>เข้าสู่ระบบ</b></button>
            </div>
            <!-- /.col -->
          </div>
        </form>

      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->

  <!-- jquery .js (v.3.4.1) -->
  <script type="text/javascript" src="{{ config('app.url') }}assets/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap .js -->
  <script type="text/javascript" src="{{ config('app.url') }}assets/plugins/bootstrap-4.5.0/js/bootstrap.min.js"></script>
</body>
