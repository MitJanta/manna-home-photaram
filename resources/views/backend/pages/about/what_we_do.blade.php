<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> <i class="fas fa-address-card"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                  <div class="card card-warning">
                    <div class="card-header">
                      <h3 class="card-title">แก้ไขข้อมูล{{ $page_th }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="send_form" class="form-horizontal" action="{{ route('backend.about_what_we_do_edit') }}" onsubmit="onLoad()" method="post">
                      @csrf

                      <div class="container">
                        <div class="card-body">

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="about_us_th_text" class="col-lg-12 pl-1 control-label">ข้อความ What we do (ไทย)</label>
                              <textarea name="about_us_th_text" id="about_us_th_text" class="textarea" placeholder="ข้อความ What we do 1"
                                style="width: 100%; height: 250px;">{!! $what_we_do_th->content_th !!}</textarea>
                              @error('about_us_th_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text EN -->
                          <div class="col-12 px-0">
                            <label for="about_us_en_text" class="mt-3 control-label">ข้อความ What we do (EN)</label>
                              <textarea name="about_us_en_text" id="about_us_en_text" class="textarea" placeholder="ข้อความ What we do (EN)"
                                style="width: 100%; height: 250px;">{!! $what_we_do_eng->content_en !!}</textarea>
                              @error('about_us_en_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text EN -->

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูป What we do 1</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูป What we do 1 ใหม่ </strong><br><small>( ขนาด : 768 x 768 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_about_us_1" id="upload_input_about_us_1" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_about_us_1" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_about_us_1')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="about_us_1_data" id="about_us_1_data">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_about_us_1" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูป What we do 1 เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_1->image_name }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_1->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูป What we do 2</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูป What we do 2 ใหม่ </strong><br><small>( ขนาด : 768 x 768 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_about_us_2" id="upload_input_about_us_2" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_about_us_2" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_about_us_2')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="about_us_2_data" id="about_us_2_data">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_about_us_2" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูป What we do 2 เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid2">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_2->image_name }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_2->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูป What we do 3</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูป What we do 3 ใหม่ </strong><br><small>( ขนาด : 768 x 768 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_about_us_3" id="upload_input_about_us_3" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_about_us_3" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_about_us_3')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="about_us_3_data" id="about_us_3_data">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_about_us_3" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูป What we do 3 เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid3">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_3->image_name }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_3->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                        </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <div class="container">
                          <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                        </div>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Summernote
$('#about_us_th_text').summernote({
    height: 200,
    placeholder: 'กรอกข้อความ What we do (ไทย)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#about_us_en_text').summernote({
    height: 200,
    placeholder: 'กรอกข้อความ What we do (EN)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Croppie for news image
$uploadCrop2 = $('#upload_display_about_us_1').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 257,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 325
    }
});

// Croppie for news image
$uploadCrop3 = $('#upload_display_about_us_2').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 257,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 325
    }
});

// Croppie for news image
$uploadCrop4 = $('#upload_display_about_us_3').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 257,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 325
    }
});

$('#upload_input_about_us_1').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop2.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#upload_input_about_us_2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop3.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#upload_input_about_us_3').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop4.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop2.croppie('result', {
        type: 'canvas',
        size: {
            width: 768
        }
    }).then(function(resp) {
        image = resp;
        $('#about_us_1_data').val(image);
        // console.log(resp);
    });

    $uploadCrop3.croppie('result', {
        type: 'canvas',
        size: {
            width: 768
        }
    }).then(function(resp) {
        image = resp;
        $('#about_us_2_data').val(image);
        // console.log(resp);
    });

    $uploadCrop4.croppie('result', {
        type: 'canvas',
        size: {
            width: 768
        }
    }).then(function(resp) {
        image = resp;
        $('#about_us_3_data').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid2'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid3'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
