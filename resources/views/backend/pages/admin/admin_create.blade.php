<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"><i class="fas fa-user-plus"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a onclick="onLoad('{{ route('admin.admin_manage') }}')" href="{{ route('admin.admin_manage') }}">จัดการผู้ดูแลเว็บ</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="card card-warning">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-user-plus" style="margin-right:5px"></i> {{ $page_th }}</h3>
                        <button type="button" onclick="onLoad('{{ route('admin.admin_manage') }}')" class="btn btn-default btn-sm float-right">
                            <i class="fas fa-chevron-left"> ย้อนกลับ</i>
                        </button>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="created_admin" class="form-horizontal" action="{{ route('admin.admin_manage.create') }}" onsubmit="onLoad()" method="post">
                        @csrf

                        <div class="card-body container">
                            <div class="form-group row">
                                <label for="inputName" class="col-lg-2 control-label">ชื่อ<span class="text-danger"> *</span></label>
                                <div class="input-group col-lg-10">
                                    <input type="text" name="name" id="inputName" class="form-control @error('name') is-invalid @enderror" placeholder="ชื่อ" value="{{ old('name')}}" oninvalid="this.setCustomValidity('กรุณากรอกชื่อ')"
                                      oninput="this.setCustomValidity('')" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-user"></span>
                                        </div>
                                    </div>
                                    @error('name')
                                    <span class="invalid-feedback ml-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail" class="col-lg-2 control-label">อีเมล<span class="text-danger"> *</span></label>
                                <div class="input-group col-lg-10">
                                    <input type="email" name="email" id="inputEmail" class="form-control @error('email') is-invalid @enderror" placeholder="อีเมล" value="{{ old('email') }}"
                                      oninvalid="this.setCustomValidity('กรุณากรอกอีเมลให้ถูกต้อง')" oninput="this.setCustomValidity('')" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-envelope"></span>
                                        </div>
                                    </div>
                                    @error('email')
                                    <span class="invalid-feedback ml-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-lg-2 control-label">รหัสผ่าน<span class="text-danger"> *</span></label>
                                <div class="input-group col-lg-10">
                                    <input name="password" type="password" id="inputPassword" class="form-control @error('password') is-invalid @enderror" placeholder="รหัสผ่าน (อย่างน้อย 6 ตัว)" value="{{ old('password') }}"
                                      oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่าน')" oninput="this.setCustomValidity('')" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback ml-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputConfirmPassword" class="col-lg-2 control-label">ยืนยันรหัสผ่าน<span class="text-danger"> *</span></label>
                                <div class="input-group col-lg-10">
                                    <input name="confirm_password" type="password" id="inputConfirmPassword" class="form-control @error('confirm_password') is-invalid @enderror" placeholder="กรอกรหัสผ่านอีกครั้ง" value="{{ old('confirm_password') }}"
                                      oninvalid="this.setCustomValidity('กรุณากรอกยืนยันรหัสผ่าน')" oninput="this.setCustomValidity('')" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-lock"></span>
                                        </div>
                                    </div>
                                    @error('confirm_password')
                                    <span class="invalid-feedback ml-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- checkbox permission-->
                            <div class="form-group row">
                                <label for="pm_chk1" class="col-lg-2 control-label">สิทธิ์การเข้าใช้งาน<span class="text-danger"> *</span></label>
                                <div class="col-lg-10 clearfix">
                                    <div class="icheck-warning">
                                        <input class="form-check-input" type="checkbox" id="pm_chk1" name="admin_chk1">
                                        <label class="form-check-label" for="pm_chk1">จัดการผู้ดูแลเว็บ</label>
                                    </div>
                                    <div class="icheck-warning">
                                        <input class="form-check-input" type="checkbox" id="pm_chk2" name="admin_chk2">
                                        <label class="form-check-label" for="pm_chk2">ข้อมูลทั่วไป</label>
                                    </div>
                                    <div class="icheck-warning">
                                        <input class="form-check-input" type="checkbox" id="pm_chk3" name="admin_chk3">
                                        <label class="form-check-label" for="pm_chk3">ข่าวและกิจกรรม</label>
                                    </div>
                                    <div class="icheck-warning">
                                        <input class="form-check-input" type="checkbox" id="pm_chk4" name="admin_chk4">
                                        <label class="form-check-label" for="pm_chk4">ข้อมูลติดต่อและการบริจาค</label>
                                    </div>
                                    <small class="text-danger ml-2">
                                        <strong id="leastOnePermission"></strong>
                                    </small>
                                </div>

                            </div>
                            <!-- End checkbox permission-->

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="container">
                                <button type="submit" class="btn btn-warning float-right" onclick="return onClickCreate()">เพิ่มบัญชีผู้ดูแลเว็บ</button>
                                <button type="button" class="btn btn-secondary" onclick="onLoad('{{ route('admin.admin_manage') }}')">ยกเลิก</button>
                            </div>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>

            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->

    <!-- Custom javascript for Admin create page -->
    <script type="text/javascript">
        function onClickCreate() {
            checked = $("input[type=checkbox]:checked").length;
            if ($('#inputName').val() == '' || $('#inputEmail').val() == '' || $('#inputPassword').val() == '' || $('#inputConfirmPassword').val() == '') {
                // return false;
            } else if (!checked && $('#inputName').val() != '' && $('#inputEmail').val() != '' && $('#inputPassword').val() != '' && $('#inputConfirmPassword').val() != '') {
                $('#leastOnePermission').text('กรุณาเลือกสิทธิ์อย่างน้อย 1 สิทธิ์');
                return false;
            } else {
                $('#loader_spinner').css('display', 'block');
                $("#created_admin").submit();
            }
        }
    </script>
    <!-- .end Custom javascript for Admin create page -->
</body>

</html>
