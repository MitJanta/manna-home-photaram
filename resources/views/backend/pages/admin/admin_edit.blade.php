<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-user-edit"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.admin_manage') }}" onclick="onLoad('{{ route('admin.admin_manage') }}')">จัดการผู้ดูแลเว็บ</a></li>
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
      <section class="content">
        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">แก้ไขข้อมูลผู้จัดการเว็บ : {{ $admin_detail->name }}</h3>
            <span class="float-right">อัพเดตล่าสุด : {{ date('d/m/Y', strtotime($admin_detail->updated_at)) }} <i class="far fa-clock"></i> {{ date('H:i:s', strtotime($admin_detail->updated_at)) }}</span>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form id="send_form" class="form-horizontal" action="{{ route('admin.admin_manage.edit') }}" onsubmit="onLoad()" method="post">
            @csrf

            <div class="container">
              <div class="card-body">
                <div class="form-group row">
                  <label for="inputName" class="col-lg-2 control-label">ชื่อ</label>
                  <div class="input-group col-lg-10">
                    <input type="hidden" name="id" value="{{ $admin_detail->id }}"/>
                    <input type="text" name="admin_name" id="inputName" class="form-control" placeholder="ชื่อ" value="{{ $admin_detail->name }}">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-user"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail" class="col-lg-2 control-label">อีเมล</label>
                  <div class="input-group col-lg-10">
                    <input type="email" name="admin_email" id="inputEmail" class="form-control"
                    placeholder="อีเมล" value="{{ $admin_detail->email }}" disabled>
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- checkbox permission-->
                <div class="form-group row">
                  <label class="col-lg-2 control-label">สิทธิ์การเข้าใช้งาน</label>
                  <div class="col-lg-10 clearfix">

                    <div class="icheck-warning">
                      <input class="form-check-input" type="checkbox" id="pm_chk1" name="admin_chk1"
                      @if($admin_detail->type) checked disabled
                      @elseif(!$admin_detail->type && $admin_detail->permission_1) checked
                      @endif>
                      @if($admin_detail->type)
                      <input type="hidden" name="admin_chk1" value="on">
                      @endif
                      <label class="form-check-label" for="pm_chk1">จัดการผู้ดูแลเว็บ</label>
                    </div>

                    <div class="icheck-warning">
                      <input class="form-check-input" type="checkbox" id="pm_chk2" name="admin_chk2"
                      @if($admin_detail->type) disabled checked value="on"
                      @elseif(!$admin_detail->type && $admin_detail->permission_2) checked
                      @endif>
                      @if($admin_detail->type)
                      <input type="hidden" name="admin_chk2" value="on">
                      @endif
                      <label class="form-check-label" for="pm_chk2">ข้อมูลทั่วไป</label>
                    </div>

                    <div class="icheck-warning">
                      <input class="form-check-input" type="checkbox" id="pm_chk3" name="admin_chk3"
                      @if($admin_detail->type) disabled checked value="on"
                      @elseif(!$admin_detail->type && $admin_detail->permission_3) checked
                      @endif>
                      @if($admin_detail->type)
                      <input type="hidden" name="admin_chk3" value="on">
                      @endif
                      <label class="form-check-label" for="pm_chk3">ข่าวและกิจกรรม</label>
                    </div>

                    <div class="icheck-warning">
                      <input class="form-check-input" type="checkbox" id="pm_chk4" name="admin_chk4"
                      @if($admin_detail->type) disabled checked value="on"
                      @elseif(!$admin_detail->type && $admin_detail->permission_4) checked
                      @endif>
                      @if($admin_detail->type)
                      <input type="hidden" name="admin_chk4" value="on">
                      @endif
                      <label class="form-check-label" for="pm_chk4">ข้อมูลติดต่อและการบริจาค</label>
                    </div>

                    @if(session('permission') != null)
                    <small class="text-danger">
                        <strong>{{ session('permission') }}</strong>
                    </small>
                    @endif

                  </div>
                </div>
                <!-- End checkbox permission-->

              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="container">
                <button type="submit" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                <button onclick="onLoad('{{ route('admin.admin_manage') }}')" type="button" class="btn btn-secondary">ยกเลิก</button>
              </div>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>

      </section>
      <!-- /.content -->

</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
  // $(document).ready(
  //   // Summernote
  //   $('.textarea').summernote()
  // );
</script>
</body>
</html>
