<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-users"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"><b>ตาราง : ผู้ดูแลเว็บ</b> (จำนวน {{ $admins_num }} คน)</h3>

        <div class="card-tools">
          <button onclick="onLoad('{{ route('admin.admin_manage.create_form') }}')" type="button" class="btn btn-success">
            <i class="fas fa-plus" style="padding-right: 5px"></i> เพิ่มผู้ดูแลเว็บ</button>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-responsive-xl table-hover">
            <thead>
              <tr>
                  <!-- <th style="width: 3%">ลำดับ</th> -->
                  <th style="width: auto">ชื่อ <small class="text-primary">(กดที่ชื่อเพื่อดูบันทึกการใช้งาน)</small></th>
                  <th style="width: 25%">สิทธิ์การเข้าถึง</th>
                  <th style="width: 25%">แก้ไขล่าสุด <small>&nbsp<i class="fas fa-arrow-up"></i></small></th>
                  <th style="width: 10%">แก้ไข</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                    <!-- <th style="width: 3%">ลำดับ</th> -->
                    <th style="width: auto">ชื่อ <small>(กดที่ชื่อเพื่อดูบันทึกการใช้งาน)</small></th>
                    <th style="width: 25%">สิทธิ์การเข้าถึง</th>
                    <th style="width: 25%">แก้ไขล่าสุด <small>&nbsp<i class="fas fa-arrow-up"></i></small></th>
                    <th style="width: 10%">แก้ไข</th>
                </tr>
            </foot>
            <tbody>

               <!-- Loop users table -->
               @foreach($admin_list as $row)
               <tr>
                   <td>
                     <ul class="list-inline">
                       <li class="list-inline-item time-line">
                         <i class="fas @if($row->type) fa-user-cog @else fa-user @endif text-gray-dark" style="font-size:2em"></i>
                       </li>
                       <a href="{{ route('admin.admin_manage.logging', $row->id) }}" target="_blank">
                       <li class="list-inline-item">
                         <p style="margin-top:-23px;position:absolute;background-color:#d9edf7;padding:2px 5px;border-radius:8px">{{ $row->name }} &nbsp;<i class="fas fa-external-link-alt" style="font-size: .7rem"></i> </p>
                       </li>
                      </a>
                     </ul>
                   </td>

                   <td>
                     <p>@if($row->type === 1) <b>Super Admin</b> @else Sub Admin @endif</p>
                   </td>

                   <td>
                       <p>{{ date('d/m/Y', strtotime($row->updated_at)) }}<br><i class="far fa-clock"></i> {{ date('H:i', strtotime($row->updated_at)) }}</p1>
                   </td>
                   <td class="project-actions text-left">
                     <a href="{{ route('admin.admin_manage.edit_form', $row->id) }}">
                     <button class="btn btn-warning btn-sm" onclick="onLoad('{{ route('admin.admin_manage.edit_form', $row->id) }}')">
                         <i class="fas fa-pencil-alt">
                         </i>
                         แก้ไข
                     </button>
                   </a>
                   </td>
               </tr>
               @endforeach
             <!-- End loop users table -->
            </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
  // $(document).ready(
  //   // Summernote
  //   $('.textarea').summernote()
  // );
</script>
</body>
</html>
