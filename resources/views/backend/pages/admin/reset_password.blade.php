<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-key"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

<!-- Main content -->
<section class="content">

  <div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title"><i class="fas @if(Auth::guard('admin')->user()->isSuperAdmin()) fa-user-cog @else fa-user @endif"></i> : {{ $admin_name }}</h3>
      <span class="float-right">อัพเดตล่าสุด : {{ date('d/m/Y', strtotime($updated_at)) }} <i class="far fa-clock"></i> {{ date('H:i:s', strtotime($updated_at)) }}</span>
    </div>
    <!-- /.card-header -->


      <div class="container">
      <div class="card-body">
        <!-- Original password -->
        <!-- form start -->
        <form class="form-horizontal" action="{{ route('admin.reset_password.update') }}" method="post" onsubmit="onLoad()">
          @csrf
        <div class="form-group row">
          <label for="admin_old_password" class="col-lg-2 control-label">รหัสผ่านเดิม <span class="text-danger">*</span></label>
          <div class="input-group col-lg-10">
            <input name="admin_old_password" type="password" id="admin_old_password"
                   class="form-control @error('admin_old_password') is-invalid @enderror @if(session('password') != '') is-invalid @endif"
                   placeholder="รหัสผ่านเดิม" value="{{ old('admin_old_password') }}"
                   oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่านเดิม')" oninput="this.setCustomValidity('')" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user-lock"></span>
              </div>
            </div>
            @error('admin_old_password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <span class="invalid-feedback ml-2" role="alert">
            <strong>{{ session('password') }}</strong>
            </span>
          </div>

        </div>

        <!-- New password -->
        <div class="form-group row">
          <label for="admin_password" class="col-lg-2 control-label">รหัสผ่านใหม่ <span class="text-danger">*</span><br class="d-lg-block d-none"><small style="position:absolute;margin-top:3px" class="text-danger"> (อย่างน้อย 6 ตัว)</small></label>
          <div class="input-group col-lg-10">
            <input name="admin_password" type="password" id="admin_password"
                   class="form-control @error('admin_password') is-invalid @enderror"
                   placeholder="รหัสผ่านใหม่" value="{{ old('admin_password') }}"
                   oninvalid="this.setCustomValidity('กรุณากรอกรหัสผ่านใหม่')" oninput="this.setCustomValidity('')" required>
            <!-- <span toggle="#inputPassword" class="far fa-fw fa-eye field-icon toggle-password text-gray"
                  style="float: right;margin-left: -35px;padding-right: 35px;margin-top: 10px;position: relative;z-index: 2;"></span> -->
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="col-lg-2"></div>
          <!-- <span class="text-danger" style="margin-top:8px;margin-left:8px"></span> -->
          @error('admin_password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>

        <!-- Confirm new password -->
        <div class="form-group row">
          <label for="confirm_password" class="col-lg-2 control-label">ยืนยันรหัสผ่าน <span class="text-danger">*</span></label>
          <div class="input-group col-lg-10">
            <input name="confirm_password" type="password" id="confirm_password"
                   class="form-control @error('confirm_password') is-invalid @enderror"
                   placeholder="กรอกรหัสผ่านอีกครั้ง" value="{{ old('confirm_password') }}"
                   oninvalid="this.setCustomValidity('กรุณากรอกยืนยันรหัสผ่าน')" oninput="this.setCustomValidity('')" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="col-lg-2"></div>
          @error('confirm_password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>

      </div>
    </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <div class="container">
        <button type="submit" class="btn btn-warning float-right">เปลี่ยนรหัสผ่าน</button>
        <button type="button" class="btn btn-secondary" onclick="onLoad('{{ route('backend.dashboard') }}')">ยกเลิก</button>
      </div>
    </div>
      <!-- /.card-footer -->
    </form>
  </div>

</section>
<!-- /.content -->

</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
// Custom javascript here ...
</script>
</body>
</html>
