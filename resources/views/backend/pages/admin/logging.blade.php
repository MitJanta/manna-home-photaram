<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<style media="screen">
    #logging_table_filter {
        float: right;
    }

    #logging_table_paginate {
        float: right;
    }
</style>

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"><i class="fas fa-users"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('admin.admin_manage') }}" onclick="onLoad('{{ route('admin.admin_manage') }}')">จัดการผู้ดูแลเว็บ</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">ตาราง : บันทึกการใช้งานของ <b>{{ $admin_name->name }}</b></h3>
                        <button type="button" onclick="onLoad('{{ route('admin.admin_manage') }}')" class="btn bg-gray btn-sm float-right">
                            <i class="fas fa-chevron-left"> ย้อนกลับ</i>
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="logging_table" class="table table-bordered table-responsive-xl table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="width: 110px;min-width: 110px">เวลา</th>
                                    <th style="max-width: 120px">ที่อยู่ IP</th>
                                    <th style="width: auto">ลิงก์ที่ทำงาน <small>& คำสั่ง</small></th>
                                    <th style="width: auto">รายละเอียด</th>
                                    <th style="max-width: 200px">เบราว์เซอร์</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>เวลา</th>
                                    <th>ที่อยู่ IP</th>
                                    <th>ลิงก์ที่ทำงาน <small>& คำสั่ง</small></th>
                                    <th>รายละเอียด</th>
                                    <th>เบราว์เซอร์</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <!-- Loop logs table -->
                                @foreach($logs_list as $row)
                                <tr>
                                    <td>{!! \App\Helper\DateHelper::dateToThaiFormat($row->created_at) !!}
                                    </td>
                                    <td>{{ $row->log_ip }}</td>
                                    <td>{{ $row->log_url }}<br><b>{{ $row->log_method }}</b></td>
                                    <td>{{ $row->log_desc }}</td>
                                    <td>{{ $row->log_agent }}</td>
                                </tr>
                                @endforeach
                                <!-- End loop logs table -->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
    <script type="text/javascript">
        $("#logging_table").DataTable({
            'stateSave': true, // Awesome that can remember page of dataTable
            'ordering': false,
            'language': {
                "decimal": "",
                "emptyTable": "ยังไม่มีบันทึก",
                "info": "แสดงลำดับ _START_ ถึง _END_ จาก _TOTAL_ บันทึก",
                "infoEmpty": "แสดงลำดับที่ 0 ถึง 0 จาก 0 บันทึก",
                "infoFiltered": "(กรองข้อมูลจาก _MAX_ บันทึกทั้งหมด)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "แสดง _MENU_ &nbspบันทึก",
                "loadingRecords": "กำลังโหลด...",
                "processing": "กำลังประมวลผล...",
                "search": "ค้นหา:",
                "zeroRecords": "ไม่ตรงกับข้อมูลใดๆ",
                "paginate": {
                    "first": "แรก",
                    "last": "สุดท้าย",
                    "next": "ถัดไป",
                    "previous": "ก่อนหน้า"
                },
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
            },
        });
    </script>
</body>

</html>
