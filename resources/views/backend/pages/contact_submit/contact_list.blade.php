<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<style media="screen">
  #contact_table_filter {
    float: right;
  }

  #contact_table_paginate {
    float: right;
  }
</style>

<!-- Body -->
<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-envelope"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
      <section class="content">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">ตาราง : {{ $page_th }} </h3>
          </div>
          <div class="card-body">
            <table id="contact_table" class="table table-bordered table-hover table-responsive-xl" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="all" style="min-width:105px;width:105px;max-width:105px" width="105">วันที่</th>
                  <th class="all" style="min-width:200px;width:auto;">ชื่อ-นามสกุล</th>
                  <th class="all" style="min-width:200px;width:auto;">เบอร์โทร</th>
                  <th class="all" style="min-width:200px;width:auto;">อีเมล</th>
                  <th class="all" style="min-width:200px;width:auto;">ข้อความ</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>วันที่</th>
                  <th>ชื่อ-นามสกุล</th>
                  <th>เบอร์โทร</th>
                  <th>อีเมล</th>
                  <th>ข้อความ</th>
                </tr>
              </tfoot>
              <tbody>
                <!-- Loop contact table -->
                @foreach ($contact_list as $row)
                  <tr>
                    <td>
                      {!! App\Helper\DateHelper::dateTimeToSlashFormat($row['created_at']) !!}
                    </td>
                    <td>
                      {{ $row['first_name'] }} {{ $row['last_name'] }}
                    </td>
                    <td>
                      {{ $row['telephone'] }}
                    </td>
                    <td>
                      {{ $row['email'] }}
                    </td>
                    <td>
                      {{ $row['message'] }}
                    </td>
                  </tr>
                  @endforeach
                  <!-- End loop users table -->
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        @include('backend.layouts.modal')

      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
    var table = $("#contact_table").DataTable({
          'stateSave': true,  // Awesome that can remember page of dataTable
          'ordering': false,
          // 'scrollX': true,
           'language': {"decimal": "",
               "emptyTable":     "ยังไม่มีข้อความ",
               "info":           "แสดงลำดับ _START_ ถึง _END_ จาก _TOTAL_ ข้อความ",
               "infoEmpty":      "แสดงลำดับที่ 0 ถึง 0 จาก 0 ข้อความ",
               "infoFiltered":   "(กรองข้อมูลจาก _MAX_ ข้อความทั้งหมด)",
               "infoPostFix":    "",
               "thousands":      ",",
               "lengthMenu":     "แสดง _MENU_ &nbspข้อความ",
               "loadingRecords": "กำลังโหลด...",
               "processing":     "กำลังประมวลผล...",
               "search":         "ค้นหา:",
               "zeroRecords":    "ไม่ตรงกับข้อมูลใดๆ",
               "paginate": {
                   "first":      "แรก",
                   "last":       "สุดท้าย",
                   "next":       "ถัดไป",
                   "previous":   "ก่อนหน้า"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               },
             },
    });
</script>
</body>
</html>
