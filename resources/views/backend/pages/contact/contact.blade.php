<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> <i class="far fa-address-card"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                  <div class="card card-warning">
                    <div class="card-header">
                      <h3 class="card-title">แก้ไขข้อมูล{{ $page_th }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="send_form" class="form-horizontal" action="{{ route('backend.contact_data_edit') }}" onsubmit="onLoad()" method="post">
                      @csrf

                      <div class="container">
                        <div class="card-body">

                          <!-- What we do text TH-->
                          <div class="input-group pt-3">
                            <label for="contact_email_text" class="col-lg-12 pl-1 control-label">อีเมลติดต่อ</label>
                              <input type="email" name="contact_email_text" id="contact_email_text" value="{{ $contact_email->content_th }}" class="form-control @error('contact_email_text') is-invalid @enderror" placeholder="กรอกที่อยู่ E-mail"
                                oninvalid="this.setCustomValidity('กรุณากรอก E-mail ให้ถูกต้อง')" oninput="this.setCustomValidity('')" required>
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-envelope"></span>
                                  </div>
                              </div>
                              @error('contact_email_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="contact_address_text" class="mt-3 control-label">ที่อยู่</label>
                              <textarea name="contact_address_text" id="contact_address_text" class="textarea" placeholder="กรอกที่อยู่"
                                style="width: 100%; height: 250px;">{!! $contact_address->content_th !!}</textarea>
                              @error('contact_address_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="input-group pt-3">
                            <label for="contact_tel_1_text" class="col-lg-12 pl-1 control-label">เบอร์โทร 1</label>
                              <input type="text" name="contact_tel_1_text" id="contact_tel_1_text" value="{{ $contact_tel_1->content_th }}" class="form-control @error('contact_tel_1_text') is-invalid @enderror" placeholder="กรอกเบอร์โทร"
                                oninvalid="this.setCustomValidity('กรุณากรอกเบอร์โทร')" oninput="this.setCustomValidity('')" required>
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-phone-alt"></span>
                                  </div>
                              </div>
                              @error('contact_tel_1_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="input-group pt-3">
                            <label for="contact_tel_2_text" class="col-lg-12 pl-1 control-label">เบอร์โทร 2</label>
                              <input type="text" name="contact_tel_2_text" id="contact_tel_2_text" value="{{ $contact_tel_2->content_th }}" class="form-control @error('contact_tel_2_text') is-invalid @enderror" placeholder="กรอกเบอร์โทร"
                                oninvalid="this.setCustomValidity('กรุณากรอกเบอร์โทร')" oninput="this.setCustomValidity('')" required>
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-phone-alt"></span>
                                  </div>
                              </div>
                              @error('contact_tel_2_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- Google Map -->
                          <div class="input-group pt-3">
                            <label for="contact_map_text" class="col-lg-12 pl-1 control-label">Google Map (โค้ดแบบฝัง Embed) : <a href="https://www.google.co.th/maps" target="_blank">Google map <i class="fas fa-external-link-alt"></i> </a>
                              &nbsp;<span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="วิธีการคัดลอกโค้ด" data-content='1. เข้าลิงก์ Google map จากลิงก์ด้านซ้าย</br>2. เลือกตำแหน่งที่ต้องการฝังโค้ด Google map</br>3. กดปุ่ม "แชร์"</br>4. จะแสดงหน้าต่างขึ้นมา ให้กดปุ่มเมนูชื่อ "ฝังแผนที่"</br>5. กดปุ่ม "คัดลอก HTML"</br>6. วางโค้ดแทนโค้ดเดิมในช่องกรอก "Google Map (โค้ดแบบฝัง Embed)"'
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                    </span>
                            &nbsp;<span class="" id="grid">
                                <a href="{{ config('app.url') }}assets/images/google_map_ex.png" target="_blank" style="cursor: zoom-in">
                                  <i class="fas fa-image"></i>
                                </a>
                            </span>
                            </label>
                              <textarea rows="7" name="contact_map_text" id="contact_map_text" class="form-control @error('contact_map_text') is-invalid @enderror" placeholder="กรอก Google Map (โค้ดแบบฝัง Embed)"
                                oninvalid="this.setCustomValidity('กรุณากรอก Google Map')" oninput="this.setCustomValidity('')" required>{!! $contact_google_map->content_th !!}</textarea>
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-map-marked-alt"></span>
                                  </div>
                              </div>
                              @error('contact_map_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end Google Map -->

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูปข้อความติดต่อ</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูปข้อความติดต่อใหม่ </strong><br><small>( ขนาด : 768 x 1039 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_image" id="upload_input_image" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_image" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_image')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="image_data" id="image_data">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_image" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูปข้อความติดต่อเดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $contact_massage_img->image_name }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $contact_massage_img->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                        </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <div class="container">
                          <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                        </div>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Summernote
$('#contact_address_text').summernote({
    height: 150,
    placeholder: 'กรอกที่อยู่',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Croppie for news image
$uploadCrop = $('#upload_display_image').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 347.8,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 420
    }
});

$('#upload_input_image').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop.croppie('result', {
        type: 'canvas',
        size: {
            width: 768
        }
    }).then(function(resp) {
        image = resp;
        $('#image_data').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid2'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
