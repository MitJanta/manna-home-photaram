<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<style media="screen">
  #news_table_filter {
    float: right;
  }

  #news_table_paginate {
    float: right;
  }
</style>

<!-- Body -->
<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-newspaper"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
      <section class="content">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">ตาราง : ข่าวสาร </h3>
            <div class="card-tools" id="button_add_admin">
              <button href="#" onclick="onLoad('{{ route('backend.news_add_form', $page_num) }}')" type="button" class="btn btn-success alertToast">
                <i class="fas fa-plus" style="padding-right: 5px"></i> เพิ่มข่าว</button>
            </div>
          </div>
          <div class="card-body">
            <table id="news_table" class="table table-bordered table-hover table-responsive-xl" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="all" style="min-width:105px;width:105px;max-width:105px" width="105">วันที่ข่าว</th>
                  <th class="all" style="width:110px;max-width: 110px">ภาพ</th>
                  <th class="all" style="min-width:200px;width:auto;">หัวข้อข่าว</th>
                  <th class="desktop text-center" style="width:5%">เปิด/ซ่อน</th>
                  <th class="all text-center" style="width:5%">แก้ไข</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th class="all">วันที่ข่าว</th>
                  <th class="all">ภาพ</th>
                  <th class="all">หัวข้อข่าว</th>
                  <th class="desktop text-center">เปิด/ซ่อน</th>
                  <th class="all text-center">แก้ไข</th>
                </tr>
              </tfoot>
              <tbody>
                <!-- Loop users table -->
                @foreach ($news_list as $row)
                  <tr>
                    <td>
                      {!! \App\Helper\DateHelper::dateToThaiFormat($row['created_at']) !!}
                    </td>
                    <td>
                        <img style="height:auto;width:160px" class="lazyload" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" data-original="{{ config('app.url') }}files_upload/news_acts/{{ $row['cover'] }}"/>
                    </td>
                    <td>
                        {{ $row['title'] }}</p>
                    </td>
                    <td class="text-center">
                      <p>
                        @if($row['enable'] == 1)
                          <a href="{{ route('backend.hide_news', [$page_num, $row['id']]) }}">
                            <i class="far fa-eye text-gray-dark"></i>
                          </a>
                         @else
                          <a href="{{ route('backend.show_news', [$page_num, $row['id']]) }}">
                            <i class="far fa-eye-slash text-danger"></i>
                          </a>
                         @endif
                      </p>
                    </td>
                    <td class="text-center">
                      <a href="{{ route('backend.news_edit_form', [$page_num, $row['id']]) }}" onclick="onLoad('{{ route('backend.news_edit_form', [$page_num, $row['id']]) }}')">
                        <i class="fas fa-pencil-alt text-gray-dark"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  <!-- End loop users table -->
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        @include('backend.layouts.modal')

      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
    var table = $("#news_table").DataTable({
          'stateSave': true,  // Awesome that can remember page of dataTable
          'ordering': false,
          // 'scrollX': true,
           'language': {"decimal": "",
               "emptyTable":     "ยังไม่มีข่าว",
               "info":           "แสดงลำดับ _START_ ถึง _END_ จาก _TOTAL_ ข่าว",
               "infoEmpty":      "แสดงลำดับที่ 0 ถึง 0 จาก 0 ข่าว",
               "infoFiltered":   "(กรองข้อมูลจาก _MAX_ ข่าวทั้งหมด)",
               "infoPostFix":    "",
               "thousands":      ",",
               "lengthMenu":     "แสดง _MENU_ &nbspข่าว",
               "loadingRecords": "กำลังโหลด...",
               "processing":     "กำลังประมวลผล...",
               "search":         "ค้นหา:",
               "zeroRecords":    "ไม่ตรงกับข้อมูลใดๆ",
               "paginate": {
                   "first":      "แรก",
                   "last":       "สุดท้าย",
                   "next":       "ถัดไป",
                   "previous":   "ก่อนหน้า"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               },
             },
             'drawCallback': function(){
               $("img.lazyload").lazyload(); // Add lazy load image
            }
    });
</script>
</body>
</html>
