<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"><i class="fas fa-newspaper"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('backend.news_manage', 1) }}" onclick="onLoad('{{ route('backend.news_manage',1) }}')">ข่าวสาร</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">

                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title" onclick="alertSelect()"><i class="nav-icon fas fa-plus"></i>&nbsp เพิ่มข่าวสาร</h3>
                        <button type="button" onclick="onLoad('{{ route('backend.news_manage', $page_num) }}')" class="btn btn-default btn-sm float-right">
                            <i class="fas fa-chevron-left"> ย้อนกลับ</i>
                        </button>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="form_add_news" method="post" action="{{ route('backend.news_add') }}" onsubmit="setSelect2Value()" enctype="multipart/form-data">
                        @csrf

                        <!-- Hidden page_num value -->
                        <input type="hidden" value="{{ $page_num }}" name="page_num">

                        <div class="container">
                            <div class="card-body">

                                <!-- Text box for input text -->
                                <div class="form-group row">
                                    <div class="input-group col-12">
                                        <label for="th_news_name" class="col-12 ml-n1 control-label">หัวข้อข่าว <span class="text-danger"> *</span></label>
                                        <input type="text" name="th_news_name" id="th_news_name" class="form-control @error('th_news_name') is-invalid @enderror" placeholder="หัวข้อข่าว" value="{{ old('th_news_name') }}"
                                          oninvalid="this.setCustomValidity('กรุณากรอกหัวข้อข่าว (ภาษาไทย)')" oninput="this.setCustomValidity('')" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-file-signature"></span>
                                            </div>
                                        </div>
                                        @error('th_news_name')
                                        <span class="invalid-feedback ml-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Text box for input text -->
                                <div class="form-group row">
                                    <div class="input-group col-12">
                                        <label for="author_name" class="col-12 ml-n1 control-label">
                                            ชื่อผู้เขียน<span class="text-danger"> *</span>
                                        </label>
                                        <input type="text" name="author_name" id="author_name" class="form-control @error('author_name') is-invalid @enderror" placeholder="ชื่อผู้เขียน" value="{{ old('author_name') }}"
                                          oninvalid="this.setCustomValidity('กรุณากรอกชื่อผู้เขียน')" oninput="this.setCustomValidity('')" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                        @error('author_name')
                                        <span class="invalid-feedback ml-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <label>ข้อความ (ภาษาไทย)<span class="text-danger"> * </span></label>
                                        <textarea id="news_detail_th" name="news_detail_th" class="textarea @error('news_detail_th') is-invalid @enderror" placeholder="Place some text here"
                                          style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        @error('news_detail_th')
                                        <span class="text-danger ml-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 control-label">อัปโหลดรูปข่าว<span class="text-red"> *</span></label>
                                    <!-- Thai image input -->
                                    <div class="col-lg-5 col-lg-offset-5 text-center">
                                        <strong>เลือกรูปข่าว </strong><br><small>( ขนาด : 1280 x 640 px )</small>
                                        <br />
                                        <input type="file" class="custom-file-input @error('news_image') is-invalid @enderror" name="news_image" id="upload_input_lg_th" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px"
                                          oninvalid="this.setCustomValidity('กรุณาเลือกรูปข่าว')" oninput="this.setCustomValidity('')" required>
                                        <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_lg_th" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                        @error('news_image')
                                        <span class="text-danger ml-2 mt-5" role="alert">
                                            <strong class="mt-5">{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-5">
                                        <!-- /. hidden_image base64 -->
                                        <input type="hidden" name="news_lg_hidden_th" id="news_lg_hidden_th">
                                        <div class="text-center mt-4" style="margin-top:30px">
                                            <div id="upload_display_lg_th" style="width:100%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container mb-4 px-4">
                                    <!-- <button type="button" class="btn btn-success float-right" value="SEND">SEND</button> -->
                                    <button id="submit_btn" type="submit" class="btn btn-success float-right"><i class="fas fa-plus"></i>&nbsp เพิ่มข่าวสาร</button>
                                    <button onclick="onLoad('{{ route('backend.news_manage', $page_num) }}')" type="button" class="btn btn-secondary">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                    <!-- </form> -->

                    @include('backend.layouts.modal')

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->

    <!-- Custom javascript for this page -->
    <script type="text/javascript">
        // Summernote
        $('#news_detail_th').summernote({
            height: 250,
            placeholder: 'กรอกเนื้อหาข่าว...',
            lang: "th-TH",
            toolbar: [
                // ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['fontname', ['fontname']],
                // ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                // ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                // ['insert', ['picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']],
            ],
        });

        // Croppie for news image
        $uploadCrop = $('#upload_display_lg_th').croppie({
            enableExif: true,
            url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
            viewport: {
                width: 257,
                height: 129,
                type: 'rectangle'
            },
            boundary: {
                width: 325,
                height: 200
            }
        });

        $('#upload_input_lg_th').on('change', function() {
            var reader = new FileReader();
            reader.onload = function(e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('ตัดครอปรูปแล้ว');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });

        $("#submit_btn").click(function() {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: {
                    width: 1280
                }
            }).then(function(resp) {
                image = resp;
                $('#news_lg_hidden_th').val(image);
                // console.log(resp);
            });
        });
    </script>
    <!-- .endCustom javascript for this page -->

</body>

</html>
