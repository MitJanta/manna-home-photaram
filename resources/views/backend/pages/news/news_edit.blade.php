<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"><i class="fas fa-newspaper"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('backend.news_manage', $page_num) }}" onclick="onLoad('{{ route('backend.news_manage', 1) }}')">ข่าวสาร</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="card card-warning">
                    <!-- card-head -->
                    <div class="card-header">
                        <h3 class="card-title"><i class="nav-icon fas fa-pen"></i>&nbsp แก้ไขนื้อหาข่าวสาร</h3>
                        <br><small>อัปเดตล่าสุด : {{ date('d/m/Y', strtotime($news->updated_at)) }} <i class="far fa-clock"></i> {{ date('H:i:s', strtotime($news->updated_at)) }}</small>
                        <button type="button" onclick="onLoad('{{ route('backend.news_manage', $page_num) }}')" class="btn btn-default btn-sm float-right">
                            <i class="fas fa-chevron-left"> ย้อนกลับ</i>
                        </button>
                    </div>
                    <!-- .end card-header -->

                    <!-- form start -->
                    <form id="form_add_news" method="post" action="{{ route('backend.news_edit') }}" onsubmit="setSelect2Value()" enctype="multipart/form-data">
                        @csrf
                        <!-- Hidden type value -->
                        <input type="hidden" value="{{ $page_num }}" name="page_num">

                        <div class="container">
                            <div class="card-body">

                                <!-- #Send News id by hidden input -->
                                <input type="hidden" name="id" value="{{ $news->id }}">

                                <div class="form-group row">
                                    <!-- News title input TH-->
                                    <div class="input-group col-12">
                                        <label for="th_news_name" class="col-lg-12 pl-1 control-label">หัวข้อข่าว <small>(ไทย)</small></label>
                                        <input type="text" name="th_news_name" id="th_news_name" value="{{ $news->title }}" class="form-control @error('th_news_name') is-invalid @enderror" placeholder="หัวข้อข่าว"
                                          oninvalid="this.setCustomValidity('กรุณากรอกหัวข้อข่าว (ภาษาไทย)')" oninput="this.setCustomValidity('')" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-file-signature"></span>
                                            </div>
                                        </div>
                                        @error('th_news_name')
                                        <span class="invalid-feedback ml-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <!-- .end News title input TH -->

                                    <!-- News author name input TH-->
                                    <div class="input-group col-12 mt-3">
                                        <label for="author_name" class="col-lg-12 pl-1 control-label">ผู้เขียนข่าว</label>
                                        <input type="text" name="author_name" id="author_name" value="{{ $news->author }}" class="form-control @error('author_name') is-invalid @enderror" placeholder="ชื่อผู้เขียนข่าว"
                                          oninvalid="this.setCustomValidity('กรุณากรอกชื่อผู้เขียนข่าว')" oninput="this.setCustomValidity('')" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                        @error('author_name')
                                        <span class="invalid-feedback ml-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <!-- .end News author name input TH -->
                                </div>

                                <div class="form-group row">
                                    <!-- News detail TH input -->
                                    <div class="col-12">
                                        <label>ข้อความ (ภาษาไทย)</label>
                                        <textarea id="news_detail_th" name="news_detail_th" class="textarea @error('news_detail_th') is-invalid @enderror" placeholder="Place some text here"
                                          style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $news->content }}</textarea>
                                        @error('news_detail_th')
                                        <span class="text-danger ml-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <!-- .end News detail TH input -->
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-2 control-label">อัปโหลดรูปปกข่าว</label>
                                    <!-- News image input -->
                                    <div class="col-xl-5 col-xl-offset-5 text-center">
                                        <strong>เลือกรูปปกข่าวใหม่ </strong><br><small>( ขนาด : 1280 x 640 px )</small>
                                        <br />
                                        <input type="file" class="custom-file-input" name="news_image" id="upload_input_lg_th" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                        <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_lg_th" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                        @error('news_image')
                                        <span class="text-danger ml-2 mt-5" role="alert">
                                            <strong class="mt-5">{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <!-- #hidden_image base64 -->
                                        <input type="hidden" name="news_lg_hidden_th" id="news_lg_hidden_th">

                                        <div class="text-center mt-4" style="margin-top:30px">
                                            <div id="upload_display_lg_th" style="width:100%;"></div>
                                        </div>
                                    </div>
                                    <!-- .end News image input -->

                                    <!-- Old news image show -->
                                    <div class="col-xl-5 text-center">
                                        <strong>รูปปกข่าวเดิม <small>กดที่รูปเพื่อดู <i class="fas fa-external-link-alt"></i></small></strong>
                                        <br>
                                        <div class="" id="grid">
                                            <a href="{{ config('app.url') }}files_upload/news_acts/{{ $news->cover }}" target="_blank" style="cursor: zoom-in">
                                                <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/news_acts/{{ $news->cover }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                            </a>
                                        </div>
                                    </div>
                                    <!-- .end Old news image show -->
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                          <div class="container mb-3 mt-2 px-4">
                              <button id="submit_btn" type="submit" class="btn btn-warning float-right"><i class="fas fa-save"></i>&nbsp บันทึกการแก้ไข</button>
                              <button onclick="onLoad('{{ route('backend.news_manage', $page_num) }}')" type="button" class="btn btn-secondary">ยกเลิก</button>
                              <button id="delete_news_btn" type="button" class="btn btn-danger" style="cursor:pointer;" data-toggle="modal" data-target="#modal-news-alert"
                                data-delete-link="{{ route('backend.remove_news', [$page_num, $news->id]) }}"><i class="fas fa-trash-alt"></i> ลบข่าว</button>
                          </div>
                        </div>
                    </form>
                    <!-- </form> -->
                </div>
                <!-- card div-->

                <!-- Add image slide Panel-Add -->
                <div class="card card-info mt-10">
                  <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-images"></i> &nbspรูปสไลด์ Gallery</h3>
                    <button type="button" class="btn bg-light btn-sm float-right" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>

                  </div>
                  <div class="card-body">
                        <div class="row">
                        <!-- Video name input -->
                        <div class="info-box col-12 h-100 row" style="padding: 20px;background-color:#E4E4E4">
                          <h5 align="center col-12" style="line-height: 1.5em;">
                                อัปโหลดรูปสไลด์</br>
                                <small align="center" class="text-center text-danger mt-2">นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB ต่อไฟล์ และอัปโหลดได้สูงสุด 20 ภาพเท่านั้น
                                    <!-- <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต" data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB ต่อไฟล์"
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-danger" style="cursor: pointer"></i>
                                    </span> -->
                                </small>
                            </h5>
                          <!-- Initial config & Manage by Dropzone -->
                          <div id="dropzoneForm1" class="p-4 d-block text-bold text-center dropzone w-100" style="font-size: 1.2em; color: rgba(0,0,0,.3);border-style: dotted;border-radius: .5em">

                          </div>
                          <input type="hidden" id="gallery" value="{{ $news->gallery }}">
                          <br />
                          <div align="center">
                              <button type="button" class="btn btn-info d-none" id="submit-all"><i class="fas fa-file-upload"></i> &nbsp;อัปโหลด</button>
                          </div>
                        </div>

                        <!-- News slide image table -->
                        <div class="col-12 p-lg-4">
                        <label class="mt-lg-1 mt-3 control-label">
                          <big><i class="fas fa-images text-info"></i> &nbsp;รูปสไลด์</big>
                        </label>
                        <div id="preview" class="text-center mt-3">
                          <h5>รูปสไลด์ Gallery (0/20 รูป)</br></h5>
                        </div>
                        </div>
                      </div>
                    </div>
                  <!-- .end card-body -->
                </div>
                <!-- .end card add gallery image -->

                @include('backend.layouts.modal')

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->

    <!-- Custom javascript for this page -->
    <script type="text/javascript">
        // Summernote
        $('#news_detail_th').summernote({
            height: 250,
            placeholder: 'กรอกเนื้อหาข่าว...',
            lang: "th-TH",
            toolbar: [
                // ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                // ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                // ['insert', ['picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']],
            ],
        })

        // Croppie for news image
        $uploadCrop = $('#upload_display_lg_th').croppie({
            enableExif: true,
            url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
            viewport: {
                width: 257,
                height: 129,
                type: 'rectangle'
            },
            boundary: {
                width: 325,
                height: 200
            }
        });

        $('#upload_input_lg_th').on('change', function() {
            var reader = new FileReader();
            reader.onload = function(e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('ตัดครอปรูปแล้ว');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });

        $("#submit_btn").click(function() {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: {
                    width: 1280
                }
            }).then(function(resp) {
                image = resp;
                $('#news_lg_hidden_th').val(image);
                // console.log(resp);
            });
        });

        // lightGallery plugin init
        lightGallery(document.getElementById('grid'), {
            selector: 'a',
            mode: 'lg-slide',
            cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            width: '991px',
            thumbnail: true,
            animateThumb: true,
            showThumbByDefault: true,
            // iframeMaxWidth: 'auto',
            download: false,
            loop: true,
            zoom: true,
            scale: 1.8,
            fullScreen: true,
            // rotate: true
        });

        // Dropzone Initail & Setting
    Dropzone.autoDiscover = false;
    $(document).on("ready", Dropzone.discover);

    $("div#dropzoneForm1").dropzone({
        url: "{{ route('backend.upload_gallery_image') }}",
        method:"POST",
        autoProcessQueue: true,
        withCredentials: true,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        // acceptedFiles : "image/*", // อนุญาตให้เลือกไฟล์ประเภทรูปภาพได้
        acceptedFiles: ".png,.jpg,.jpeg",
        parallelUploads: 5, // ให้ทำงานพร้อมกัน 5 ไฟล์
        maxFiles: 20, // ไฟล์สูงสุด 5 ไฟล์
        maxFilesize: 2, // MB
        addRemoveLinks: true, // อนุญาตให้ลบไฟล์ก่อนการอัปโหลด
        dictRemoveFile: '<span class="text-danger" style="">X นำรูปภาพออก</span>', // ชื่อ ปุ่ม remove
        dictCancelUpload: "ยกเลิก", // ชื่อ ปุ่ม ยกเลิก
        dictDefaultMessage: "เลือกไฟล์รูปภาพ <i class='fas fa-file-image'></i> หรือ ลากไฟล์รูปภาพมาวางที่นี่ <i class='fas fa-images'></i>", // ข้อความบนพื้นที่แสดงรูปจะแสดงหลังจากโหลดเพจเสร็จ
        dictFileTooBig: "ไม่อนุญาตให้อัปโหลดไฟล์เกิน 2 MB", //ข้อความแสดงเมื่อเลือกไฟล์ขนาดเกินที่กำหนด
        resizeWidth: 1280,
        init: function() {
            console.log('Init success');
            var submitButton = document.querySelector('#submit-all');
            var myDropzone = this;
            submitButton.addEventListener("click", function() {
                myDropzone.processQueue();
            });
            this.on("success", function(file, response) {
                // console.log(response);
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    var _this = this;
                    _this.removeAllFiles();
                }
                list_image('{{ $news->gallery }}');
            });

            this.on("sending", function(file, xhr, formData) {
                // console.log(formData);
                formData.append("gallery", $("#gallery").val());
            });

            this.on('error', function(file, response) {
                $(file.previewElement).find('.dz-error-message').text(response);
            });
        },
    });

    list_image('{{ $news->gallery }}');

    // Load list รูป gallery
    function list_image(gallery) {
        $.ajax({
            url: "{{ route('backend.load_news_gallery_list') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            data: {
                gallery: gallery
            },
            success: function(data) {
                // ดักเช็ค required ไฟล์แนบอย่างน้อย 1 ไฟล์
                $('#preview').html(data);
                // lightGallery plugin init
                lightGallery(document.getElementById('grid2'), {
                    selector: '.col-lg-3 .file .image',
                    mode: 'lg-slide',
                    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                    width: '991px',
                    thumbnail: true,
                    animateThumb: true,
                    showThumbByDefault: true,
                    // iframeMaxWidth: 'auto',
                    download: false,
                    loop: true,
                    zoom: true,
                    scale: 1.8,
                    fullScreen: true,
                    // rotate: true
                });
            }
        });
    }

    // ปุ่มลบรูป gallery
    $(document).on('click', '.remove_image', function() {
      var name = $(this).attr('id');
      var gallery_name = $(this).attr('gallery_name');
        $.ajax({
            url: "{{ route('backend.delete_news_image_attache') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            data: {
              name: name,
            },
            success: function(data) {
              // console.log(data);
              if (data) {
                list_image(gallery_name);
              }
            }
        })
    });
    </script>
    <!-- .endCustom javascript for this page -->

</body>

</html>
