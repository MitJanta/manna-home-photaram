<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> <i class="fas fa-home"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">แผงควบคุม</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
              <div class="card card-warning">
                <div class="card-header">
                  <h3 class="card-title">แก้ไขข้อมูล{{ $page_th }}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="send_form" class="form-horizontal" action="{{ route('backend.home_divider_edit') }}" onsubmit="onLoad()" method="post">
                  @csrf

                  <div class="container">
                    <div class="card-body">

                      <div class="form-group row mt-3">
                          <label class="col-sm-2 control-label">เปลี่ยนรูปแบ่งส่วน</label>
                          <!-- Home Banner input -->
                          <div class="col-sm-10 text-center">
                              <strong>เลือกรูปแบ่งส่วนใหม่ </strong><br><small>( ขนาด : 1920 x 498 px )</small>
                              <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                    </span>
                              <br />
                              <br />
                              <input type="file" class="custom-file-input" name="upload_input_divider" id="upload_input_divider" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                              <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_divider" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                              @error('upload_input_help_us')
                              <span class="text-danger ml-2 mt-5" role="alert">
                                  <strong class="mt-5">{{ $message }}</strong>
                              </span>
                              @enderror

                              <!-- #hidden_image base64 -->
                              <input type="hidden" name="divider_data" id="divider_data">

                              <div class="text-center mt-4" style="margin-top:30px">
                                  <div id="upload_display_divider" style="width:100%;"></div>
                              </div>
                          </div>
                          <!-- .end News image input -->

                          <div class="col-sm-2">

                          </div>

                          <!-- Old news image show -->
                          <div class="col-sm-10 text-center">
                              <strong>รูปแบ่งส่วนเดิม <small>กดที่รูปเพื่อดู</small></strong>
                              <br>
                              <div class="" id="grid">
                                  <a href="{{ config('app.url') }}files_upload/contents/{{ $image_divider->image_name }}" target="_blank" style="cursor: zoom-in">
                                      <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $image_divider->image_name }}" style="max-width: 768px;width:100%;margin-top:8px;margin-bottom:8px">
                                  </a>
                              </div>
                          </div>
                          <!-- .end Old news image show -->
                      </div>

                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="container">
                      <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Croppie for news image
$uploadCrop5 = $('#upload_display_divider').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 66.65,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 150
    }
});

$('#upload_input_divider').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop5.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop5.croppie('result', {
        type: 'canvas',
        size: {
            width: 1920
        }
    }).then(function(resp) {
        image = resp;
        $('#divider_data').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
