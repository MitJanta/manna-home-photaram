<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> <i class="fas fa-home"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">แผงควบคุม</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
              <div class="card card-warning">
                <div class="card-header">
                  <h3 class="card-title">แก้ไขข้อมูล{{ $page_th }}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="send_form" class="form-horizontal" action="{{ route('backend.home_about_edit') }}" onsubmit="onLoad()" method="post">
                  @csrf

                  <div class="container">
                    <div class="card-body">

                      <div class="form-group row mt-3">
                          <label class="col-xl-2 control-label">เปลี่ยนรูป About us 1</label>
                          <!-- Home Banner input -->
                          <div class="col-xl-5 col-xl-offset-5 text-center">
                              <strong>เลือกรูป About us 1 ใหม่ </strong><br><small>( ขนาด : 768 x 1125 px )</small>
                              <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                    </span>
                              <br />
                              <br />
                              <input type="file" class="custom-file-input" name="upload_input_about_us_1" id="upload_input_about_us_1" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                              <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_about_us_1" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                              @error('upload_input_about_us_1')
                              <span class="text-danger ml-2 mt-5" role="alert">
                                  <strong class="mt-5">{{ $message }}</strong>
                              </span>
                              @enderror

                              <!-- #hidden_image base64 -->
                              <input type="hidden" name="about_us_1_data" id="about_us_1_data">

                              <div class="text-center mt-4" style="margin-top:30px">
                                  <div id="upload_display_about_us_1" style="width:100%;"></div>
                              </div>
                          </div>
                          <!-- .end News image input -->

                          <!-- Old news image show -->
                          <div class="col-xl-5 text-center">
                              <strong>รูป About us 1 เดิม <small>กดที่รูปเพื่อดู</small></strong>
                              <br>
                              <div class="" id="grid2">
                                  <a href="{{ config('app.url') }}files_upload/contents/{{ $about_data->image_name }}" target="_blank" style="cursor: zoom-in">
                                      <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $about_data->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                  </a>
                              </div>
                          </div>
                          <!-- .end Old news image show -->
                      </div>

                      <!-- About us text TH-->
                      <div class="col-12 px-0">
                          <label for="about_us_th_text" class="control-label">ข้อความ About us</label>
                          <textarea name="about_us_th_text" id="about_us_th_text" class="textarea" placeholder="ข้อความ About us TH"
                            style="width: 100%; height: 250px;">{!! $about_data->content_th !!}</textarea>
                          @error('about_us_th_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end About us text TH -->

                      <!-- About us text EN -->
                      <div class="col-12 mt-3 px-0">
                          <textarea name="about_us_en_text" id="about_us_en_text" class="textarea" placeholder="ข้อความ About us EN"
                            style="width: 100%; height: 250px;">{!! $about_data->content_en !!}</textarea>
                          @error('about_us_en_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end About us text EN -->

                      <div class="form-group row mt-3">
                          <label class="col-xl-2 control-label">เปลี่ยนรูป About us 2</label>
                          <!-- Home Banner input -->
                          <div class="col-xl-5 col-xl-offset-5 text-center">
                              <strong>เลือกรูป About us 2 ใหม่ </strong><br><small>( ขนาด : 768 x 430 px )</small>
                              <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                    </span>
                              <br />
                              <br />
                              <input type="file" class="custom-file-input" name="upload_input_about_us_2" id="upload_input_about_us_2" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                              <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_about_us_2" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                              @error('upload_input_about_us_2')
                              <span class="text-danger ml-2 mt-5" role="alert">
                                  <strong class="mt-5">{{ $message }}</strong>
                              </span>
                              @enderror

                              <!-- #hidden_image base64 -->
                              <input type="hidden" name="about_us_2_data" id="about_us_2_data">

                              <div class="text-center mt-4" style="margin-top:30px">
                                  <div id="upload_display_about_us_2" style="width:100%;"></div>
                              </div>
                          </div>
                          <!-- .end News image input -->

                          <!-- Old news image show -->
                          <div class="col-xl-5 text-center">
                              <strong>รูป About us 2 เดิม <small>กดที่รูปเพื่อดู</small></strong>
                              <br>
                              <div class="" id="grid3">
                                  <a href="{{ config('app.url') }}files_upload/contents/{{ $about_data->link_url }}" target="_blank" style="cursor: zoom-in">
                                      <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $about_data->link_url }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                  </a>
                              </div>
                          </div>
                          <!-- .end Old news image show -->
                      </div>

                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="container">
                      <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Summernote
$('#about_us_th_text').summernote({
    height: 150,
    placeholder: 'กรอกข้อความ About us (ไทย)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#about_us_en_text').summernote({
    height: 150,
    placeholder: 'กรอกข้อความ About us (EN)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Croppie for news image
$uploadCrop2 = $('#upload_display_about_us_1').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 376.43,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 450
    }
});

// Croppie for news image
$uploadCrop3 = $('#upload_display_about_us_2').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 143.6,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 220
    }
});

$('#upload_input_about_us_1').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop2.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#upload_input_about_us_2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop3.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop2.croppie('result', {
        type: 'canvas',
        size: {
            width: 768
        }
    }).then(function(resp) {
        image = resp;
        $('#about_us_1_data').val(image);
        // console.log(resp);
    });

    $uploadCrop3.croppie('result', {
        type: 'canvas',
        size: {
            width: 768
        }
    }).then(function(resp) {
        image = resp;
        $('#about_us_2_data').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid2'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid3'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
