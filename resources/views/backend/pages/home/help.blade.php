<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> <i class="fas fa-home"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">แผงควบคุม</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
              <div class="card card-warning">
                <div class="card-header">
                  <h3 class="card-title">แก้ไขข้อมูล {{ $page_th }}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="send_form" class="form-horizontal" action="{{ route('backend.home_help_edit') }}" onsubmit="onLoad()" method="post">
                  @csrf

                  <div class="container">
                    <div class="card-body">

                      <div class="form-group row mt-3">
                          <label class="col-xl-2 control-label">เปลี่ยนรูป Help us</label>
                          <!-- Home Banner input -->
                          <div class="col-xl-5 col-xl-offset-5 text-center">
                              <strong>เลือกรูป Help us ใหม่ </strong><br><small>( ขนาด : 1600 x 1390 px )</small>
                              <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                    </span>
                              <br />
                              <br />
                              <input type="file" class="custom-file-input" name="upload_input_help_us" id="upload_input_help_us" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                              <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_help_us" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                              @error('upload_input_help_us')
                              <span class="text-danger ml-2 mt-5" role="alert">
                                  <strong class="mt-5">{{ $message }}</strong>
                              </span>
                              @enderror

                              <!-- #hidden_image base64 -->
                              <input type="hidden" name="help_us_data" id="help_us_data">

                              <div class="text-center mt-4" style="margin-top:30px">
                                  <div id="upload_display_help_us" style="width:100%;"></div>
                              </div>
                          </div>
                          <!-- .end News image input -->

                          <!-- Old news image show -->
                          <div class="col-xl-5 text-center">
                              <strong>รูป Help us เดิม <small>กดที่รูปเพื่อดู</small></strong>
                              <br>
                              <div class="" id="grid">
                                  <a href="{{ config('app.url') }}files_upload/contents/{{ $help_us_data_eng->image_name }}" target="_blank" style="cursor: zoom-in">
                                      <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $help_us_data_eng->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                  </a>
                              </div>
                          </div>
                          <!-- .end Old news image show -->
                      </div>

                      <strong>ข้อมูลบัญชีธนาคาร (EN)</strong>
                      <!-- Acc num EN -->
                      <div class="input-group mt-2">
                          <label for="acc_num_en_text" class="col-lg-12 pl-1 control-label"></label>
                          <input type="text" name="acc_num_en_text" id="acc_num_en_text" value="{{ $help_us_data_eng->title_th }}" class="form-control @error('acc_num_en_text') is-invalid @enderror" placeholder="เลขบัญชี EN"
                            oninvalid="this.setCustomValidity('กรุณากรอกเลขบัญชี EN')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('acc_num_en_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end Acc num text EN -->

                      <!-- Bank name EN -->
                      <div class="input-group mt-2">
                          <label for="bank_name_en_text" class="col-lg-12 pl-1 control-label"></label>
                          <input type="text" name="bank_name_en_text" id="bank_name_en_text" value="{{ $help_us_data_eng->title_en }}" class="form-control @error('bank_name_en_text') is-invalid @enderror" placeholder="ชื่อธนาคาร EN"
                            oninvalid="this.setCustomValidity('กรุณากรอกชื่อธนาคาร EN')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('bank_name_en_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end Bank name text EN -->

                      <!-- Acc name EN -->
                      <div class="input-group mt-2 pb-4 border-bottom">
                          <label for="acc_name_en_text" class="col-lg-12 pl-1 control-label"></label>
                          <input type="text" name="acc_name_en_text" id="acc_name_en_text" value="{{ $help_us_data_eng->content_th }}" class="form-control @error('acc_name_en_text') is-invalid @enderror" placeholder="ชื่อบัญชี EN"
                            oninvalid="this.setCustomValidity('กรุณากรอกชื่อบัญชี EN')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('acc_name_en_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end Acc name text EN -->

                      <p class="mt-4"><strong>ข้อมูลบัญชีธนาคาร (ไทย)</strong></p>
                      <!-- Acc num TH -->
                      <div class="input-group mt-2">
                          <label for="acc_num_en_text" class="col-lg-12 pl-1 control-label"></label>
                          <input type="text" name="acc_num_th_text" id="acc_num_th_text" value="{{ $help_us_data_th->title_th }}" class="form-control @error('acc_num_th_text') is-invalid @enderror" placeholder="เลขบัญชี TH"
                            oninvalid="this.setCustomValidity('กรุณากรอกเลขบัญชี TH')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('acc_num_th_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end Acc num text th -->

                      <!-- Bank name th -->
                      <div class="input-group mt-2">
                          <label for="bank_name_th_text" class="col-lg-12 pl-1 control-label"></label>
                          <input type="text" name="bank_name_th_text" id="bank_name_th_text" value="{{ $help_us_data_th->title_en }}" class="form-control @error('bank_name_th_text') is-invalid @enderror" placeholder="ชื่อธนาคาร TH"
                            oninvalid="this.setCustomValidity('กรุณากรอกชื่อธนาคาร TH')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('bank_name_th_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end Bank name text th -->

                      <!-- Acc name th -->
                      <div class="input-group mt-2">
                          <label for="acc_name_th_text" class="col-lg-12 pl-1 control-label"></label>
                          <input type="text" name="acc_name_th_text" id="acc_name_th_text" value="{{ $help_us_data_th->content_th }}" class="form-control @error('acc_name_th_text') is-invalid @enderror" placeholder="ชื่อบัญชี TH"
                            oninvalid="this.setCustomValidity('กรุณากรอกชื่อบัญชี TH')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('acc_name_th_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end Acc name text th -->

                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="container">
                      <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Croppie for news image
$uploadCrop4 = $('#upload_display_help_us').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 223.27,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 300
    }
});

$('#upload_input_help_us').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop4.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop4.croppie('result', {
        type: 'canvas',
        size: {
            width: 1600
        }
    }).then(function(resp) {
        image = resp;
        $('#help_us_data').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
