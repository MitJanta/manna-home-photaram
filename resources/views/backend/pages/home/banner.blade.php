<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> <i class="fas fa-home"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">แผงควบคุม</a></li>
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
              <div class="card card-warning">
                <div class="card-header">
                  <h3 class="card-title">แก้ไขข้อมูล {{ $page_th }}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="send_form" class="form-horizontal" action="{{ route('backend.home_banner_edit') }}" onsubmit="onLoad()" method="post">
                  @csrf

                  <div class="container">
                    <div class="card-body">

                      <div class="form-group row">
                          <label class="col-xl-2 control-label">เปลี่ยนรูป Banner</label>
                          <!-- Home Banner input -->
                          <div class="col-xl-5 col-xl-offset-5 text-center">
                              <strong>เลือกรูป Banner ใหม่ </strong><br><small>( ขนาด : 1920 x 1350 px )</small>
                              <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาตและคำอธิบาย" data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB และรูปจะถูก filter สีเพื่อให้อ่านง่าย ดังภาพด้านล่าง<img class='w-100' src='{{ config('app.url') }}assets/images/ex_banner.png'>"
                                      data-html='true'>
                                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                    </span>
                              <br />
                              <br />
                              <input type="file" class="custom-file-input" name="upload_input_home_banner" id="upload_input_home_banner" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                              <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_home_banner" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                              @error('upload_input_home_banner')
                              <span class="text-danger ml-2 mt-5" role="alert">
                                  <strong class="mt-5">{{ $message }}</strong>
                              </span>
                              @enderror

                              <!-- #hidden_image base64 -->
                              <input type="hidden" name="home_banner_data" id="home_banner_data">

                              <div class="text-center mt-4" style="margin-top:30px">
                                  <div id="upload_display_home_banner" style="width:100%;"></div>
                              </div>
                          </div>
                          <!-- .end News image input -->

                          <!-- Old news image show -->
                          <div class="col-xl-5 text-center">
                              <strong>รูป Banner เดิม <small>กดที่รูปเพื่อดู</small></strong>
                              <br>
                              <div class="" id="grid">
                                  <a href="{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}" target="_blank" style="cursor: zoom-in">
                                      <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                  </a>
                              </div>
                          </div>
                          <!-- .end Old news image show -->
                      </div>

                      <!-- News title input TH-->
                      <div class="input-group">
                          <label for="banner_title" class="col-lg-12 pl-1 control-label">หัวข้อ Banner</label>
                          <input type="text" name="banner_title" id="banner_title" value="{{ $banner_data->title_en }}" class="form-control @error('banner_title') is-invalid @enderror" placeholder="หัวข้อ Banner"
                            oninvalid="this.setCustomValidity('กรุณากรอกหัวข้อ Banner')" oninput="this.setCustomValidity('')" required>
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-file-signature"></span>
                              </div>
                          </div>
                          @error('banner_title')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end News title input TH -->

                      <!-- News author name input TH-->
                      <div class="col-12 px-0">
                          <label for="banner_text" class="mt-3 control-label">ข้อความ Banner</label>
                          <textarea name="banner_text" id="banner_text" class="textarea" placeholder="ข้อความ Banner"
                            style="width: 100%; height: 250px;">{!! $banner_data->content_en !!}</textarea>
                          @error('banner_text')
                          <span class="invalid-feedback ml-2" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                      <!-- .end News author name input TH -->

                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="container">
                      <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Summernote
$('#banner_text').summernote({
    height: 250,
    placeholder: 'กรอกข้อความ Banner',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Croppie for news image
$uploadCrop = $('#upload_display_home_banner').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 180.77,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 250
    }
});

$('#upload_input_home_banner').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop.croppie('result', {
        type: 'canvas',
        size: {
            width: 1920
        }
    }).then(function(resp) {
        image = resp;
        $('#home_banner_data').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
