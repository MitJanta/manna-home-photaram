<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<style media="screen">
  #filesTable_filter {
    float: right;
  }

  #filesTable_paginate {
    float: right;
  }
</style>

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-file-upload"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
    <section class="content">
      <div class="card">

        <div class="card-header">
          <form class="form-horizontal" action="{{ route('backend.file_upload.upload') }}" method="post" onsubmit="onLoad()" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <div class="row">

                <div class="input-group col-lg-5 mt-2">
                  <input type="text" name="description" id="inputFileName" class="form-control @error('description') is-invalid @enderror" placeholder="กรอกชื่อไฟล์"
                         oninvalid="this.setCustomValidity('กรุณากรอกชื่อไฟล์')" oninput="this.setCustomValidity('')" required>
                  @error('description')
                      <span class="invalid-feedback ml-2" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>

                <div class="input-group col mt-2 text-center" id="choose_file">
                  <input type="file" class="custom-file-input @error('file_upload') is-invalid @enderror" name="file_upload" id="file_upload" accept=".jpg,.jpeg,.png,.pdf,.doc,.docx" style="padding-left:60px"
                          oninvalid="this.setCustomValidity('กรุณาเลือกไฟล์')"
                          oninput="this.setCustomValidity('')" required>
                  <label class="custom-file-label text-left" for="upload" style="margin:0px 8px 0px 8px;width:auto;color:gray">
                    <!-- @error('description') ? error('description') : 'เลือกไฟล์'@enderror -->
                    เลือกไฟล์
                  </label>
                  <p class="text-left"><small> &nbsp&nbsp(ขนาดไฟล์ไม่เกิน <span class="text-danger"><b>2</b></span> MB, รูปภาพขนาดไม่เกิน <span class="text-red"><b>4000x4000</b></span> px)</small>
                    <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg,.jpeg,.png,.pdf,.doc และ .docx เท่านั้น"
                      data-html='true'>
                        <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                    </span>
                  </p>
                </div>

                <div class="col-lg-2 mt-2" id="button_upload_file">
                  <button type="submit" style="position:relative;float:right" type="button" class="btn btn-success"><i class="fas fa-plus"></i>&nbsp เพิ่มไฟล์</button>
                </div>
              </div>
            </div>

          </form>
        </div>

        <div class="card-body">
          <table id="filesTable" class="table table-bordered table-hover table-responsive-xl">
            <thead>
                <tr>
                    <th style="width: auto;min-width:80px" class="text-center">เวลาอัปไฟล์</th>
                    <th style="width: auto">ชื่อไฟล์</th>
                    <th style="width: auto">ลิงก์ไฟล์</th>
                    <th style="width: 3%" class="text-center">คัดลอก</th>
                    <th style="width: 3%" class="text-center">ดู</th>
                    <th style="width: 3%" class="text-center">ดาวน์โหลด</th>
                    <th style="width: 3%" class="text-center">ลบ</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th style="width: 15%" class="text-center">เวลาอัปไฟล์</th>
                    <th style="width: auto">ชื่อไฟล์</th>
                    <th style="width: auto">ลิงก์ไฟล์</th>
                    <th style="width: 3%" class="text-center">คัดลอก</th>
                    <th style="width: 3%" class="text-center">ดู</th>
                    <th style="width: 3%" class="text-center">ดาวน์โหลด</th>
                    <th style="width: 3%" class="text-center">ลบ</th>
                </tr>
              </tfoot>
              <tbody>
                 <!-- Loop users table -->
               @foreach($files_new_list as $row)
                  @if($row['enable'] == 1)
                  <tr>
                     <td>
                       <span class="d-none">{{ $row['created_at'] }}</span>
                       <p class="text-center">
                         <strong>
                           {!! $row['new_format_datetime'] !!}
                           </strong>
                        </p>
          					 </td>
          					 <td>
          							{{ $row['description'] }}
          					 </td>
                     <td>
                       <p id="copy{{ $row['id'] }}">{{ config('app.url') }}files_upload/files/{{ $row['file_name'] }}</p>
                     </td>
                     <td class="text-center">
                       <a href="#" onclick="copyToClipboard('#copy{{ $row['id'] }}')">
                         <i  class="far fa-copy"></i>
                       </a>
                     </td>
                    <td class="text-center">
                      <a href="{{ config('app.url') }}files_upload/files/{{ $row['file_name'] }}" target="_blank">
                        <i class="fas fa-search text-success"></i>
                      </a>
                    </td>
                    <td class="text-center">
                      <a href="{{ route('backend.file_upload.download', $row['id']) }}">
                        <i class="fas fa-download text-primary"></i>
                      </a>
                    </td>
                    <td class="text-center">
                      <i id="delete_btn" class="fas fa-trash-alt text-danger delete_btn" style="cursor:pointer;" data-toggle="modal" data-target="#modal-alert" data-delete-link="{{ route('backend.file_upload.remove', $row['id']) }}"></i>
                    </td>
          			 </tr>
                 @endif
                 @endforeach
               <!-- End loop users table -->
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      @include('backend.layouts.modal')

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
$(document).ready(function () {
    var table = $("#filesTable").DataTable({
      'stateSave': true,  // Awesome that can remember page of dataTable
      'columnDefs': [{
              'targets': [1,2,3,4,5,6], /* column index */
              'orderable': false, /* true or false */
           }],
       'language': {"decimal":        "",
           "emptyTable":     "ยังไม่มีไฟล์",
           "info":           "แสดงลำดับ _START_ ถึง _END_ จาก _TOTAL_ ไฟล์",
           "infoEmpty":      "แสดงลำดับที่ 0 ถึง 0 จาก 0 ไฟล์",
           "infoFiltered":   "(กรองข้อมูลจาก _MAX_ ไฟล์ทั้งหมด)",
           "infoPostFix":    "",
           "thousands":      ",",
           "lengthMenu":     "แสดง _MENU_ &nbspไฟล์",
           "loadingRecords": "กำลังโหลด...",
           "processing":     "กำลังประมวลผล...",
           "search":         "ค้นหา:",
           "zeroRecords":    "ไม่ตรงกับข้อมูลใดๆ",
           "paginate": {
               "first":      "แรก",
               "last":       "สุดท้าย",
               "next":       "ถัดไป",
               "previous":   "ก่อนหน้า"
           },
           "aria": {
               "sortAscending":  ": activate to sort column ascending",
               "sortDescending": ": activate to sort column descending"
         },},
    });

    table.order( [[ 0, 'desc' ]] ).draw();

    // Check file upto 2 MB
    $('#file_upload').on('change', function() {
        var size = this.files[0].size/1024/1024;
        if(size < 2){

        }else{
          $('#modal-file').modal('show');
          $('#file_upload').val('');
          $('.custom-file-label').text('เลือกไฟล์');
        }
    });
  });

  // Copy to clipboard function
  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);

    $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3500
      });

        Toast.fire({
          type: 'success',
          title: ' &nbsp&nbsp&nbsp&nbsp<big>คัดลอกลิงก์ไฟล์แล้ว</big>'
        });
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    });
  }
</script>
</body>
</html>
