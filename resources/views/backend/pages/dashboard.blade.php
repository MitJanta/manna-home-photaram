<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">แผงควบคุม <small>{{ config('app.version') }}</small></h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
                                <li class="breadcrumb-item active">แผงควบคุม</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <!-- ./col -->
                        <div class="col-sm-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>Laravel 7.0</h3>

                                    <p>Framework</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-code"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->

                        <div class="col-sm-6">
                            <div class="small-box bg-secondary">
                                <div class="inner">
                                    <h3><i class="far fa-clock" style="font-size:1.2em;padding-right:5px"> </i>
                                        <span id="Hour" style="font-size:x-large;"> </span>
                                        <span id="Minute" style="font-size:x-large;"></span>
                                        <span id="Second" style="font-size:large;"></span>
                                    </h3>
                                    <p>นาฬิกา</p>
                                </div>
                                <div class="icon">
                                    <i class="far fa-clock"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->

                        <!-- Back-end can DO List Version. 1.0.0 -->
                        <!-- Can set "collapsed-card" class for initial collapsed & change icon to plus instead -->
                        <div class="card col-12 collapse-card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="ion ion-clipboard mr-1"></i>
                                    <b>Manna : Back-end Version 1.0</b>
                                </h3>

                                <div class="card-tools">
                                    <button type="button" title="ซ่อนกล่องข้อมูล" class="btn bg-secondary btn-sm" data-card-widget="collapse">
                                        <!-- Can set "collapsed-card" class for initial collapsed & change icon to plus instead -->
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <!-- /.card-header -->
                            <div class="card-body">
                                <ul class="todo-list" data-widget="todo-list">
                                    <li>
                                        <!-- todo text -->
                                        <span>1. เมนู หน้าหลัก (Home) > <b>สามารถแก้ไข</b> ภาพ Banner, About us, Help us, เส้นแบ่งส่วน</span>
                                        <!-- Emphasis label -->
                                        <small class="badge badge-warning"><i class="fas fa-globe"></i> ข้อมูลเว็บไซต์</small>
                                    </li>

                                    <li>
                                        <span>2. เมนู เกี่ยวกับเรา (About us) > <b>สามารถแก้ไข</b> ภาพ Banner, Who we are, What we do </span>
                                        <small class="badge badge-warning"><i class="fas fa-globe"></i> ข้อมูลเว็บไซต์</small>
                                    </li>

                                    <li>
                                        <span>3. เมนู ข่าวและกิจกรรม (News&Activity) > <b>สามารถเพิ่มและแก้ไขข้อมูล</b> ภาพ Banner และข่าวสารและกิจกรรม
                                            <small class="badge badge-primary"><i class="far fa-newspaper"></i> ข่าวสาร</small>
                                    </li>

                                    <li>
                                        <span>4. เมนู ติดต่อเรา (Contact us) > <b>สามารถแก้ไข</b> ภาพ Banner, Contact us, Footer (ส่วนท้ายเว็บ)</span>
                                        <small class="badge badge-warning"><i class="fas fa-globe"></i> ข้อมูลเว็บไซต์</small>
                                    </li>

                                    <li>
                                        <span>5. เมนู การบริจาค (Donation) > <b>สามารถแก้ไข</b> ภาพ Banner, How you can help</span>
                                        <small class="badge badge-warning"><i class="fas fa-globe"></i> ข้อมูลเว็บไซต์</small>
                                    </li>

                                    <li>
                                        <span>6. เมนู ข้อมูลจากผู้ติดต่อ > <b>สามารถดูข้อมูล</b> ข้อความการติดต่อ, ข้อมูลการบริจาค</span>
                                        <small class="badge badge-info"><i class="far fa-user"></i> ข้อมูลจากผู้ติดต่อ</small>
                                    </li>

                                    <li>
                                        <span>7. เมนู อัปโหลดไฟล์ > <b>สามารถอัปโหลดไฟล์ต่างๆ</b></span>
                                        <small class="badge badge-warning"><i class="fas fa-globe"></i> ข้อมูลเว็บไซต์</small>
                                        <small class="badge badge-primary"><i class="far fa-newspaper"></i> ข่าวสาร</small>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <!-- .end Back-End doing list Version. 1.0.0 -->

                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
    // Start function for fire a small toast status
    $(function() {
        const Toast2 = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $('.alertToast').click(function() {
            Toast2.fire({
                type: 'info',
                title: ' &nbsp&nbsp&nbsp&nbspระบบจะจำรหัสการลงชื่อเข้าใช้</br>เป็นเวลา 1 วัน '
            })
        });
    });
    // End function for fire a small toast status

    function timedMsg() {
        var t = setInterval("change_time();", 1000);
    }

    function change_time() {
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();
        // if(curr_hour > 12)
        //   curr_hour = curr_hour - 12;
        document.getElementById('Hour').innerHTML = curr_hour + ' :';
        if (curr_min < 10)
            curr_min = '0' + curr_min;
        document.getElementById('Minute').innerHTML = curr_min;
        if (curr_sec < 10)
            curr_sec = '0' + curr_sec;
        document.getElementById('Second').innerHTML = ': ' + curr_sec;
    }

    timedMsg();
</script>

</html>
