<!DOCTYPE html>
<html lang="th">

@include('backend.layouts.header')

<!-- Body -->

<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('backend.layouts.topnav')
        @include('backend.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"><i class="fas fa-hands"></i> {{ $page_th }}</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
                                <li class="breadcrumb-item active">{{ $page_th }}</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                  <div class="card card-warning">
                    <div class="card-header">
                      <h3 class="card-title">แก้ไขข้อมูล{{ $page_th }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="send_form" class="form-horizontal" action="{{ route('backend.donation_how_you_can_help_edit') }}" onsubmit="onLoad()" method="post">
                      @csrf

                      <div class="container">
                        <div class="card-body">

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="th_text_text" class="mt-3 control-label">ข้อความ How you can help</label>
                              <textarea name="th_text_text" id="th_text_text" class="textarea" placeholder="กรอกข้อความ How you can help (ไทย)"
                                oninvalid="this.setCustomValidity('กรุณากรอกข้อความ How you can help (ไทย)')" oninput="this.setCustomValidity('')" required>{{ $donation->content_th }}</textarea>
                              @error('th_text_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="col-12 px-0 mt-3">
                              <textarea name="en_text_text" id="en_text_text" class="textarea" placeholder="กรอกข้อความ How you can help (EN)"
                                oninvalid="this.setCustomValidity('กรุณากรอกข้อความ How you can help (EN)')" oninput="this.setCustomValidity('')" required>{!! $donation->content_en !!}</textarea>
                              @error('en_text_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูปข้างเลขบัญชี (ไทย) Desktop</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูปข้างเลขบัญชี (ไทย) Desktop ใหม่ </strong><br><small>( ขนาด : 1024 x 1465 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_image_1" id="upload_input_image_1" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_image_1" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_image_1')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="image_data_1" id="image_data_1">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_image_1" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูปข้างเลขบัญชี (ไทย) Desktop เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $acc_data_th->image_name }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $acc_data_th->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูปข้างเลขบัญชี (ไทย) Mobile</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูปข้างเลขบัญชี (ไทย) Mobile ใหม่ </strong><br><small>( ขนาด : 1920 x 710 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_image_2" id="upload_input_image_2" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_image_2" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_image_2')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="image_data_2" id="image_data_2">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_image_2" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูปข้างเลขบัญชี (ไทย) Mobile เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $acc_data_th->link_url }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $acc_data_th->link_url }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                          <!-- What we do text TH-->
                          <div class="input-group pt-3">
                            <label for="acc_num_th_text" class="col-lg-12 pl-1 control-label">เลขบัญชี (ไทย)</label>
                              <input type="text" name="acc_num_th_text" id="acc_num_th_text" value="{{ $acc_data_th->title_th }}" class="form-control @error('acc_num_th_text') is-invalid @enderror" placeholder="กรอกเลขบัญชี (ไทย)"
                                oninvalid="this.setCustomValidity('กรุณากรอกเลขบัญชี (ไทย)')" oninput="this.setCustomValidity('')" required>
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-file-signature"></span>
                                  </div>
                              </div>
                              @error('acc_num_th_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="acc_name_th_text" class="mt-3 control-label">ชื่อบัญชี (ไทย)</label>
                              <textarea name="acc_name_th_text" id="acc_name_th_text" class="textarea" placeholder="กรอกชื่อบัญชี (ไทย)"
                                style="width: 100%; height: 250px;">{!! $acc_data_th->title_en !!}</textarea>
                              @error('acc_name_th_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="acc_th_text" class="mt-3 control-label">ข้อความ (ไทย)</label>
                              <textarea name="acc_th_text" id="acc_th_text" class="textarea" placeholder="กรอกข้อความ (ไทย)"
                                style="width: 100%; height: 250px;">{!! $acc_data_th->content_th !!}</textarea>
                              @error('acc_th_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูปข้างเลขบัญชี (EN) Desktop</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูปข้างเลขบัญชี (EN) Desktop ใหม่ </strong><br><small>( ขนาด : 1024 x 1465 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_image_3" id="upload_input_image_3" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_image_3" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_image_3')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="image_data_3" id="image_data_3">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_image_3" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูปข้างเลขบัญชี (EN) Desktop เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid3">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $acc_data_en->image_name }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $acc_data_en->image_name }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                          <div class="form-group row mt-3">
                              <label class="col-xl-2 control-label">เปลี่ยนรูปข้างเลขบัญชี (EN) Mobile</label>
                              <!-- Home Banner input -->
                              <div class="col-xl-5 col-xl-offset-5 text-center">
                                  <strong>เลือกรูปข้างเลขบัญชี (EN) Mobile ใหม่ </strong><br><small>( ขนาด : 1920 x 710 px )</small>
                                  <span tabindex="0" data-toggle="popover" data-trigger="hover" data-placement="right" data-title="นามสกุลไฟล์ที่อนุญาต " data-content="นามสกุล .jpg, .jpeg และ .png เท่านั้น ขนาดไม่เกิน 2 MB"
                                          data-html='true'>
                                            <i class="fas fa-info-circle text-warning" style="cursor: pointer"></i>
                                        </span>
                                  <br />
                                  <input type="file" class="custom-file-input" name="upload_input_image_4" id="upload_input_image_4" accept=".jpeg,.jpg,.png" style="padding-left:20px;padding-top:5px">
                                  <label class="custom-file-label mx-auto text-left text-gray" for="upload_input_image_4" style="margin-top:60px;width:auto;max-width:345px">เลือกรูป</label>
                                  @error('upload_input_image_4')
                                  <span class="text-danger ml-2 mt-5" role="alert">
                                      <strong class="mt-5">{{ $message }}</strong>
                                  </span>
                                  @enderror

                                  <!-- #hidden_image base64 -->
                                  <input type="hidden" name="image_data_4" id="image_data_4">

                                  <div class="text-center mt-4" style="margin-top:30px">
                                      <div id="upload_display_image_4" style="width:100%;"></div>
                                  </div>
                              </div>
                              <!-- .end News image input -->

                              <!-- Old news image show -->
                              <div class="col-xl-5 text-center">
                                  <strong>รูปข้างเลขบัญชี (EN) Mobile เดิม <small>กดที่รูปเพื่อดู</small></strong>
                                  <br>
                                  <div class="" id="grid4">
                                      <a href="{{ config('app.url') }}files_upload/contents/{{ $acc_data_en->link_url }}" target="_blank" style="cursor: zoom-in">
                                          <img class="border border-secondary mt-2" src="{{ config('app.url') }}files_upload/contents/{{ $acc_data_en->link_url }}" style="max-width: 400px;width:100%;margin-top:8px;margin-bottom:8px">
                                      </a>
                                  </div>
                              </div>
                              <!-- .end Old news image show -->
                          </div>

                          <!-- What we do text TH-->
                          <div class="input-group pt-3">
                            <label for="acc_num_en_text" class="col-lg-12 pl-1 control-label">เลขบัญชี (EN)</label>
                              <input type="text" name="acc_num_en_text" id="acc_num_en_text" value="{{ $acc_data_en->title_th }}" class="form-control @error('acc_num_en_text') is-invalid @enderror" placeholder="กรอกเลขบัญชี (EN)"
                                oninvalid="this.setCustomValidity('กรุณากรอกเลขบัญชี (EN)')" oninput="this.setCustomValidity('')" required>
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-file-signature"></span>
                                  </div>
                              </div>
                              @error('acc_num_en_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="acc_name_en_text" class="mt-3 control-label">ชื่อบัญชี (EN)</label>
                              <textarea name="acc_name_en_text" id="acc_name_en_text" class="textarea" placeholder="กรอกชื่อบัญชี (EN)"
                                style="width: 100%; height: 250px;">{!! $acc_data_en->title_en !!}</textarea>
                              @error('acc_name_en_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                          <!-- What we do text TH-->
                          <div class="col-12 px-0">
                            <label for="acc_en_text" class="mt-3 control-label">ข้อความ (EN)</label>
                              <textarea name="acc_en_text" id="acc_en_text" class="textarea" placeholder="กรอกข้อความ (EN)"
                                style="width: 100%; height: 250px;">{!! $acc_data_en->content_th !!}</textarea>
                              @error('acc_en_text')
                              <span class="invalid-feedback ml-2" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                          </div>
                          <!-- .end What we do text TH -->

                        </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <div class="container">
                          <button type="submit" id="submit_btn" class="btn btn-warning float-right">บันทึกการแก้ไข</button>
                        </div>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.layouts.footer')

    </div>
    <!-- ./wrapper -->
</body>

<script type="text/javascript">
// Summernote
$('#th_text_text').summernote({
    height: 150,
    placeholder: 'กรอกข้อความ How you can help (ไทย)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#en_text_text').summernote({
    height: 150,
    placeholder: 'กรอกข้อความ How you can help (EN)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#acc_name_th_text').summernote({
    height: 100,
    placeholder: 'กรอกชื่อบัญชี (ไทย)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#acc_th_text').summernote({
    height: 100,
    placeholder: 'กรอกข้อความ (ไทย)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#acc_name_en_text').summernote({
    height: 100,
    placeholder: 'กรอกชื่อบัญชี (EN)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Summernote
$('#acc_en_text').summernote({
    height: 100,
    placeholder: 'กรอกข้อความ (EN)',
    lang: "th-TH",
    toolbar: [
        // ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['fontname', ['fontname']],
        // ['color', ['color']],
        // ['para', ['ul', 'ol', 'paragraph']],
        // ['table', ['table']],
        // ['insert', ['link', 'picture', 'video']],
        // ['insert', ['picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
    ],
});

// Croppie for news image
$uploadCrop = $('#upload_display_image_1').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 367.78,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 430
    }
});

// Croppie for news image
$uploadCrop2 = $('#upload_display_image_2').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 95,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 150
    }
});

// Croppie for news image
$uploadCrop3 = $('#upload_display_image_3').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 367.78,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 430
    }
});

// Croppie for news image
$uploadCrop4 = $('#upload_display_image_4').croppie({
    enableExif: true,
    url: '{{ config('app.url') }}assets/dist/img/preview.jpg',
    viewport: {
        width: 257,
        height: 95,
        type: 'rectangle'
    },
    boundary: {
        width: 325,
        height: 150
    }
});

$('#upload_input_image_1').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#upload_input_image_2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop2.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#upload_input_image_3').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop3.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#upload_input_image_4').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(e) {
        $uploadCrop4.croppie('bind', {
            url: e.target.result
        }).then(function() {
            console.log('ตัดครอปรูปแล้ว');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$("#submit_btn").click(function() {
    $uploadCrop.croppie('result', {
        type: 'canvas',
        size: {
            width: 1024
        }
    }).then(function(resp) {
        image = resp;
        $('#image_data_1').val(image);
        // console.log(resp);
    });

    $uploadCrop2.croppie('result', {
        type: 'canvas',
        size: {
            width: 1920
        }
    }).then(function(resp) {
        image = resp;
        $('#image_data_2').val(image);
        // console.log(resp);
    });

    $uploadCrop3.croppie('result', {
        type: 'canvas',
        size: {
            width: 1024
        }
    }).then(function(resp) {
        image = resp;
        $('#image_data_3').val(image);
        // console.log(resp);
    });

    $uploadCrop4.croppie('result', {
        type: 'canvas',
        size: {
            width: 1920
        }
    }).then(function(resp) {
        image = resp;
        $('#image_data_4').val(image);
        // console.log(resp);
    });
});

// lightGallery plugin init
lightGallery(document.getElementById('grid'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid2'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid3'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});

// lightGallery plugin init
lightGallery(document.getElementById('grid4'), {
    selector: 'a',
    mode: 'lg-slide',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    width: '991px',
    thumbnail: true,
    animateThumb: true,
    showThumbByDefault: true,
    // iframeMaxWidth: 'auto',
    download: false,
    loop: true,
    zoom: true,
    scale: 1.8,
    fullScreen: true,
    // rotate: true
});
</script>

</html>
