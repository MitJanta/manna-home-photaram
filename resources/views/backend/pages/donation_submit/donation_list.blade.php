<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<style media="screen">
  #donation_table_filter {
    float: right;
  }

  #donation_table_paginate {
    float: right;
  }
</style>

<!-- Body -->
<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-envelope"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
      <section class="content">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">ตาราง : {{ $page_th }} </h3>
          </div>
          <div class="card-body">
            <table id="donation_table" class="table table-bordered table-hover table-responsive display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="all" style="min-width:50px;width:50px;">ดูข้อมูล</th>
                  <th class="all" style="min-width:105px;width:105px;max-width:105px" width="105">วันที่ส่งข้อมูล</br> <small>ปี/เดือน/วัน</small> </th>
                  <th class="all" style="min-width:200px;width:auto;">ชื่อ-นามสกุล</th>
                  <th class="all" style="min-width:200px;width:auto;">อีเมล</th>
                  <th class="all text-right" style="min-width:200px;width:auto;">จำนวนเงิน (บาท)</th>
                  <th class="all d-none" style="min-width:200px;width:auto;">บริจาคเพื่อ</th>
                  <th class="all" style="min-width:200px;width:auto;">ชำระโดย</th>
                  <th class="all d-none" style="min-width:200px;width:auto;">ข้อมูลการโอนเงิน (กรณีโอนเงิน)</th>
                  <th class="all d-none" style="min-width:200px;width:auto;">ไฟล์แนบ (กรณีโอนเงิน)</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>ดูข้อมูล</th>
                  <th>วันที่ส่งข้อมูล</th>
                  <th>ชื่อ-นามสกุล</th>
                  <th>อีเมล</th>
                  <th class="text-right">จำนวนเงิน (บาท)</th>
                  <th class="all d-none">บริจาคเพื่อ</th>
                  <th>ชำระโดย</th>
                  <th class="all d-none">ข้อมูลการโอนเงิน (กรณีโอนเงิน)</th>
                  <th class="all d-none">ไฟล์แนบ (กรณีโอนเงิน)</th>
                </tr>
              </tfoot>
              <tbody>
                <!-- Loop donation table -->
                @foreach ($donation_list as $row)
                  <tr>
                    <td>
                      <a href="{{ route('backend.donation_detail', $row['id']) }}">
                        <i class="fas fa-search text-info"></i>
                        @if($row['mark_as_read'] == 0)
                        <small class="badge badge-warning">NEW</small>
                        @endif
                      </a>
                    </td>
                    <td>
                      <p class="d-none">{{ $row['created_at'] }}</p>
                      {!! App\Helper\DateHelper::dateTimeToSlashFormat($row['created_at']) !!}
                    </td>
                    <td>
                      {{ $row['first_name'] }} {{ $row['last_name'] }}
                    </td>
                    <td>
                      @if(!empty($row['email'])) {{ $row['email'] }} @else - @endif
                    </td>
                    <td class="text-right">
                      {{ number_format($row['money_amount'], 0) }}
                    </td>
                    <td class="d-none">
                      @if(!empty($row['donate_for'])) {{ $row['donate_for'] }} @else - @endif
                    </td>
                    <td>
                      @if($row['paid_by'] == 0) เงินสด @elseif($row['paid_by'] == 1) โอนผ่านธนาคาร @endif
                    </td>
                    <td class="d-none">
                      @if($row['paid_by'] == 0) -
                      @elseif($row['paid_by'] == 1)
                        @if(!empty($row['bank_name'])) ชื่อธนาคาร : {{ $row['bank_name'] }} @endif @if(!empty($row['bank_other'])) ธนาคารอื่นๆ ระบุชื่อธนาคาร : {{ $row['bank_other'] }} @endif @if(!empty($row['bank_branch'])) สาขา : {{ $row['bank_branch'] }} @endif @if(!empty($row['date'])) {!! App\Helper\DateHelper::dateToThaiFormat($row['date']) !!} @endif
                      @endif
                    </td>
                    <td class="d-none">
                      @if(!empty($row['attache_file'])) <a href="{{ config('app.url').'files_upload/donate_attaches/'.$row['attache_file'] }}">{{ config('app.url').'files_upload/donate_attaches/'.$row['attache_file'] }}</a> @else - @endif
                    </td>
                  </tr>
                  @endforeach
                  <!-- End loop users table -->
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        @include('backend.layouts.modal')

      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">
    $("#donation_table").DataTable({
          'stateSave': true,  // Awesome that can remember page of dataTable
          'ordering': true,
          'searching': true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "lengthChange" : true, //thought this line could hide the LengthMenu
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ],
          // 'scrollX': true,
           'language': {"decimal": "",
               "emptyTable":     "ยังไม่มีข้อมูล",
               "info":           "แสดงลำดับ _START_ ถึง _END_ จาก _TOTAL_ ข้อมูล",
               "infoEmpty":      "แสดงลำดับที่ 0 ถึง 0 จาก 0 ข้อมูล",
               "infoFiltered":   "(กรองข้อมูลจาก _MAX_ ข้อมูลทั้งหมด)",
               "infoPostFix":    "",
               "thousands":      ",",
               "lengthMenu":     "แสดง _MENU_ &nbspข้อมูล",
               "loadingRecords": "กำลังโหลด...",
               "processing":     "กำลังประมวลผล...",
               "search":         "ค้นหา:",
               "zeroRecords":    "ไม่ตรงกับข้อมูลใดๆ",
               "paginate": {
                   "first":      "แรก",
                   "last":       "สุดท้าย",
                   "next":       "ถัดไป",
                   "previous":   "ก่อนหน้า"
               },
               "aria": {
                   "sortAscending":  ": activate to sort column ascending",
                   "sortDescending": ": activate to sort column descending"
               },
             },
    });
</script>
</body>
</html>
