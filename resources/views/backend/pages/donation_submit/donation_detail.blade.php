<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('backend.layouts.header')

<!-- Body -->
<body onload="onLoadBody()" class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('backend.layouts.topnav')
@include('backend.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="fas fa-envelope"></i> {{ $page_th }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">Home</a></li> -->
            <li class="breadcrumb-item active">{{ $page_th }}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
      <section class="content">

        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">{{ $page_th }}</h3>
            <button type="button" onclick="onLoad('{{ route('backend.donation_list') }}')" class="btn btn-default btn-sm float-right">
                <i class="fas fa-chevron-left"> ย้อนกลับ</i>
            </button>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3 font-weight-bold text-md-right text-left">
                เวลาที่ส่งข้อมูล
              </div>
              <div class="col-md-9">
                {!! $created_datetime !!}
              </div>

              <div class="col-md-3 font-weight-bold text-md-right text-left">
                ชื่อ-นามสกุล
              </div>
              <div class="col-md-9">
                {{ $donation_detail->first_name }} {{ $donation_detail->last_name }}
              </div>

              <div class="col-md-3 font-weight-bold text-md-right text-left">
                อีเมล
              </div>
              <div class="col-md-9">
                @if(!empty($donation_detail->email)) {{ $donation_detail->email }} @else - @endif
              </div>

              <div class="col-md-3 font-weight-bold text-md-right text-left">
                บริจาคเพื่อ
              </div>
              <div class="col-md-9">
                @if(!empty($donation_detail->donate_for)) {{ $donation_detail->donate_for }} @else - @endif
              </div>

              <div class="col-md-3 font-weight-bold text-md-right text-left">
                จำนวนเงิน
              </div>
              <div class="col-md-9">
                @if(!empty($donation_detail->money_amount)) {{ number_format($donation_detail->money_amount, 0) }} บาท @else - @endif
              </div>

              <div class="col-md-3 font-weight-bold text-md-right text-left mt-3">
                ชำระโดย
              </div>
              <div class="col-md-9 mt-3">
                @if($donation_detail->paid_by == 0) เงินสด @elseif($donation_detail->paid_by == 1) โอนผ่านธนาคาร @endif
              </div>

              @if($donation_detail->paid_by == 1)
                <div class="col-md-3 font-weight-bold text-md-right text-left">
                  ธนาคาร
                </div>
                <div class="col-md-9">
                  {{ $bank_name }}
                </div>

                @if(!empty($donation_detail->bank_other))
                <div class="col-md-3 font-weight-bold text-md-right text-left">
                  ธนาคารอื่นๆ ระบุ
                </div>
                <div class="col-md-9">
                  {{ $donation_detail->bank_other }}
                </div>
                @endif

                <div class="col-md-3 font-weight-bold text-md-right text-left">
                  สาขา
                </div>
                <div class="col-md-9">
                  {{ $donation_detail->bank_branch }}
                </div>

                <div class="col-md-3 font-weight-bold text-md-right text-left">
                  วันที่และเวลาโอนเงิน
                </div>
                <div class="col-md-9">
                  {!! App\Helper\DateHelper::dateToThaiFormat($donation_detail->date) !!}
                </div>

                @if(!empty($donation_detail->attache_file))
                <div class="col-md-3 font-weight-bold text-md-right text-left">
                  หลักฐานการโอนเงิน
                </div>
                <div class="col-md-9">
                  <a href="{{ config('app.url').'files_upload/donate_attaches/'.$donation_detail->attache_file }}" target="_blank">เปิดไฟล์ในแท็บใหม่ <small><i class="fas fa-external-link-alt"></i></small> </a>
                </div>
                @endif
              @endif
            </div>

          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->

        @include('backend.layouts.modal')

      </section>
      <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('backend.layouts.footer')

</div>
<!-- ./wrapper -->
<script type="text/javascript">

</script>
</body>
</html>
