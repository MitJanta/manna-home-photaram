<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('backend.dashboard') }}" onclick="onLoad('{{ route('backend.dashboard') }}')" class="brand-link">
        <img src="{{ config('app.url') }}assets/images/favicon/android-icon-192x192.png" alt="Logo" title="manna_logo" class="brand-image elevation-3" style="opacity: .8; max-height: 30px">
        <small class="text-center brand-text font-weight-light">Admin : {{ substr(config('app.name'), 0, 15) }} @if(strlen(config('app.name')) > 15) ... @endif</small>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <i class="fas @if(Auth::guard('admin')->user()->isSuperAdmin() === 1) fa-user-cog @else fa-user @endif text-white" style="font-size: 2rem"></i>
            </div>
            <div class="info">
                <span class="text-white">{{ Auth::guard('admin')->user()->name }}</span>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('backend.dashboard') }}" onclick="onLoad('{{ route('backend.dashboard') }}')" class="nav-link @if(Route::is('backend.dashboard') || Route::is('backend.index')) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            แผงควบคุม
                        </p>
                    </a>
                </li>

                @if(Auth::guard('admin')->user()->isPermission2())
                <li class="nav-item {{ Route::is('backend.home_banner_edit_form') || Route::is('backend.home_about_edit_form') || Route::is('backend.home_help_edit_form') || Route::is('backend.home_divider_edit_form') ? ' menu-open' : '' }}">
                    <a href="#n"
                      class="nav-link {{ Route::is('backend.home_banner_edit_form') || Route::is('backend.home_about_edit_form') || Route::is('backend.home_help_edit_form') || Route::is('backend.home_divider_edit_form') ? ' active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>หน้าหลัก<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.home_banner_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.home_banner_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Banner</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.home_about_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.home_about_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>About us</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.home_help_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.home_help_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Help us</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.home_divider_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.home_divider_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>ภาพเส้นแบ่งส่วน</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::guard('admin')->user()->isPermission2())
                <li class="nav-item  {{ Route::is('backend.about_banner_edit_form') || Route::is('backend.about_who_we_are_edit_form') || Route::is('backend.about_what_we_do_edit_form') ? ' menu-open' : '' }}">
                    <a href="#n" class="nav-link {{ Route::is('backend.about_banner_edit_form') || Route::is('backend.about_who_we_are_edit_form') || Route::is('backend.about_what_we_do_edit_form') ? ' active' : '' }}">
                        <i class="nav-icon fas fa-address-card"></i>
                        <p>เกี่ยวกับเรา<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.about_banner_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.about_banner_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Banner</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.about_who_we_are_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.about_who_we_are_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Who we are</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.about_what_we_do_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.about_what_we_do_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>What we do</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::guard('admin')->user()->isPermission3())
                <li class="nav-item @if(Route::is('backend.news_banner_edit_form') || Route::is('backend.news_manage') || Route::is('backend.news_add_form') || Route::is('backend.news_edit_form')) menu-open @endif">
                    <a href="#n" class="nav-link @if(Route::is('backend.news_banner_edit_form') || Route::is('backend.news_manage') || Route::is('backend.news_add_form') || Route::is('backend.news_edit_form')) active @endif">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>ข่าวและกิจกรรม<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.news_banner_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.news_banner_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Banner</p>
                            </a>
                        </li>
                        <li class="nav-item">
                              <a href="{{ route('backend.news_manage', 1) }}" onclick="" class="nav-link {{ Route::is('backend.news_manage') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>จัดการข่าว</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::guard('admin')->user()->isPermission2())
                <li class="nav-item {{ Route::is('backend.contact_banner_edit_form') || Route::is('backend.contact_data_edit_form') || Route::is('backend.footer_edit_form') ? ' menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Route::is('backend.contact_banner_edit_form') || Route::is('backend.contact_data_edit_form') || Route::is('backend.footer_edit_form') ? ' active' : '' }}">
                        <i class="nav-icon far fa-address-card"></i>
                        <p>ติดต่อเรา<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.contact_banner_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.contact_banner_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Banner</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.contact_data_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.contact_data_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Contact us</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.footer_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.footer_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Footer</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::guard('admin')->user()->isPermission2())
                <li class="nav-item {{ Route::is('backend.donation_banner_edit_form') || Route::is('backend.donation_how_you_can_help_edit_form')  ? 'menu-open' : '' }}">
                    <a href="#n" class="nav-link  {{ Route::is('backend.donation_banner_edit_form') || Route::is('backend.donation_how_you_can_help_edit_form')  ? 'active' : '' }}">
                        <i class="nav-icon fas fa-hands"></i>
                        <p>การบริจาค<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.donation_banner_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.donation_banner_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>Banner</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.donation_how_you_can_help_edit_form') }}" onclick="" class="nav-link {{ Route::is('backend.donation_how_you_can_help_edit_form') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>How you can help</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::guard('admin')->user()->isPermission4())
                <li class="nav-item {{ Route::is('backend.contact_list') || Route::is('backend.donation_list') ? ' menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Route::is('backend.contact_list') || Route::is('backend.donation_list') ? ' active' : '' }}">
                        <i class="nav-icon fas fa-envelope"></i>
                        <p>ข้อมูลจากผู้ติดต่อ<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.contact_list') }}" onclick="" class="nav-link {{ Route::is('backend.contact_list') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>ข้อความการติดต่อ<small> (อีเมล)</small></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('backend.donation_list') }}" onclick="" class="nav-link {{ Route::is('backend.donation_list') ? 'active' : '' }}">
                                <i class="fas fa-circle nav-icon" style="font-size: 8px;opacity:0.3;"></i>
                                <p>ข้อมูลการบริจาค</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                @if(Auth::guard('admin')->user()->isPermission2() || Auth::guard('admin')->user()->isPermission3())
                <li class="nav-item">
                    <a href="{{ route('backend.file_upload') }}" target="_blank" class="nav-link @if(Route::is('backend.file_upload')) active @endif">
                        <i class="nav-icon fas fa-file-upload"></i>
                        <p>อัปโหลดไฟล์</p>
                        <i class="nav-icon fas fa-external-link-alt float-right text-gray"></i>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
