<!-- Dialog box overlay -->
<div class="modal fade" id="modal_logout">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> คุณแน่ใจที่จะออกจากระบบ ?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body text-center"> หากคุณทำการกระทำใดอยู่แล้วยังไม่ได้บันทึก
        </br><p class="text-danger">การกระทำนั้นจะถูกยกเลิกทันที <i class="fas fa-times-circle"></i></p>
      <p>กด <b>ยกเลิก</b> เพื่อกลับสู่หน้าจัดการเว็บ</br>กด <b class="text-danger"><i class="fas fa-sign-out-alt"></i> ออกจากระบบ</b> เพื่อยืนยันการออกจากระบบ.</p>
      </div>

      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
        <form method="post" action="{{ route('auth.logout') }}">
          @csrf
        <button type="submit" class="btn btn-danger" onclick="return onLoad()"><i class="fas fa-sign-out-alt"></i> ออกจากระบบ</button>
      </form>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Dialog box overlay End!! -->

<!-- Alert Delete -->
<div class="modal fade" id="modal-alert">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="modal-title"> <i class="fas fa-trash-alt"> </i>&nbsp&nbsp Delete this data ?</h4>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><br />คุณต้องการลบข้อมูลนี้ใช่หรือไม่ &hellip;</p>
      </div>
      <div class="modal-footer  justify-content-between">
        <button type="button" class="btn btn-default float-left" data-dismiss="modal" autofocus>ยกเลิก</button>
        <a id="modal_delete_btn" href="" onclick="onLoad()" style="color:#f56954;">
          <button type="button" class="btn btn-danger">ลบ</button>
        </a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Alert Delete -->
<div class="modal fade" id="modal-news-alert">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="modal-title"> <i class="fas fa-trash-alt"> </i>&nbsp&nbsp Delete this news ?</h4>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><br />คุณต้องการลบข่าวนี้ใช่หรือไม่ &hellip;<br /><small class="text-danger">โดยภาพสไลด์ของข่าวนี้จะถูกลบทั้งหมดด้วย</br>การกระทำนี้ไม่สามารถย้อนกลับได้</small></p>
      </div>
      <div class="modal-footer  justify-content-between">
        <button type="button" class="btn btn-default float-left" data-dismiss="modal" autofocus>ยกเลิก</button>
        <a id="modal_delete_news_btn" href="" onclick="onLoad()" style="color:#f56954;">
          <button type="button" class="btn btn-danger">ลบ</button>
        </a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Alert Delete -->
<div class="modal fade" id="modal-project-alert">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="modal-title"> <i class="fas fa-trash-alt"> </i>&nbsp&nbsp Delete this project ?</h4>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><br/>คุณต้องการลบข้อมูลโครงการนี้ใช่หรือไม่ &hellip;<br /><small class="text-danger">โดยภาพสไลด์ของโครงการนี้จะถูกลบทั้งหมดด้วย</small></p>
      </div>
      <div class="modal-footer  justify-content-between">
        <button type="button" class="btn btn-default float-left" data-dismiss="modal" autofocus>ยกเลิก</button>
        <a id="modal_delete_project_btn" href="" onclick="onLoad()" style="color:#f56954;">
          <button type="button" class="btn btn-danger">ลบ</button>
        </a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Alert  -->
<div class="modal modal-danger fade" id="modal-file">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title"> <i class="fas fa-file-upload"> </i>&nbsp&nbsp File upload size too large ?</h4>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <p><br /><i class="fas fa-info-circle text-info mt-3 mr-2" style="font-size:1.5rem;"> </i> ไฟล์ของท่านมีขนาดใหญ่เกินที่กำหนด &hellip;<br/><small class="ml-5 text-danger">สามารถอัปโหลดได้ไม่เกิน</small><span class="text-danger"> 2 MB.</span></small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info pull-right" data-dismiss="modal" autofocus>ตกลง</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Alert  -->
<div class="modal modal-danger fade" id="modal-image">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title"> <i class="fas fa-file-image"> </i>&nbsp&nbsp Image file upload size too large ?</h4>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <p><br /><i class="fas fa-info-circle text-info mt-3 mr-2" style="font-size:1.5rem;"> </i> ไฟล์รูปของท่านมีขนาดใหญ่เกินที่กำหนด &hellip;<br/><small class="ml-5 text-danger">สามารถอัปโหลดได้ไม่เกิน</small><span class="text-danger"> 2 MB.</span></small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info pull-right" data-dismiss="modal" autofocus>ตกลง</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Alert slide image overload size (2MB) -->
<div class="modal modal-danger fade" id="modal-slide-alert">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="modal-title"> <i class="fas fa-file-upload"> </i>&nbsp&nbsp File upload size too large ?</h4>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <p><br /><i class="fas fa-info-circle text-info mt-3 mr-2" style="font-size:1.5rem;"> </i> ไฟล์ของท่านมีขนาดใหญ่เกินที่กำหนด &hellip;<br/><small class="ml-5 text-danger">สามารถอัปโหลดได้ไม่เกิน</small><span class="text-danger"> 8 MB.</span></small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info pull-right" data-dismiss="modal" autofocus>ตกลง</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
