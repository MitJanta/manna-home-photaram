<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('backend.dashboard') }}" onclick="onLoad('{{ route('backend.dashboard') }}')" class="nav-link">หน้าหลัก</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="{{ route('home') }}" class="nav-link" target="_blank">เว็บไซต์ Manna <i class="fas fa-external-link-alt"></i> </a>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Notifications Dropdown Menu -->
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="fas fa-user-circle" style="font-size: 1.3rem"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

        <div class="dropdown-divider"></div>
        <a onclick="onLoad('{{ route('admin.reset_password') }}')" href="{{ route('admin.reset_password') }}" class="dropdown-item onload">
          <i class="fas fa-key mr-2"></i> ตั้งรหัสผ่านใหม่
        </a>

        @if(Auth::guard('admin')->user()->isPermission1())
        <div class="dropdown-divider"></div>
        <a onclick="onLoad('{{ route('admin.admin_manage') }}')" href="{{ route('admin.admin_manage') }}" class="dropdown-item onload">
          <i class="fas fa-users mr-2"></i> จัดการผู้ดูแลเว็บ<small class="float-right text-gray mt-1">บันทึกการใช้งาน</small>
        </a>
        @endif

        <div class="dropdown-divider"></div>
        <button type="button" class="dropdown-item bg-danger onload" data-toggle="modal" data-target="#modal_logout">
          <i class="fas fa-sign-out-alt mr-2"></i> ออกจากระบบ
        </button>

      </div>
    </li>
  </ul>
</nav>
<!-- /.navbar -->
