<footer class="main-footer">
  <strong>Copyright &copy; 2021 <a href="{{ config('app.url') }}" target="_blank">mannahomephotharam.com</a>.</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> {{ config('app.version', '1.0') }}
  </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<!-- jQuery -->
<script src="{{ config('app.url') }}assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ config('app.url') }}assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap .js -->
<script type="text/javascript" src="{{ config('app.url') }}assets/plugins/bootstrap-4.5.0/js/bootstrap.bundle.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ config('app.url') }}assets/plugins/inputmask/jquery.inputmask.bundle.js"></script>
<!-- Summernote -->
<script src="{{ config('app.url') }}assets/plugins/summernote/summernote-bs4.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/summernote/lang/summernote-th-TH.min.js"></script>
<!-- SweetAlert2 -->
<script src="{{ config('app.url') }}assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Select2 -->
<script src="{{ config('app.url') }}assets/plugins/select2/js/select2.full.min.js"></script>
<!-- Croppie -->
<script src="{{ config('app.url') }}assets/plugins/croppie-2.6.4/js/croppie.min.js"></script>
<!-- DataTables -->
<script src="{{ config('app.url') }}assets/plugins/datatables/datatables.min.js"></script>
<!-- daterangepicker -->
<script src="{{ config('app.url') }}assets/plugins/moment/moment.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ config('app.url') }}assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ config('app.url') }}assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ config('app.url') }}assets/dist/js/adminlte.js"></script>
<!-- lazyload image -->
<script src="{{ config('app.url') }}assets/plugins/lazy-image-loader/jquery.lazyload.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ config('app.url') }}assets/dist/js/demo.js"></script>

<!-- Light gallery JS viewer -->
<script src="{{ config('app.url') }}assets/plugins/lightgallery.js-master/dist/js/lightgallery.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/lightgallery.js-master/demo/js/lg-zoom.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/lightgallery.js-master/demo/js/lg-fullscreen.min.js"></script>

@if($page_en == 'edit_news_form')
<!-- Dropzone 5 -> Version นี้ยัง Support IE อยู่ ถ้า Version 6 จะไม่ Support -->
<script src="{{ config('app.url') }}assets/plugins/dropzone5/dropzone.min.js?=1.0"></script>
@endif

<!-- Global javascript -->
<script type="text/javascript">
$('[data-toggle="popover"]').popover();

$("img.lazyload").lazyload();

//Initialize Select2 Elements
$('.select2').select2({
  theme: 'bootstrap4',
});

// Custom input file
$(".custom-file-input").on("change", function() {
  var size = this.files[0].size / 1024 / 1024;
  if (size < 2) { // ดักไว้ 2 MB
    var fileName = $(this).val().split("\\").pop();
    // console.log(fileName);
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    $(this).siblings(".custom-file-label").addClass("text-dark");
  } else {
      $('.modal').modal('hide');
      $('#modal-max-file').modal('show');
      $(this).siblings('.custom-file-input').val('');
      $(this).siblings('.custom-file-label').text('เลือกไฟล์');
  }
});

// ประกาศเรียกใช้ SweetAlert ตาม session ที่ส่งมา
    @if(!empty(session('message')) && !empty(session('alert')) && !empty(session('type')))
      @if(session('alert') == 'toast')
      const Swalert = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
      });

      $('.alertTrigger').ready(function() {
          Swalert.fire({
              icon: '{{ session('type') }}',
              title: '{!! session('message') !!}',
          })
      });
      @elseif(!empty(session('alert')) && session('alert') == 'popup')
      // console.log('test alert 1');
      const Swalert = Swal.mixin({
          toast: false,
          position: "center",
          showConfirmButton: true,
          timer: 5000,
          showClass: {
              popup: 'animate__animated animate__zoomIn'
          },
          hideClass: {
              popup: 'animate__animated animate__zoomOut'
          },
          customClass: {
            closeButton: 'btn btn-warning',
            confirmButton: 'btn btn-warning',
            cancelButton: 'btn btn-danger'
          },
      });

      $('.alertTrigger').ready(function() {
          Swalert.fire({
              icon: '{{ session('type') }}',
              title: '{!! session('message') !!}',
          })
      });
      @elseif(!empty(session('alert')) && session('alert') == 'modal')
      $('#modal-title-dynamic').text('{!! session('title') !!}');
      $('#modal-message-dynamic').text('{!! session('message') !!}');
      $('#modal-' + {{ session('type') }}).modal('show');
      @endif
    @endif

// Function for onLoad Spinner (Button)
function onLoad(route = null){
    var list, index;
    if(route == null){
      // $('#send_form').submit();
      $('#loader_spinner').css('display','block');
    } else {
      location.href=route;
      $('#loader_spinner').css('display','block');
    }
}
// .end Function for Spinner (Button)

// Function for onLoad Spinner (Button)
function onLoadBody(){
      $('#loader_spinner2').css('display','none');
}
// .end Function for Spinner (Button)

// Remove btn
$('.delete_btn').on('click', function () {
  var delete_link = $(this).data('delete-link');
  $('#modal_delete_btn').attr('href',delete_link);
});

$('#delete_news_btn').on('click', function () {
  var delete_link = $(this).data('delete-link');
  // console.log(delete_link);
  $('#modal_delete_news_btn').attr('href',delete_link);
});

$('#delete_project_btn').on('click', function () {
  var delete_link = $(this).data('delete-link');
  // console.log(delete_link);
  $('#modal_delete_project_btn').attr('href',delete_link);
});
</script>
