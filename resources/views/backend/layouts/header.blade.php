<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(Route::is('backend.donation_list'))
    <title>{{ $page_th . ' : ' . config('app.name', 'Manna') . ' ' . date('d_m_Y H_i_s', strtotime(date('Y-m-d H:i:s'))) }}</title>
    @else
    <title>{{ $page_th . ' : ' . config('app.name', 'Manna') }}</title>
    @endif

    <!--__________________ Meta data __________________-->
  	<meta name="copyright" content="Copyright 2021 Manna Home Photharam" />
  	<meta name="author" content="Manna Home Photharam Admin Backend" />
  	<meta name="keywords" content='{{ $page_th }} : Manna Home Photharam' />
  	<meta name="Description" content='Copyright 2021 Manna Home Photharam Back-End' />
  	<meta name="robots" content="index,follow" />

  	<meta property="og:title" content='{{ $page_th }}' />
  	<meta property="og:type" content="website" />
  	<meta property="fb:pages" content="https://www.facebook.com/mannahomethai/">

  	<link rel="apple-touch-icon" sizes="57x57" href="{{ config('app.url') }}assets/images/favicon/apple-icon-57x57.png?=1.7">
  	<link rel="apple-touch-icon" sizes="60x60" href="{{ config('app.url') }}assets/images/favicon/apple-icon-60x60.png?=1.7">
  	<link rel="apple-touch-icon" sizes="72x72" href="{{ config('app.url') }}assets/images/favicon/apple-icon-72x72.png?=1.7">
  	<link rel="apple-touch-icon" sizes="76x76" href="{{ config('app.url') }}assets/images/favicon/apple-icon-76x76.png?=1.7">
  	<link rel="apple-touch-icon" sizes="114x114" href="{{ config('app.url') }}assets/images/favicon/apple-icon-114x114.png?=1.7">
  	<link rel="apple-touch-icon" sizes="120x120" href="{{ config('app.url') }}assets/images/favicon/apple-icon-120x120.png?=1.7">
  	<link rel="apple-touch-icon" sizes="144x144" href="{{ config('app.url') }}assets/images/favicon/apple-icon-144x144.png?=1.7">
  	<link rel="apple-touch-icon" sizes="152x152" href="{{ config('app.url') }}assets/images/favicon/apple-icon-152x152.png?=1.7">
  	<link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}assets/images/favicon/apple-icon-180x180.png?=1.7">
  	<link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}assets/images/favicon/android-icon-192x192.png?=1.7">
  	<link rel="icon" type="image/png" sizes="32x32" href="{{ config('app.url') }}assets/images/favicon/favicon-32x32.png?=1.7">
  	<link rel="icon" type="image/png" sizes="96x96" href="{{ config('app.url') }}assets/images/favicon/favicon-96x96.png?=1.7">
  	<link rel="icon" type="image/png" sizes="16x16" href="{{ config('app.url') }}assets/images/favicon/favicon-16x16.png?=1.7">
  	<link rel="manifest" href="{{ config('app.url') }}assets/images/favicon/manifest.json?=1.7">
  	<meta name="msapplication-TileColor" content="#ffffff">
  	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png?=1.7">
  	<meta name="theme-color" content="#ffffff">

    <!-- Fonts CSS -->
  	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/fonts.css?=1.6">

    <!-- Bootstrap 4.5.0 CSS -->
  	<link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/bootstrap-4.5.0/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ config('app.url') }}assets/dist/css/adminlte.min.css?=1.0">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css?=1.0">
  	<!-- Font-Awesome CSS -->
  	<link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Datatable CSS -->
    <link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/datatables/datatables.min.css">
  	<!-- SweetAlert2 -->
    <link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/animate.css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
    <link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/toastr/toastr.min.css">
    <!-- icheck -->
    <link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- daterangepicker -->
  	<link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/daterangepicker/daterangepicker.css">
    <!-- Croppie -->
    <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/croppie-2.6.4/css/croppie.css?=1.0">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/summernote/summernote-bs4.css?=1.0">
    <!-- Light gallery -->
  	<link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/lightgallery.js-master/src/css/lightgallery.css">
    @if($page_en == 'edit_news_form')
    <!-- Dropzone 5 -> Version นี้ยัง Support IE อยู่ ถ้า Version 6 จะไม่ Support -->
    <link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/dropzone5/dropzone.min.css" />
    @endif
</head>

<style>
::placeholder {
    /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: gray !important;
    opacity: 0.45 !important;
    /* Firefox */
}

:-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    color: gray !important;
    opacity: 0.45 !important;
}

::-ms-input-placeholder {
    /* Microsoft Edge */
    color: gray !important;
    opacity: 0.45 !important;
}

.note-group-select-from-files {
  display: none;
}

.isDisabled {
  pointer-events: none;
  cursor: default;
}

.isDisabled i {
  color:rgba(0, 0, 0, 0.2)!important;
}
</style>

@include('backend.layouts.onLoad')

<!-- Load Logout modal -->
@include('backend.layouts.modal')
