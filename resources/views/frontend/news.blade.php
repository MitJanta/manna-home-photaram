<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- include header -->
@include('frontend.layouts.header')

<!-- include header -->
@include('frontend.layouts.topnav')

<body>
    <section id="banner_section"style="background-image: linear-gradient(0deg, rgba(170, 57, 198, 0.47), rgba(170, 57, 198, 0.47)), url('{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}')">
        <div class="content-float">
            <img src="{{ config('app.url') }}assets/images/asset-12.png" alt="">
            <p class="banner-title mt-3">NEWS&ACTIVITY </p>
            <ol class="mt-n3 d-flex">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">News&Activity</li>
            </ol>
        </div>
    </section>

    <section id="title_section">
        <div class="container">
            <p class="content-title">
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">News</span>
                & Activity
            </p>
        </div>
    </section>

    <section id="news_list_section">
        <div class="container">
            <div class="row">

                @foreach($news_list as $row)
                <div class="col-lg-4 col-md-6 w-100">
                    <div class="card-news-acts h-100">
                        <div class="img-news">
                            <img src="{{ config('app.url') }}files_upload/news_acts/{{ $row->cover }}" class="w-100" alt="">
                            <div class="news-date d-md-none d-block">
                                <div class="news-date-num">{{ date("d",strtotime($row->date)) }}</div>
                                <div class="news-date-month">{{ ucfirst(substr(date("M",strtotime($row->date)), 0, 3))." ".date("Y",strtotime($row->date)) }}</div>
                            </div>
                        </div>
                        <div class="news-detail d-flex">
                            <div class="news-date d-md-block d-none">
                                <div class="news-date-num">{{ date("d",strtotime($row->date)) }}</div>
                                <div class="news-date-month">{{ ucfirst(substr(date("M",strtotime($row->date)), 0, 3))." ".date("Y",strtotime($row->date)) }}</div>
                            </div>
                            <div class="news-text">
                                <p class="news-text-by w-100">
                                    By <span class="text-yellow">{{ $row->author }}</span>
                                </p>
                                <p class="news-text-title">
                                  {{ $row->title }}
                                </p>
                            </div>
                        </div>
                        <p class="content-text news-text-detail">
                          @if(!empty($row->content))
                            {{ strip_tags($row->content) }}
                          @else
                            {{ $row->title }}
                          @endif
                        </p>
                        <p class="content-read-more-btn text-center mt-4" style="position: absolute;bottom: 0;left: 1rem;">
                            <a href="{{ route('news_acts_detail', [$page_num, $row->id]) }}">
                                READ MORE
                            </a>
                        </p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
        <div class="news-pagination">
          <div class="pagination justify-content-center d-block text-center">
              <a class="pagination-btn @if($page_num != 1) active @endif" @if($page_num != 1) href="{{route('news_acts', $page_num-1)}}" @endif> <i class="fas fa-chevron-left"></i> </a>
              @for($i = 1;$i <= $max_page; $i++)
              <a class="pagination-number @if($page_num == $i) active @endif" href="{{ route('news_acts', $i) }}">{{ $i }}</a>
              @endfor
              <a class="pagination-btn @if($page_num != $max_page && $max_page != 0) active @endif" @if($page_num != $max_page && $max_page != 0) href="{{route('news_acts', $page_num+1)}}" @endif> <i class="fas fa-chevron-right"></i> </a>
          </div>
        </div>
    </section>

</body>

<!-- include footer -->
@include('frontend.layouts.footer')

<script type="text/javascript">

</script>
