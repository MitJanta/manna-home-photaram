<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- include header -->
@include('frontend.layouts.header')

<!-- include header -->
@include('frontend.layouts.topnav')

<body>
  <section id="banner_section" style="background-image: linear-gradient(0deg, rgba(170, 57, 198, 0.47), rgba(170, 57, 198, 0.47)), url('{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}')">
    <div class="content-float">
      <img src="{{ config('app.url') }}assets/images/asset-12.png" alt="">
      <p class="banner-title mt-3">ABOUT US </p>
      <ol class="mt-n3 d-flex">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">About Us</li>
      </ol>
    </div>
  </section>

  <section id="who_we_are_section">
    <div class="container">
      <div class="row">

        <div class="col-md-5">
          <p class="content-text-bold mb-md-5 mb-3">ABOUT US</p>
          <p class="content-title">
            Who
            <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
              we are
            </span>
          </p>
          <p>
            Manna Home<span class="d-md-block d-none"></span>
            Photharam for Woman</br>
            & Children Foundation
          </p>
        </div>

        <div class="col-md-7">
          <img src="{{ config('app.url') }}files_upload/contents/{{ $about_data_th->image_name }}" class="d-block mx-auto mb-md-5 mb-4" alt="">
          <p class="content-text">
            {!! $about_data_th->content_th !!}
          </p>
        </div>

      </div>

    </div>
  </section>

  <section id="who_we_are_en_section">
    <!-- who_we_are_en Desktop size -->
    <div class="container d-md-block d-none" style="position: relative;">
      <img src="{{ config('app.url') }}files_upload/contents/{{ $about_data_eng->image_name }}" class="img-who-we-are" alt="">
      <div class="content-gray-box">
        <p class="content-text">
          <span style="font-family: 'Prompt-Medium', sans-serif;font-size: 2rem">
            {{ substr($about_data_eng->content_en, 0, 1) }}</span>{{ substr($about_data_eng->content_en, 1) }}
        </p>
      </div>
    </div>
    <!-- .end who_we_are_en Desktop size -->

    <!-- who_we_are_en Mobile size -->
    <div class="container d-md-none d-block" style="position: relative;">
      <img src="{{ config('app.url') }}files_upload/contents/{{ $about_data_eng->image_name }}" class="w-100" alt="">
    </div>
    <!-- .end who_we_are_en Mobile size -->

  </section>

  <section id="what_we_do_section">
    <div class="container">
      <p class="content-title text-center d-md-block d-none">
        What
        <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
          we do
        </span>
      </p>
      <img src="{{ config('app.url') }}assets/images/asset-13.png" class="icon d-md-block d-none" alt="">
      <p class="content-text mt-4 d-md-block d-none">
        {!! $what_we_do_th->content_th !!}
      </p></br>
      <p class="content-text d-md-block d-none">
        {!! $what_we_do_eng->content_en !!}
      </p>
      <!-- Mobile size -->
      <p class="content-text d-md-none d-block px-md-5 px-3">
        <span style="font-family: 'Prompt-Medium', sans-serif;font-size: 2rem">
          {{ substr($about_data_eng->content_en, 0, 1) }}</span>{{ substr($about_data_eng->content_en, 1) }}
      </p>
      <!-- .end Mobile size -->
      <div class="row d-flex mx-auto mt-5" style="max-width: 768px;">
        <div class="col-4">
          <img src="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_1->image_name }}" class="w-100 px-md-2 px-0 what-we-do-img" alt="">
        </div>
        <div class="col-4">
          <img src="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_2->image_name }}" class="w-100 px-md-2 px-0 what-we-do-img" alt="">
        </div>
        <div class="col-4">
          <img src="{{ config('app.url') }}files_upload/contents/{{ $what_we_do_img_3->image_name }}" class="w-100 px-md-2 px-0 what-we-do-img" alt="">
        </div>
      </div>
    </div>
  </section>

</body>

<!-- include footer -->
@include('frontend.layouts.footer')

<script type="text/javascript">

</script>
