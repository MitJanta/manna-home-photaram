<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- include header -->
@include('frontend.layouts.header')

<!-- include header -->
@include('frontend.layouts.topnav')

<style media="screen">
#g-recaptcha-response {
  display: block !important;
  position: absolute;
  margin: -78px 0 0 0 !important;
  width: 302px !important;
  height: 76px !important;
  z-index: -999999;
  opacity: 0;
}
</style>

<body>
    <section id="banner_section"style="background-image: linear-gradient(0deg, rgba(170, 57, 198, 0.47), rgba(170, 57, 198, 0.47)), url('{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}')">
        <div class="content-float">
            <img src="{{ config('app.url') }}assets/images/asset-12.png" alt="">
            <p class="banner-title mt-3">CONTACT US </p>
            <div class="d-block px-auto">
                <ol class="mt-n3 d-flex">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                </ol>
            </div>
        </div>
    </section>

    <section id="contact_title_section">
        <div class="container">
            <p class="content-title">
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">Contact</span>&nbsp;us
            </p>
        </div>
    </section>

    <section id="contact_content_section">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-12">
                    <img src="{{ config('app.url') }}assets/images/asset-16.png" alt="">
                    <p class="content-text text-center mt-4">
                        {!! $contact_email->content_th !!}
                    </p>
                </div>

                <div class="col-lg-4 col-12 mt-lg-0 mt-4">
                    <img src="{{ config('app.url') }}assets/images/asset-17.png" alt="">
                    <p class="content-text text-center mt-4">
                        {!! $contact_address->content_th !!}
                    </p>
                </div>

                <div class="col-lg-4 col-12 mt-lg-0 mt-4">
                    <img src="{{ config('app.url') }}assets/images/asset-18.png" alt="">
                    <p class="content-text text-center mt-4">
                        {!! $contact_tel_1->content_th !!}</br>
                        {!! $contact_tel_2->content_th !!}
                    </p>
                </div>

            </div>
        </div>
    </section>

    {!! $contact_google_map->content_th !!}

    <section id="contact_section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-md-block d-none">
                    <img src="{{ config('app.url') }}files_upload/contents/{{ $contact_massage_img->image_name }}" class="w-100 pr-5" alt="">
                </div>
                <div class="col-md-6">
                    <p class="content-title ml-n3">Contact us</p>
                    <form action="{{ route('contact_submit') }}" method="post" id="contact_message_submit">
                        @csrf
                        <div class="form-group row">
                            <div class="col-lg-6 px-0">
                                <input type="text" class="form-control bg-gray" name="fname_input" id="fname-input" placeholder="First Name" required
                                oninvalid="this.setCustomValidity('กรุณากรอกชื่อผู้บริจาค')" oninput="this.setCustomValidity('')">
                            </div>
                            <div class="col-lg-6 pl-lg-3 mt-lg-0 mt-3 px-0">
                                <input type="text" class="form-control bg-gray" name="lname_input" id="lname-input" placeholder="Last Name" required
                                oninvalid="this.setCustomValidity('กรุณากรอกนามสกุลผู้บริจาค')" oninput="this.setCustomValidity('')">
                            </div>
                            <input type="text" class="form-control col-12 bg-gray mt-4" name="tel_input" id="tel-input" placeholder="Telephone" required
                            oninvalid="this.setCustomValidity('กรุณากรอกเบอร์โทร')" oninput="this.setCustomValidity('')">
                            <input type="email" class="form-control col-12 bg-gray mt-4" name="email_input" id="email-input" placeholder="E-mail" required
                            oninvalid="this.setCustomValidity('กรุณากรอกอีเมล')" oninput="this.setCustomValidity('')">
                            <textarea class="form-control col-12 bg-gray mt-4" rows="3" name="message_input" id="message-input" placeholder="Send Message" required
                            oninvalid="this.setCustomValidity('กรุณากรอกข้อความ')" oninput="this.setCustomValidity('')"></textarea>
                            <div id="html_element" class="w-100 mt-3 d-block mx-auto"></div>
                            <button type="submit" class="btn btn-custom d-block mx-auto mt-3">SUBMIT MESSAGE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</body>

<!-- include footer -->
@include('frontend.layouts.footer')

<script type="text/javascript">
  var onloadCallback = function() {
    grecaptcha.render('html_element', {
      'sitekey' : '6LeO7KMdAAAAAD-LveyPwA9OktHWrwKait8D0Iqi'
    });
  };

  window.onload = function() {
    var $recaptcha = document.querySelector('#g-recaptcha-response');
    if($recaptcha) {
        $recaptcha.setAttribute("required", "required");
    }
  };
</script>
