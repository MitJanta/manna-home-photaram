<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- include header -->
@include('frontend.layouts.header')

<!-- include header -->
@include('frontend.layouts.topnav')

<style media="screen">
    #g-recaptcha-response {
        display: block !important;
        position: absolute;
        margin: -78px 0 0 0 !important;
        width: 302px !important;
        height: 76px !important;
        z-index: -999999;
        opacity: 0;
    }
</style>

<body>
    <section id="banner_section" style="background-image: linear-gradient(0deg, rgba(170, 57, 198, 0.47), rgba(170, 57, 198, 0.47)), url('{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}')">
        <div class="content-float">
            <img src="{{ config('app.url') }}assets/images/asset-12.png" alt="">
            <p class="banner-title mt-3">DONATION</p>
            <ol class="mt-n3 d-flex">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Donation</li>
            </ol>
        </div>
    </section>

    <section id="how_you_can_help_section">
        <div class="container">
            <p class="content-text-bold mb-md-5 mb-3">DONATION</p>
            <p class="content-title d-sm-block d-none">
                How you can
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
                    help <img src="{{ config('app.url') }}assets/images/asset-15.png" style="max-height: 25px" alt="">
                </span>
            </p>
            <p class="content-title d-sm-none d-block">
                How you can</br>
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
                    help <img src="{{ config('app.url') }}assets/images/asset-15.png" style="max-height: 25px" alt="">
                </span>
            </p>
            <p class="content-text text-center d-block mx-auto mt-5 mb-md-0 mb-5">
                {{ $how_you_can_help->content_th }}
            </p>
        </div>
    </section>

    <section id="acc_num_th" class="acc-num-th">
        <div class="background-mobile-1 d-lg-none d-flex" style="background-image: url('{{ config('app.url') }}files_upload/contents/{{ $acc_data_th->link_url }}')">
            .
        </div>
        <div class="container">
            <!-- Desktop size -->
            <div class="row d-lg-flex d-none">
                <div class="col-md-5 help-us-box-img">
                    <img src="{{ config('app.url') }}files_upload/contents/{{ $acc_data_th->image_name }}" alt="">
                </div>
                <div class="col-lg-7 help-us-box-text text-center">
                    <img src="{{ config('app.url') }}assets/images/asset-6.png" class="" alt="">
                    <p class="content-title mt-3">{!! $acc_data_th->title_th !!}</p>
                    <p class="content-text-bold">
                        {!! $acc_data_th->title_en !!}
                    </p>
                    <p class="content-text">
                        {!! $acc_data_th->content_th !!}
                    </p>
                </div>
            </div>
            <!-- .end Desktop size -->
            <!-- Mobile size -->
            <div class="row d-lg-none d-flex">
                <div class="col-lg-7 help-us-box-text text-center">
                    <img src="{{ config('app.url') }}assets/images/asset-5.png" class="mt-3" alt="">
                    <p class="content-title mt-3">{!! $acc_data_th->title_th !!}</p>
                    <p class="content-text-bold">
                        {!! $acc_data_th->title_en !!}
                    </p>
                    <p class="content-text">
                        {!! $acc_data_th->content_th !!}
                    </p>
                </div>
            </div>
            <!-- .end Mobile size -->
        </div>
    </section>

    <section id="text_pure_section">
        <div class="container">
            <p class="content-text text-center d-block mx-auto" style="max-width: 768px">
                {!! $how_you_can_help->content_en !!}
            </p>
        </div>
    </section>

    <section id="acc_num_en" class="acc-num-en">
        <div class="background-mobile-2 d-lg-none d-flex" style="background-image: url('{{ config('app.url') }}files_upload/contents/{{ $acc_data_en->link_url }}')">
            .
        </div>
        <div class="container">
            <!-- Desktop size -->
            <div class="row d-lg-flex d-none">
                <div class="col-md-7 help-us-box-text text-center">
                    <img src="{{ config('app.url') }}assets/images/asset-5.png" class="mt-4" alt="">
                    <p class="content-title mt-3">{!! $acc_data_en->title_th !!}</p>
                    <p class="content-text-bold">
                        {!! $acc_data_en->title_en !!}
                    </p>
                    <p class="content-text">
                        {!! $acc_data_en->content_th !!}
                    </p>
                </div>
                <div class="col-md-5 help-us-box-img">
                    <img src="{{ config('app.url') }}files_upload/contents/{{ $acc_data_en->image_name }}" alt="">
                </div>
            </div>
            <!-- .end Desktop size -->
            <!-- Mobile size -->
            <div class="row d-lg-none d-flex">
                <div class="col-12 help-us-box-text text-center">
                    <img src="{{ config('app.url') }}assets/images/asset-6.png" class="mt-4" alt="">
                    <p class="content-title mt-3">{!! $acc_data_en->title_th !!}</p>
                    <p class="content-text-bold">
                        {!! $acc_data_en->title_en !!}
                    </p>
                    <p class="content-text">
                        {!! $acc_data_en->content_th !!}
                    </p>
                </div>
            </div>
            <!-- .end Mobile size -->
        </div>
    </section>

    <section id="form_donate_section">
        <div class="container">
            <p class="content-title text-center d-sm-block d-none">
                แบบฟอร์ม
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
                    การบริจาค
                </span>
            </p>
            <p class="content-title text-center d-sm-none d-block">
                แบบฟอร์ม</br>
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
                    การบริจาค
                </span>
            </p>

            <form action="{{ route('donation_submit') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group mt-5 row px-md-0 px-3">
                    <label for="name-input" class="content-text col-12 pl-0">ชื่อผู้บริจาค : Name of donor</label>
                    <div class="col-lg-6 px-0">
                        <input type="text" class="form-control bg-gray" name="fname_input" id="fname-input" placeholder="First Name" required oninvalid="this.setCustomValidity('กรุณากรอกชื่อผู้บริจาค')" oninput="this.setCustomValidity('')">
                    </div>
                    <div class="col-lg-6 pl-lg-3 mt-lg-0 mt-3 px-0">
                        <input type="text" class="form-control bg-gray" name="lname_input" id="lname-input" placeholder="Last Name" required oninvalid="this.setCustomValidity('กรุณากรอกนามสกุลผู้บริจาค')" oninput="this.setCustomValidity('')">
                    </div>
                    <div class="col-12 mt-3 px-0">
                        <input type="email" class="form-control bg-gray" name="email_input" id="email-input" placeholder="E-mail (กรุณากรอก หากต้องการใบเสร็จ)">
                    </div>
                    <label for="purpose-input" class="content-text col-12 pl-0 mt-4">บริจาคเพื่อ : Donate for</label>
                    <textarea rows="2" class="form-control col-12 bg-gray" name="purpose_input" id="purpose-input" placeholder="" oninvalid="this.setCustomValidity('กรุณากรอกบริจาคเพื่อ')" oninput="this.setCustomValidity('')"></textarea>
                    <label for="amount-input" class="content-text col-12 pl-0 mt-3">จำนวนเงิน : Amount you want to donate</label>
                    <div class="col-12 px-0">
                        <input type="number" class="form-control bg-gray" name="amount_input" id="amount-input" placeholder="จำนวนเงิน" required oninvalid="this.setCustomValidity('กรุณากรอกจำนวนเงิน')" oninput="this.setCustomValidity('')">
                    </div>

                    <!-- Gray line divider -->
                    <div class="gray-line-devider col-12">.</div>

                    <p class="content-text-bold col-12  px-0">ชำระโดย : Paid by</p>
                    <div class="form-check icheck-secondary col-12 mt-md-4 mt-2">
                        <input class="form-check-input" type="radio" name="paid_by" id="paid-by" value="1">
                        <label class="form-check-label content-text gray" for="paid-by">
                            เงินสด : Cash
                        </label>
                    </div>
                    <div class="form-check icheck-secondary col-12 mt-md-5 mt-4 d-flex row pr-0" style="padding-left: 15px">
                        <input class="form-check-input" type="radio" name="paid_by" id="paid-by2" value="2" checked>
                        <label class="form-check-label content-text gray col-md-3 d-md-block d-flex w-100" for="paid-by2">
                            โอนผ่านธนาคาร
                            <span class="d-md-none d-block">&nbsp;: Bank transfer</span>
                            <div class="d-md-block d-none w-100">Bank transfer</div>
                        </label>
                        <div class="col-md-9">
                            <div class="row">

                                <select class="form-control col-12 mt-md-0 mt-4" style="width: 100% !important" name="bank_name" id="bank-name" required>
                                    <option value="" checked>กรุณาเลือก</option>
                                    @foreach($banks_data as $row)
                                    @if($row->code == 'OT')
                                    <option value="{{ $row->code }}" checked>{{ $row->name }} (Please specify)</option>
                                    @else
                                    <option value="{{ $row->code }}" checked>{{ $row->code }} - {{ $row->name }}</option>
                                    @endif
                                    @endforeach
                                </select>

                                <input type="text" class="form-control col-12 mt-4 d-none" name="bank_name_input" id="bank-name-input" placeholder="กรุณาระบุ">

                                <label for="branch-input" class="content-text col-12 pl-0 mt-4">สาขา : Branch</label>
                                <input type="text" class="form-control col-12" name="branch_input" id="branch-input" placeholder="">

                                <div class="col-lg-6 px-0">
                                    <label for="date-input" class="content-text pl-0 mt-4">วันที่โอน : Dated</label>
                                    <div class="input-group pl-0">
                                        <input type="text" class="form-control daterange-date" id="date-input" name="date_input" placeholder="">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="fas fa-calendar-alt"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 pl-lg-3 px-0">
                                    <label for="time-input" class="content-text w-100 px-0 mt-4">เวลาที่โอน : Transfer time</label>
                                    <div class="input-group px-0">
                                        <input type="text" class="form-control daterange-time" id="time-input" name="time_input" placeholder="">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="fas fa-clock"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <label for="amount-input" class="content-text col-auto pl-0 mt-4">หลักฐานการโอนเงิน : Payment slip</label>
                                <div class="custom-file col-auto" style="font-size: .8rem;">
                                    <input type="file" class="custom-file-input" id="attache-file" name="attache_file">
                                    <label class="custom-file-label" for="attache-file">เลือกไฟล์</label>
                                </div>

                            </div>
                        </div>

                        <div id="html_element" class="w-100 px-0 mt-3 d-block mx-auto pt-4 border-top"></div>

                    </div>
                    <button type="submit" class="btn btn-custom d-block mx-auto mt-4">SUBMIT</button>
                </div>
            </form>

        </div>
    </section>

</body>

<!-- include footer -->
@include('frontend.layouts.footer')

<script type="text/javascript">
    $('#bank-name').change(function() {
        if (this.value == 'OT') {
            $('#bank-name-input').removeClass('d-none');
            $('#bank-name-input').addClass('d-block');
            $('#bank-name-input').prop('required', true);
        } else {
            $('#bank-name-input').addClass('d-none');
            $('#bank-name-input').removeClass('d-block');
            $('#bank-name-input').prop('required', false);
        }
    });

    $('input[name="paid_by"]').change(function() {
      if (this.value == 1) {
        lockTransfer();
      } else if (this.value == 2) {
        lockCash();
      }
    });

    function lockCash() {
      $('#bank-name').prop('disabled', false);
      $('#branch-input').prop('disabled', false);
      $('#date-input').prop('disabled', false);
      $('#time-input').prop('disabled', false);
      $('#attache-file').prop('disabled', false);

      $('#bank-name').prop('required', true);
      $('#branch-input').prop('required', true);
      $('#date-input').prop('required', true);
      $('#time-input').prop('required', true);
      $('#attache-file').prop('required', true);

      if ($('#bank_name').val() == 'OT') {
        $('#bank-name-input').prop('disabled', false);
        $('#bank-name-input').prop('required', true);
      }
    }

    function lockTransfer() {
      $('#bank-name').prop('disabled', true);
      $('#branch-input').prop('disabled', true);
      $('#date-input').prop('disabled', true);
      $('#time-input').prop('disabled', true);
      $('#attache-file').prop('disabled', true);

      $('#bank-name').prop('required', false);
      $('#branch-input').prop('required', false);
      $('#date-input').prop('required', false);
      $('#time-input').prop('required', false);
      $('#attache-file').prop('required', false);

      if ($('#bank_name').val() == 'OT') {
        $('#bank-name-input').prop('disabled', true);
        $('#bank-name-input').prop('required', false);
      }
    }

    var onloadCallback = function() {
        grecaptcha.render('html_element', {
            'sitekey': '6LeO7KMdAAAAAD-LveyPwA9OktHWrwKait8D0Iqi'
        });
    };

    window.onload = function() {
        var $recaptcha = document.querySelector('#g-recaptcha-response');
        if ($recaptcha) {
            $recaptcha.setAttribute("required", "required");
        }
        lockCash();
    };
</script>
