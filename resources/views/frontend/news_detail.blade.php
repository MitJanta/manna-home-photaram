<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- include header -->
@include('frontend.layouts.header')

<!-- include header -->
@include('frontend.layouts.topnav')

<body onload="galleryLoad()">
  <section id="banner_section" style="background-image: linear-gradient(0deg, rgba(170, 57, 198, 0.47), rgba(170, 57, 198, 0.47)), url('{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}')">
    <div class="content-float">
      <img src="{{ config('app.url') }}assets/images/asset-12.png" alt="">
      <p class="banner-title mt-3">NEWS&ACTIVITY</p>
      <ol class="mt-n3 d-flex">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">News&Activity</li>
      </ol>
    </div>
  </section>

  <section id="title_section">
    <div class="container">
      <p class="content-title">
        <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">News</span>
        & Activity
      </p>
    </div>
  </section>

  <section id="news_list_section">
      <div class="w-100">
        <div class="card-news-acts">
          <div class="img-news">
            <img src="{{ config('app.url') }}files_upload/news_acts/{{ $news_data->cover }}" class="w-100" alt="">
            <div class="news-date d-md-none d-block">
              <div class="news-date-num">{{ date("d",strtotime($news_data->date)) }}</div>
              <div class="news-date-month">{{ ucfirst(substr(date("M",strtotime($news_data->date)), 0, 3))." ".date("Y",strtotime($news_data->date)) }}</div>
            </div>
          </div>
          <div class="news-detail d-flex">
            <div class="news-date d-md-block d-none">
              <div class="news-date-num">{{ date("d",strtotime($news_data->date)) }}</div>
              <div class="news-date-month">{{ ucfirst(substr(date("M",strtotime($news_data->date)), 0, 3))." ".date("Y",strtotime($news_data->date)) }}</div>
            </div>
            <div class="news-text">
              <p class="news-text-by w-100">
                By <span class="text-yellow">{{ $news_data->author }}</span>
              </p>
              <p class="news-text-title">
                {{ $news_data->title }}
              </p>
            </div>
          </div>
          <!-- จำลองสิ่งที่ยิงออกมาจาก Summer note -->
          <div class="content-text mb-4">
            {!! $news_data->content !!}
          </div>

          <div class="text-center" id="gallery-load" style="font-size: 1.5rem;">
              <i class="fas fa-spin fa-spinner"></i>
              <p>โปรดรอสักครู่ ...</p>
          </div>

          <div class="news-photo">
            <div class="row" id="grid">
                @php $files = scandir('files_upload/news_acts/'.$news_data->gallery.'/');
                $file_not_have = 0;
                $num_rows = 0;
                if (false !== $files) {
                foreach ($files as $file) {
                if ('.' != $file && '..' != $file && $file_not_have < 2) {
                @endphp
                <div class="grid-item col-sm-6">
                    <a class="video-hover" href="{{ config('app.url') }}files_upload/news_acts/{{ $news_data->gallery }}/{{ $file }}">
                        <img class="w-100" src="{{ config('app.url') }}files_upload/news_acts/{{ $news_data->gallery }}/{{ $file }}" alt="{{ $file }}" title="{{ $file }}" />
                    </a>
                </div>
                @php
                $file_not_have++;
                }
                $num_rows++;
              }}
                @endphp
            </div>

            <div class="row collapse" id="grid2">
                @php $files = scandir('files_upload/news_acts/'.$news_data->gallery.'/');
                $file_not_have = 0;
                $num_rows = 0;
                if (false !== $files) {
                foreach ($files as $file) {
                if ('.' != $file && '..' != $file && $file_not_have > 3) {
                @endphp
                <div class="grid-item col-sm-6 mt-4">
                    <a class="video-hover" href="{{ config('app.url') }}files_upload/news_acts/{{ $news_data->gallery }}/{{ $file }}">
                        <img class="w-100" src="{{ config('app.url') }}files_upload/news_acts/{{ $news_data->gallery }}/{{ $file }}" alt="{{ $file }}" title="{{ $file }}" />
                    </a>
                </div>
                @php
                }
                $file_not_have++;
              }}
                @endphp
            </div>

            @if ($file_not_have > 2)
              <p class="content-see-gallery-btn text-center mt-5" data-toggle="collapse" role="button" href="#grid2" aria-expanded="false" aria-controls="grid2">
                <a onclick="seeGallery()" style="cursor: pointer">
                  ดูรูปภาพทั้งหมด
                </a>
              </p>
            @endif

          </div>
        </div>
      </div>
  </section>

</body>

<!-- include footer -->
@include('frontend.layouts.footer')

<script type="text/javascript">
function galleryLoad() {
    $('#gallery-load').css('display', 'none');
}

function seeGallery() {
  $('.content-see-gallery-btn').fadeOut(500);
}

function getGallery() {
  // lightGallery plugin init
  lightGallery(document.getElementById('grid'), {
      selector: '.grid-item a',
      mode: 'lg-slide',
      cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
      width: '991px',
      thumbnail: true,
      animateThumb: true,
      showThumbByDefault: true,
      // iframeMaxWidth: 'auto',
      download: false,
      loop: true,
      zoom: true,
      scale: 1.8,
      fullScreen: true,
      // rotate: true
  });

  // lightGallery plugin init
  lightGallery(document.getElementById('grid2'), {
      selector: '.grid-item a',
      mode: 'lg-slide',
      cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
      width: '991px',
      thumbnail: true,
      animateThumb: true,
      showThumbByDefault: true,
      // iframeMaxWidth: 'auto',
      download: false,
      loop: true,
      zoom: true,
      scale: 1.8,
      fullScreen: true,
      // rotate: true
  });

}

// On Ready javascript
$(document).ready(function() {
    getGallery();
});
</script>
