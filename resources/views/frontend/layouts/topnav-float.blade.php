<!--Navbar-->
<nav class="navbar navbar-expand-lg fixed-top bg-yellow row" id="navbar-float">
  <!-- Navbar brand -->
  <div class="container" style="max-width: 100%; padding: 0px !important">
    <a class="navbar-brand" href="{{ route('home') }}">
      MANNA HOME PHOTHARAM
    </a>

    <!-- Collapse button -->
    <button class="navbar-toggler third-button float-right" type="button" style="margin-right: .5rem"
    data-toggle="collapse" data-target="#navbarDropdown1" aria-controls="navbarDropdown1"
    aria-expanded="false" aria-label="Toggle navigation">
      <div class="animated-icon3"><span></span><span></span><span></span></div>
    </button>
    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="navbarDropdown1" style="padding:0px !important;">
      <!-- Links -->
      <ul class="navbar-nav ml-auto py-lg-1 py-2">

        <li class="nav-item">
          <a class="nav-link @if($page_en == 'home') active @endif" href="{{ route('home') }}">
            HOME
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @if($page_en == 'about') active @endif" href="{{ route('about') }}">
            ABOUT US
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @if($page_en == 'news' || $page_en == 'news_detail' || $page_en == 'news_gallery') active @endif" href="{{ route('news_acts', 1) }}">
            NEWS & ACTIVITY
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @if($page_en == 'contact') active @endif" href="{{ route('contact') }}">
            CONTACT US
          </a>
        </li>

        <li class="nav-item d-lg-none d-block">
          <a class="nav-link @if($page_en == 'donation') active @endif" href="{{ route('donation') }}">
            DONATION
          </a>
        </li>

      </ul>

      <!-- Donation section -->
      <div class="donation-dark-btn d-lg-block d-none @if($page_en == 'donation') active @endif">
        <a href="{{ route('donation') }}" class="row">
          <img src="{{ config('app.url') }}assets/images/asset-11.png" alt="">
          <div class="donate-text">
            <p class="donate-sm">Make your</p>
            <p class="donate-md">DONATION</p>
          </div>
        </a>
      </div>
      <!-- Links -->
    </div>
    <!-- Collapsible content -->

  </div>
  <!-- .end Div grid-container -->

</nav>
