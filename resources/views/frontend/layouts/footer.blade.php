<!-- Back to top button -->
<a id="back2Top" class="text-center" title="Back to top" href="#"><i class="fas fa-arrow-up"></i></a>

<footer>
    <div class="container">
        <div class="row">

            <div class="col-lg-7">
                <div class="row">
                    <div class="col-auto">
                        <p class="acc-text-1">เลขที่บัญชีมูลนิธิ</p>
                        <p class="acc-text-2 d-md-block d-none">มูลนิธิบ้านมานาโพธาราม</p>
                        <p class="acc-text-3 d-md-block d-none">เพื่อสตรีและเด็ก</p>

                        <p class="acc-text-2-sm d-md-none d-block">มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก</p>
                    </div>
                    <div class="col-auto row acc-num-box">
                        <div class="acc-num">
                            {{ $footer_global_data->title_th }}
                        </div>
                        <div class="logo">
                            <img src="{{ config('app.url') }}files_upload/contents/{{ $footer_global_data->image_name }}" alt="not found" title="ธนาคารทหารไทยธนชาต (TMBThanachart Bank)">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 phone-number my-lg-0 my-3">
                <div class="row text-right float-lg-right float-left pl-3">
                    <img src="{{ config('app.url') }}assets/images/asset-7.png" alt="">
                    <p class="content-white-text ml-2">{{ $contact_array_global_data['tel_1'] }}</p>
                </div>
                <div style="clear:both"></div>
                <div class="row text-right float-lg-right float-left pl-3">
                    <img src="{{ config('app.url') }}assets/images/asset-7.png" alt="">
                    <p class="content-white-text ml-2">{{ $contact_array_global_data['tel_2'] }}</p>
                </div>
            </div>

            <div class="line-divider col-12">.</div>

            <div class="col-xl-6 col-lg-5 mt-md-0 mt-4">
                <p class="content-white-title-bold-md">Address</p>
                <div class="contact-item row mt-lg-0 mt-3" style="margin-left: 0rem">
                    <img src="{{ config('app.url') }}assets/images/asset-10.png" class="mt-0" alt="">
                    <p class="content-white-text-sm pl-3" style="width: calc(100% - 50px)">
                        {!! $contact_array_global_data['address'] !!}
                    </p>
                </div>
            </div>

            <div class="col-xl-6 col-lg-7 mt-0">
                <div class="row footer-link mt-lg-0 mt-5" id="link-footer">
                    <div class="col-xl-auto col-12 first-link ml-md-n3 ml-0">
                        Main Link
                    </div>
                    <div class="d-lg-none d-block col-12"></div>
                    <div class="col-lg-auto col-sm-4 link">
                        <a href="{{ route('home') }}">
                            Home
                        </a>
                    </div>
                    <div class="col-lg-auto col-12 link">
                        <a href="{{ route('about') }}">
                            About us
                        </a>
                    </div>
                    <div class="col-lg-auto col-12 link">
                        <a href="{{ route('news_acts', 1) }}">
                            News&Activity
                        </a>
                    </div>
                    <div class="col-lg-auto col-12 link">
                        <a href="{{ route('contact') }}">
                            Contact us
                        </a>
                    </div>
                    <div class="col-lg-auto col-12 link">
                        <a href="{{ route('donation') }}">
                            Donation
                        </a>
                    </div>
                </div>

                <div class="row mt-lg-4 mt-5 footer-contact">
                    <p class="content-white-title-bold-md col-12">Contact Us</p>
                    <div class="col-12">
                        <div class="contact-item row">
                            <img src="{{ config('app.url') }}assets/images/asset-8.png" alt="">
                            <p class="content-white-text-sm col-auto pl-3">{{ $contact_array_global_data['email'] }}</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="contact-item row mt-3">
                            <img src="{{ config('app.url') }}assets/images/asset-9.png" alt="">
                            <p class="content-white-text-sm col-auto pl-3"><a href="{{ $footer_global_data->title_en }}" target="_blank">มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก <small><i class="fas fa-external-link-alt"></i></small> </a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<!-- jquery .js (v.3.4.1) -->
<script type="text/javascript" src="{{ config('app.url') }}assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap .js -->
<script type="text/javascript" src="{{ config('app.url') }}assets/plugins/bootstrap-4.5.0/js/bootstrap.min.js"></script>
<!-- Lazyload -->
<script type="text/javascript" src="{{ config('app.url') }}assets/plugins/lazy-image-loader/jquery.lazyload.min.js"></script>
<!-- daterangepicker -->
<script src="{{ config('app.url') }}assets/plugins/moment/moment.min.js"></script>
<script type="text/javascript" src="{{ config('app.url') }}assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- SweetAlert2 -->
<script src="{{ config('app.url') }}assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="{{ config('app.url') }}assets/plugins/toastr/toastr.min.js"></script>
@if($page_en == 'home')
<script type="text/javascript" src="{{ config('app.url') }}assets/js/home.js"></script>
@elseif($page_en == 'contact' || $page_en == 'donation')
<!-- <script src="https://www.google.com/recaptcha/api.js"></script> -->
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
@elseif($page_en == 'news_detail')
<!-- Masonry column gallery -->
<script src="{{ config('app.url') }}assets/plugins/GridLoadingEffects/js/masonry.pkgd.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/GridLoadingEffects/js/imagesloaded.js"></script>
<script src="{{ config('app.url') }}assets/plugins/GridLoadingEffects/js/classie.js"></script>
<script src="{{ config('app.url') }}assets/plugins/GridLoadingEffects/js/AnimOnScroll.js"></script>
<!-- Light gallery JS viewer -->
<script src="{{ config('app.url') }}assets/plugins/lightgallery.js-master/dist/js/lightgallery.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/lightgallery.js-master/demo/js/lg-zoom.min.js"></script>
<script src="{{ config('app.url') }}assets/plugins/lightgallery.js-master/demo/js/lg-fullscreen.min.js"></script>
@endif

<script type="text/javascript">
    // Top nav
    function dropdown_menu_mobile() {
        $('.third-button').on('click', function() {
            // console.log('Good');
            $('.animated-icon3').toggleClass('open');
        });
    }

    $('input.daterange-date').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "startDate": "{{ date('d') }}/{{ date('m') }}/{{ date('Y') }}",
        "endDate": "{{ date('d') }}/{{ date('m') }}/{{ date('Y') }}",
        "applyButtonClasses": "btn-warning",
        locale: {
            format: 'DD/MM/YYYY'
        },
    }, function(start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });

    $('input.daterange-time').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 1,
        timePickerSeconds: false,
        "startDate": "{{ date('H') }}:{{ date('i') }}",
        "endDate": "",
        "applyButtonClasses": "btn-warning",
        locale: {
            format: 'HH:mm'
        },
    }, function(start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    }).on('show.daterangepicker', function(ev, picker) {
        picker.container.find(".calendar-table").hide();
    });

    // On Ready javascript
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();

        $("img.lazyload").lazyload();

        // Initailize for topnav hamberger mobile size
        dropdown_menu_mobile();
    });

    // Custom input file
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        // console.log(fileName);
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        $(this).siblings(".custom-file-label").addClass("text-dark");
    });

    // Check file upto 8 MB
    $('.custom-file-input').on('change', function() {
        var size = this.files[0].size / 1024 / 1024;
        if (size < 2) { // ดักไว้ 8 MB

        } else {
            $('.modal').modal('hide');
            $('#modal-max-file').modal('show');
            $(this).siblings('.custom-file-input').val('');
            $(this).siblings('.custom-file-label').text('เลือกไฟล์');
        }
    });

    // Back-to-top Button
    // Change style of navbar on scroll
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        return false;
    });
    /*Scroll to top when arrow up clicked BEGIN*/
    $(window).scroll(function() {
        var height = $(window).scrollTop();
        if (height > 120) {
            $('#back2Top').fadeIn('1000', 'linear');
        } else {
            $('#back2Top').fadeOut('1000', 'linear');
        }
    });
    /*Scroll to top when arrow up clicked END*/

    // ประกาศเรียกใช้ SweetAlert ตาม session ที่ส่งมา
    @if(!empty(session('message')) && !empty(session('alert')) && !empty(session('type')))
    @if(session('alert') == 'toast')
    const Swalert = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
    });

    $('.alertTrigger').ready(function() {
        Swalert.fire({
            icon: '{{ session('type') }}',
            title: '{!! session('message') !!}',
        })
    });
    @elseif(!empty(session('alert')) && session('alert') == 'popup')
    // console.log('test alert 1');
    const Swalert = Swal.mixin({
        toast: false,
        position: "center",
        showConfirmButton: true,
        timer: 5000,
        showClass: {
            popup: 'animate__animated animate__zoomIn'
        },
        hideClass: {
            popup: 'animate__animated animate__zoomOut'
        },
        customClass: {
            closeButton: 'btn btn-warning',
            confirmButton: 'btn btn-warning',
            cancelButton: 'btn btn-danger'
        },
    });

    $('.alertTrigger').ready(function() {
        Swalert.fire({
            icon: '{{ session('type') }}',
            title: '{!! session('message') !!}',
        })
    });
    @elseif(!empty(session('alert')) && session('alert') == 'modal')
    $('#modal-title-dynamic').text('{!! session('title') !!}');
    $('#modal-message-dynamic').text('{!! session('message') !!}');
    $('#modal-' + {{ session('type')}}).modal('show');
    @endif
    @endif
</script>
