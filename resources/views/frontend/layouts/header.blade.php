<head>
	<title>@if(($page_en == 'news_detail' || $page_en == 'news_gallery') && !empty($news_data->title)) {{ $news_data->title }} @else {{ ucfirst($page_en) }} @endif : Manna Home Photharam</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Prevent phone number link in Safari -->
	<meta name="format-detection" content="telephone=no">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!--__________________ Meta data __________________-->
	<meta name="copyright" content="Copyright 2021 Manna Home Photharam" />
	<meta name="author" content="Manna Home Photharam" />
	<meta name="keywords" content='{{ $page_th }} : Manna Home Photharam' />
	<meta name="Description" content='{{ $page_th }} ({{ $page_en }}) : Copyright 2021 Manna Home Photharam' />
	<meta name="robots" content="index,follow" />

	<meta property="og:title" content='{{ $page_th }}' />
	<meta property="og:type" content="website" />
	<meta property="fb:pages" content="https://www.facebook.com/mannahomethai/">

	<link rel="apple-touch-icon" sizes="57x57" href="{{ config('app.url') }}assets/images/favicon/apple-icon-57x57.png?=1.8">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ config('app.url') }}assets/images/favicon/apple-icon-60x60.png?=1.8">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ config('app.url') }}assets/images/favicon/apple-icon-72x72.png?=1.8">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ config('app.url') }}assets/images/favicon/apple-icon-76x76.png?=1.8">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ config('app.url') }}assets/images/favicon/apple-icon-114x114.png?=1.8">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ config('app.url') }}assets/images/favicon/apple-icon-120x120.png?=1.8">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ config('app.url') }}assets/images/favicon/apple-icon-144x144.png?=1.8">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ config('app.url') }}assets/images/favicon/apple-icon-152x152.png?=1.8">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}assets/images/favicon/apple-icon-180x180.png?=1.8">
	<link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}assets/images/favicon/android-icon-192x192.png?=1.8">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ config('app.url') }}assets/images/favicon/favicon-32x32.png?=1.8">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ config('app.url') }}assets/images/favicon/favicon-96x96.png?=1.8">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ config('app.url') }}assets/images/favicon/favicon-16x16.png?=1.8">
	<link rel="manifest" href="{{ config('app.url') }}assets/images/favicon/manifest.json?=1.8">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png?=1.8">
	<meta name="theme-color" content="#ffffff">

	<!--__________________ CSS __________________-->
	<!-- Bootstrap 4.5.0 CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/bootstrap-4.5.0/css/bootstrap.min.css">
	<!-- jquery .js -->
	<script type="text/javascript" src="{{ config('app.url') }}assets/plugins/jquery/jquery.min.js"></script>
	<!-- Font-Awesome CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/fontawesome-free/css/all.min.css">
	<!-- daterangepicker -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/daterangepicker/daterangepicker.css">
	<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/animate.css/animate.min.css">
  <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ config('app.url') }}assets/plugins/toastr/toastr.min.css">
	<!-- Fonts CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/fonts.css?=1.8">
	<!-- Global CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/style.css?=1.8">
	<!-- Footer CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/footer.css?=1.8">
	@if($page_en == 'home')
	<!-- Topnav float CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/topnav-float.css?=1.8">
	<!-- Topnav CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/topnav.css?=1.8">
	@else
	<!-- Topnav CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/topnav.css?=1.8">
	@endif

	@if($page_en == 'home')
	<!-- Home CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/page/home.css?=1.8">
	@elseif($page_en == 'about')
	<!-- About CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/page/about.css?=1.8">
	@elseif($page_en == 'contact')
	<!-- Contact CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/page/contact.css?=1.8">
	@elseif($page_en == 'donation')
	<!-- Donation CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/page/donation.css?=1.8">
	<!-- icheck -->
	<link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	@elseif($page_en == 'news')
	<!-- News CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/page/news_list.css?=1.8">
	@elseif($page_en == 'news_detail')
	<!-- News detail CSS -->
	<link rel="stylesheet" href="{{ config('app.url') }}assets/css/page/news_detail.css?=1.8">
	<!-- Light gallery -->
	<link type="text/css" rel="stylesheet" href="{{ config('app.url') }}assets/plugins/lightgallery.js-master/src/css/lightgallery.css">
	<!-- News gallery CSS -->
	<script src="{{ config('app.url') }}assets/plugins/GridLoadingEffects/js/modernizr.custom.js"></script>
	@endif

</head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BZVJP7C6GZ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-BZVJP7C6GZ');
</script>
