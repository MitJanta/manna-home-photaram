<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

@include('frontend.layouts.header')
@include('frontend.layouts.topnav-float')
@include('frontend.layouts.topnav')

<body>
    <section id="banner_section" style="background-image: linear-gradient(0deg, rgba(51, 153, 153, 0.47), rgba(51, 153, 153, 0.47)), url('{{ config('app.url') }}files_upload/contents/{{ $banner_data->image_name }}')">
        <div class="container">
            <div class="text-section">
                <p class="text-topic">{{ $banner_data->title_en }}</p>
                <p class="text-sub-topic">{!! $banner_data->content_en !!}</p>
            </div>
            <!-- Donation btn for mobile -->
            <div class="donation-float-btn d-md-none d-block">
                <a href="{{ route('donation') }}" class="row">
                    <img src="{{ config('app.url') }}assets/images/asset-11.png" alt="">
                    <div class="donate-text">
                        <p class="donate-sm">Make your</p>
                        <p class="donate-md">DONATION</p>
                    </div>
                </a>
            </div>
            <!-- .end Donation btn for mobile -->

        </div>
    </section>

    <section id="about_us">
        <img src="{{ config('app.url') }}assets/images/graphic-1.png" class="w-100 grapic-divider" alt="">

        <div id="card_float" class="w-100 d-md-block d-none">
            <div class="container">
                <div class="row">

                    <div class="col-lg-4 col-md-6">
                        <a href="{{ route('about') }}#who_we_are_section">
                            <div class="card-float bg-purple">
                                <img src="{{ config('app.url') }}assets/images/asset-1.png" alt="not found asset-1">
                                <p class="card-float-title">“Who we are”</p>
                                <p class="card-float-read-more">READ MORE</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <a href="{{ route('about') }}#what_we_do_section">
                            <div class="card-float bg-yellow">
                                <img src="{{ config('app.url') }}assets/images/asset-2.png" alt="not found asset-2">
                                <p class="card-float-title">“What we do” </p>
                                <p class="card-float-read-more">READ MORE</p>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-4 col-md-6 mx-auto">
                        <a href="{{ route('donation') }}">
                            <div class="card-float bg-lightblue">
                                <img src="{{ config('app.url') }}assets/images/asset-3.png" alt="not found asset-3">
                                <p class="card-float-title">“How you can help”</p>
                                <p class="card-float-read-more">READ MORE</p>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <div class="about-us-content">
            <div class="container">

                <!-- About us for desktop size -->
                <div class="row d-md-flex d-none">

                    <div class="col-lg-4 col-md-6">
                        <img src="{{ config('app.url') }}files_upload/contents/{{ $about_data->image_name }}" class="w-100" alt="not found asset-1">
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <p class="content-text-bold">ABOUT US</p>
                        <p class="content-title title-about">
                            Manna Home</br>
                            Photharam for </br>
                            <span class="bg-yellow" style="color: #2c2a35 !important; padding: 0px .15em;">
                                Woman & Children
                            </span></br>
                            Foundation
                        </p>
                        <p class="content-text">
                            {!! $about_data->content_th !!}
                        </p>

                        <p class="content-read-more-btn mt-4">
                            <a href="{{ route('about') }}">
                                READ MORE
                            </a>
                        </p>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <img src="{{ config('app.url') }}assets/images/asset-4.png" alt="" class="mb-4" style="max-height: 50px; margin-top: 3.5rem">
                        <p class="content-text">
                            {!! $about_data->content_en !!}
                        </p>
                        <img src="{{ config('app.url') }}files_upload/contents/{{ $about_data->link_url }}" class="w-100 mt-3" alt="">
                    </div>

                </div>
                <!-- .end About us for desktop size -->

                <!-- About us for Mobile & Tablet size -->
                <div class="row d-md-none d-flex">

                    <div class="col-12">
                        <p class="content-text-bold">ABOUT US</p>
                        <p class="content-title title-about">
                            Manna Home</br>
                            Photharam for </br>
                            Woman & Children</br>
                            <span class="bg-yellow" style="color: #2c2a35 !important; padding: 0px .15em;">
                                Foundation
                            </span>
                        </p>
                        <img src="{{ config('app.url') }}assets/images/asset-4.png" alt="" class="mb-4 icon" style="max-height: 50px;">
                        <p class="content-text mt-5">
                            {!! $about_data->content_th !!}
                        </p>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <p class="content-text mb-5">
                            {!! $about_data->content_en !!}
                        </p>
                        <img src="{{ config('app.url') }}files_upload/contents/{{ $about_data->link_url }}" class="w-100" alt="">
                    </div>

                </div>
                <!-- .end About us for Mobile & Tablet size -->

            </div>
        </div>
    </section>

    <section id="help_us">
        <img src="{{ config('app.url') }}files_upload/contents/{{ $help_us_data_eng->image_name }}" class="img-left d-lg-block d-none" alt="">
        <div class="container px-md-2 px-0">
            <!-- Help us Desktop size -->
            <div class="row d-md-flex d-none">

                <div class="col-lg-6">

                </div>

                <div class="col-lg-6 help-us-box">
                    <p class="content-text-bold">HELP US</p>
                    <p class="content-title">HOW YOU CAN
                        <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
                            HELP
                        </span>
                    </p>

                    <img src="{{ config('app.url') }}assets/images/asset-5.png" class="mt-5" alt="">
                    <p class="content-title mt-3">{{ $help_us_data_eng->title_th }}</p>
                    <p class="content-text-bold">{!! $help_us_data_eng->title_en !!}</p>
                    <p class="content-text">
                        {!! $help_us_data_eng->content_th !!}
                    </p>

                    <img src="{{ config('app.url') }}assets/images/asset-6.png" class="mt-5" alt="">
                    <p class="content-title mt-3">{{ $help_us_data_th->title_th }}</p>
                    <p class="content-text-bold">{!! $help_us_data_th->title_en !!}</p>
                    <p class="content-text">
                        {!! $help_us_data_th->content_th !!}
                    </p>
                </div>

            </div>
            <!-- .end Help us Desktop size -->

            <!-- Help us Mobile & Tablet size -->
            <div class="row d-md-none d-flex mx-0">

                <div class="col-12 help-us-box">
                    <p class="content-text-bold">HELP US</p>
                    <p class="content-title">HOW YOU CAN
                        <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">
                            HELP
                        </span>
                    </p>

                    <img src="{{ config('app.url') }}assets/images/asset-5.png" class="mt-5" alt="">
                    <p class="content-title mt-3">078-2-33270-4</p>
                    <p class="content-text-bold">Bank name: TMBThanachart Bank</p>
                    <p class="content-text">
                        Bank Account name: Manna Home Photharam
                        for Woman & Children Foundation
                    </p>

                    <img src="{{ config('app.url') }}assets/images/asset-6.png" class="mt-5" alt="">
                    <p class="content-title mt-3">078-2-33270-4</p>
                    <p class="content-text-bold">ธนาคารทหารไทยธนชาต</p>
                    <p class="content-text">
                        ชื่อบัญชี มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก
                    </p>
                </div>

            </div>
            <!-- .end Help us Mobile & Tablet size -->

        </div>
    </section>

    <section id="img_divider">
        <img src="{{ config('app.url') }}assets/images/graphic-2.png" alt="">
        <img src="{{ config('app.url') }}files_upload/contents/{{ $image_divider->image_name }}" alt="">
    </section>

    <section id="news_activity">
        <div class="container">
            <p class="content-title text-center">
                <span class="bg-yellow" style="color: black !important; padding: 0px .2em;">NEWS</span>
                & ACTIVITY
            </p>
            <div class="row mt-5">

                @php $i = 1; @endphp
                @foreach($news_list as $row)
                <div class="col-lg-4 col-md-6 mx-auto @if($i < 4) d-block @else d-lg-none d-md-block d-none @endif" style="padding: 0px 10px">
                    <div class="card-news-acts">
                        <div class="img-news">
                            <img src="{{ config('app.url') }}files_upload/news_acts/{{ $row->cover }}" class="w-100" alt="">
                            <div class="news-date d-md-none d-block">
                                <div class="news-date-num">{{ date("d",strtotime($row->date)) }}</div>
                                <div class="news-date-month">{{ ucfirst(substr(date("M",strtotime($row->date)), 0, 3))." ".date("Y",strtotime($row->date)) }}</div>
                            </div>
                        </div>
                        <div class="news-detail d-flex">
                            <div class="news-date d-md-block d-none">
                                <div class="news-date-num">{{ date("d",strtotime($row->date)) }}</div>
                                <div class="news-date-month">{{ ucfirst(substr(date("M",strtotime($row->date)), 0, 3))." ".date("Y",strtotime($row->date)) }}</div>
                            </div>
                            <div class="news-text">
                                <p class="news-text-by w-100">
                                    By <span class="text-yellow">{{ $row->author }}</span>
                                </p>
                                <a href="{{ route('news_acts_detail', [1, $row->id]) }}">
                                    <p class="news-text-detail mt-n2">
                                        {{ $row->title }}
                                    </p>

                                    <p class="news-text-link w-100 mt-n2">
                                        Learn More
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
                @endforeach

            </div>
            <p class="content-read-more-btn text-center mt-md-5 mt-3 mx-auto d-block">
                <a href="{{ route('news_acts', 1) }}">
                    READ MORE
                </a>
            </p>
        </div>
    </section>

</body>

<!-- include footer -->
@include('frontend.layouts.footer')

<script type="text/javascript">
    var navbar_float = $("#navbar-float");
    var navbar = document.getElementById("topnav");

    window.onscroll = function() {
        onScrollFunction()
    };
    // เปลี่ยน Topnav menu ตอน scroll
    function onScrollFunction() {
        // var navbar_float = document.getElementById("navbar-float");
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            $("#navbar-float").fadeOut('300', 'linear');
            $("#topnav-normal").css('opacity', '100');
            $("#topnav-normal").fadeIn('300', 'linear');
        } else {
            $("#navbar-float").fadeIn('300', 'linear');
            $("#topnav-normal").fadeOut('300', 'linear');
        }
    }
</script>
