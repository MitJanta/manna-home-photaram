<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    protected $table = 'admins_logs';

    protected $primaryKey = 'id';

    protected $fillable = [
    'log_ip',
    'log_url',
    'log_method',
    'log_desc',
    'log_agent',
    'admin_id'
  ];

    protected $hidden = [
    'id',
    // 'created_at',
    // 'updated_at'
  ];
}
