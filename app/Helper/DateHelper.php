<?php
namespace App\Helper;

use DateTime;
use Carbon\Carbon;

class DateHelper
{
    // Generate datetime + Micro second
    public static function dateTimeInMicro()
    {
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('y-m-d H:i:s.'.$micro, $t));
        return $d->format("dmyHisu");
    }

    // $date รับมา -> 2021-09-21 09:45:59 ออกไปเป็น พศ. -> 21 ก.ย. 2021 [clock icon] 09:45:59
    public static function dateToThaiFormat($date)
    {
        $ex_date_time = explode(' ', $date);
        $time = '';
        if (isset($ex_date_time[1])) { // Check if is DateTime
            // Set Time format
            $time_explode = explode(':', $ex_date_time[1]);
            $time = ' <small><i class="far fa-clock"></i> '.$time_explode[0].':'.$time_explode[1].'</small>';
        }

        // Set Date format
        $ex_date = explode('-', $ex_date_time[0]);
        $ex_date[1] = DateHelper::monthToThaiFormatShort($ex_date[1]);
        $ex_date[0] = $ex_date[0]+543;
        $date = $ex_date[2] . " " . $ex_date[1] . " " . $ex_date[0];

        return $date_time = $date.$time;
    }

    // $date รับมา -> 2021-09-21 09:45:59 ออกไปเป็น คศ. -> 21 ก.ย. 2021 [clock icon] 09:45:59
    public static function dateToThaiFormatEngYear($date)
    {
        $ex_date_time = explode(' ', $date);
        $time = '';
        if (isset($ex_date_time[1])) { // Check if is DateTime
            // Set Time format
            $time_explode = explode(':', $ex_date_time[1]);
            $time = ' <small><i class="far fa-clock"></i> '.$time_explode[0].':'.$time_explode[1].'</small>';
        }

        // Set Date format
        $ex_date = explode('-', $ex_date_time[0]);
        $ex_date[1] = DateHelper::monthToThaiFormatShort($ex_date[1]);
        $ex_date[0] = $ex_date[0];
        $date = $ex_date[2] . " " . $ex_date[1] . " " . ((int)$ex_date[0]+543);

        return $date_time = $date.$time;
    }

    // $date รับมา -> 2021-09-21 09:45:59 ออกไปเป็น คศ. -> 21 SEP 2021 [clock icon] 09:45:59
    public static function dateToEngFormatEngYear($date)
    {
        $ex_date_time = explode(' ', $date);
        $time = '';
        if (isset($ex_date_time[1])) { // Check if is DateTime
            // Set Time format
            $time_explode = explode(':', $ex_date_time[1]);
            $time = ' <small><i class="far fa-clock"></i> '.$time_explode[0].':'.$time_explode[1].'</small>';
        }

        // Set Date format
        $ex_date = explode('-', $ex_date_time[0]);
        $ex_date[1] = DateHelper::monthToEngFormatShort($ex_date[1]);
        $ex_date[0] = $ex_date[0];
        $date = $ex_date[2] . " " . $ex_date[1] . " " . $ex_date[0];

        return $date_time = $date.$time;
    }

    // $birth_date รับมา -> 21 ก.ย. 2021 ออกไปเป็น -> 2021-09-21
    public static function thaiFormatToDate($birth_date)
    {
        // Set format birth date
        $ex_date = explode(' ', $birth_date);
        if ($ex_date[1] == 'ม.ค.') {
            $ex_date[1] = '01';
        } elseif ($ex_date[1] == 'ก.พ.') {
            $ex_date[1] = '02';
        } elseif ($ex_date[1] == 'มี.ค.') {
            $ex_date[1] = '03';
        } elseif ($ex_date[1] == 'เม.ย.') {
            $ex_date[1] = '04';
        } elseif ($ex_date[1] == 'พ.ค.') {
            $ex_date[1] = '05';
        } elseif ($ex_date[1] == 'มิ.ย.') {
            $ex_date[1] = '06';
        } elseif ($ex_date[1] == 'ก.ค.') {
            $ex_date[1] = '07';
        } elseif ($ex_date[1] == 'ส.ค.') {
            $ex_date[1] = '08';
        } elseif ($ex_date[1] == 'ก.ย.') {
            $ex_date[1] = '09';
        } elseif ($ex_date[1] == 'ต.ค.') {
            $ex_date[1] = '10';
        } elseif ($ex_date[1] == 'พ.ย.') {
            $ex_date[1] = '11';
        } elseif ($ex_date[1] == 'ธ.ค.') {
            $ex_date[1] = '12';
        }
        $year = $ex_date[2];
        $ex_date[2] = $year - 543;
        $birth_date = $ex_date[2] . "-" . $ex_date[1] . "-" . $ex_date[0];
        return $birth_date;
    }

    // $date รับมา -> 2021-09-21 09:45:59 ออกไปเป็น พศ. (ชื่อเดือนแบบเต็ม) -> 21 กันยายน 2021 [clock icon] 09:45:59
    public static function dateToThaiFullFormat($date)
    {
        $ex_date_time = explode(' ', $date);
        $time = '';
        if (isset($ex_date_time[1])) { // Check if is DateTime
            // Set Time format
            $time_explode = explode(':', $ex_date_time[1]);
            $time = ' <small><i class="far fa-clock"></i> '.$time_explode[0].':'.$time_explode[1].'</small>';
        }

        // Set Date format
        $ex_date = explode('-', $ex_date_time[0]);
        $ex_date[1] = monthToThaiFormat($ex_date[1]);
        $ex_date[0] = $ex_date[0]+543;
        $date = $ex_date[2] . " " . $ex_date[1] . " " . $ex_date[0];

        return $date_time = $date.$time;
    }

    // แปลง format date หรือ datetime เป็น date -> 23/09/2021
    public static function dateToSlashThaiFormat($date)
    {
        return date('d/m/Y', strtotime($date));
    }

    // แปลง format date หรือ datetime เป็น date -> 23/09/2021 H:i
    public static function dateTimeToSlashThaiFormat($date)
    {
        return date('d/m/Y H:i', strtotime($date));
    }

    // แปลง format date หรือ datetime เป็น date -> 23/09/2021 H:i
    public static function dateTimeToSlashFormat($date)
    {
        return date('Y/m/d H:i', strtotime($date));
    }

    // แปลง format 23/09/2021 เป็น date -> date
    public static function slashToDateFormat($slash_date)
    {
        $ex_date = explode('/', $slash_date);
        return date('Y-m-d', strtotime($ex_date[2].'-'.$ex_date[1].'-'.$ex_date[0]));
    }

    public static function monthToThaiFormat($month)
    {
        $month = abs($month);
        if ($month == 1) {
            $month = 'มกราคม';
        } elseif ($month == 2) {
            $month = 'กุมภาพันธ์';
        } elseif ($month == 3) {
            $month = 'มีนาคม';
        } elseif ($month == 4) {
            $month = 'เมษายน';
        } elseif ($month == 5) {
            $month = 'พฤษภาคม';
        } elseif ($month == 6) {
            $month = 'มิถุนายน';
        } elseif ($month == 7) {
            $month = 'กรกฎาคม';
        } elseif ($month == 8) {
            $month = 'สิงหาคม';
        } elseif ($month == 9) {
            $month = 'กันยายน';
        } elseif ($month == 10) {
            $month = 'ตุลาคม';
        } elseif ($month == 11) {
            $month = 'พฤศจิกายน';
        } elseif ($month == 12) {
            $month = 'ธันวาคม';
        }
        return $month;
    }

    public static function monthToThaiFormatShort($month)
    {
        $month = abs($month);
        if ($month == 1) {
            $month = 'ม.ค.';
        } elseif ($month == 2) {
            $month = 'ก.พ.';
        } elseif ($month == 3) {
            $month = 'มี.ค.';
        } elseif ($month == 4) {
            $month = 'เม.ย.';
        } elseif ($month == 5) {
            $month = 'พ.ค.';
        } elseif ($month == 6) {
            $month = 'มิ.ย.';
        } elseif ($month == 7) {
            $month = 'ก.ค.';
        } elseif ($month == 8) {
            $month = 'ส.ค.';
        } elseif ($month == 9) {
            $month = 'ก.ย.';
        } elseif ($month == 10) {
            $month = 'ต.ค.';
        } elseif ($month == 11) {
            $month = 'พ.ย.';
        } elseif ($month == 12) {
            $month = 'ธ.ค.';
        }
        return $month;
    }

    public static function monthToEngFormatShort($month)
    {
        $month = abs($month);
        if ($month == 1) {
            $month = 'JAN';
        } elseif ($month == 2) {
            $month = 'FEB';
        } elseif ($month == 3) {
            $month = 'MAR';
        } elseif ($month == 4) {
            $month = 'APR';
        } elseif ($month == 5) {
            $month = 'MAY';
        } elseif ($month == 6) {
            $month = 'JUN';
        } elseif ($month == 7) {
            $month = 'JULY';
        } elseif ($month == 8) {
            $month = 'AUG';
        } elseif ($month == 9) {
            $month = 'SEP';
        } elseif ($month == 10) {
            $month = 'OCT';
        } elseif ($month == 11) {
            $month = 'NOV';
        } elseif ($month == 12) {
            $month = 'DEC';
        }
        return $month;
    }

    public static function dateTimeToHi($datetime)
    {
        return (new \DateTime($datetime))->format("H:i");
    }
}
