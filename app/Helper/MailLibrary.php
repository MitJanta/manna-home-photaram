<?php

namespace App\Helper;

use DateTime;

use Auth;
use File;

use App\Content;
use App\Helper\DateHelper;
use App\Helper\AccountLibrary;
use App\Helper\GeneralLibrary;
use App\Helper\ConstantHelper;

use App\Http\Controllers\Controller;
use PHPMailer\PHPMailer;

class MailLibrary
{
    public static function sendContactMail($array)
    {
        $date = DateHelper::dateToThaiFormat(date('Y-m-d')).' เวลา '.DateHelper::dateTimeToHi(date('Y-m-d H:i:s')).' น.';
        $mail = new PHPMailer\PHPMailer();

        $contact_email = Content::where('ref_name', 'contact_email')->where('page', 'contact')->first();
        $contact_address = Content::where('ref_name', 'contact_address')->where('page', 'contact')->first();
        $contact_tel_1 = Content::where('ref_name', 'contact_tel_1')->where('page', 'contact')->first();
        $contact_tel_2 = Content::where('ref_name', 'contact_tel_2')->where('page', 'contact')->first();

        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>'.'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก'.'</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="margin: 0; padding: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#edeeef; max-width:699px;">
        <tr style="background-color:#ffb300;">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="right">
            <center>
              <a href="'.config('app.url').'"><img src="'.config('app.url').'assets/images/favicon/android-icon-192x192.png" width="100" style="padding:20px 10px;"></a>
            </center>
          </td>
          <td style="padding:0px 10px;">
            <span style="color: #082b57; font-family: Arial, sans-serif; font-size: 17px; padding-top: 25px; float:left;">
            <span style="width: 100%; font-size:1.3em; float:left;"><b>ข้อความติดต่อ !</b></span>
            </br>
            <span style="width: 100%; float:left;">
            ชื่อ-นามสกุล : '.$array['first_name'].' '.$array['last_name'].'
            </span>
            <span style="width: 100%; float:left;">
            เบอร์โทร : '.$array['telephone'].'
            </span>
            <span style="width: 100%; float:left;">
            อีเมล : '.$array['email'].'
            </span>
            <span style="width: 100%; float:left;">
            ข้อความ : '.$array['message'].'
            </span>
            </br>
            <small style="width: 100%; color:#0275d8; margin:6px 0px 20px 0px; float:left;">
              <b>หมายเหตุ</b> ข้อความติดต่อจะแสดงในระบบหลังบ้านด้วย
            </small>
            </span>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <hr />
            <span style="width: 100%; float:left; color: #153643; font-family: Arial, sans-serif; font-size: 15px; padding: 12px 30px 22px 30px;">
            <span style="font-size:1.1em;"><b>'.'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก'.'</b></span><br />
            '.str_replace('<br>', '<div style="clear: both"></div>', str_replace('</br>', '<div style="clear: both"></div>', $contact_address->content_th)).'<br><br />
            โทร. '.$contact_tel_1->content_th.', '.$contact_tel_2->content_th.' &nbsp;&nbsp; อีเมล : '.$contact_email->content_th.'
            </span>
          </td>
        </tr>
      </table>
    </body>
    </html>';

        // $delivery_email = 'art@nanobey.com';
        // $delivery_email = $email;
        $delivery_email = config('app.mail_username');
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        // $mail->SMTPSecure = env('MAIL_ENCRYPTION'); // secure transfer enabled REQUIRED for GMail
        $mail->Host = config('app.mail_host');
        $mail->Port = config('app.mail_port'); // or 587

        $mail->SMTPOptions = array(
          'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true)
        );

        $mail->IsHTML(true);
        $mail->Username = config('app.mail_username');
        $mail->Password = config('app.mail_password');
        $mail->CharSet="utf-8";
        $mail->SetFrom(config('app.mail_username'), config('app.mail_from_name'));
        $mail->Subject = "ข้อความติดต่อเรา ".$date;
        $mail->Body = $body;
        $mail->AddAddress($delivery_email);
        // $mail->addBcc("mitnanobey@gmail.com");

        $mail->Timeout = 6000;

        if ($mail->Send()) {
            // dd('test send mail');
            return true;
        } else {
            return false;
            // return $mail->ErrorInfo;
        }
    }

    public static function sendDonationMail($array)
    {
        $date = DateHelper::dateToThaiFormat(date('Y-m-d')).' เวลา '.DateHelper::dateTimeToHi(date('Y-m-d H:i:s')).' น.';
        $mail = new PHPMailer\PHPMailer();

        $contact_email = Content::where('ref_name', 'contact_email')->where('page', 'contact')->first();
        $contact_address = Content::where('ref_name', 'contact_address')->where('page', 'contact')->first();
        $contact_tel_1 = Content::where('ref_name', 'contact_tel_1')->where('page', 'contact')->first();
        $contact_tel_2 = Content::where('ref_name', 'contact_tel_2')->where('page', 'contact')->first();

        $text_data = '<span style="width: 100%; float:left;">
        ชื่อ-นามสกุล : '.$array['first_name'].' '.$array['last_name'].'
        </span>';

        if (!empty($array['email'])) {
            $text_data .= '<span style="width: 100%; float:left;">อีเมล : '.$array['email'].'</span>';
        }

        $text_data .= '<span style="width: 100%; float:left;">จำนวนเงิน : '.number_format($array['money_amount'], 0).' บาท
        </span>';

        if (!empty($array['donate_for'])) {
            $text_data .= '<span style="width: 100%; float:left;">
          บริจาคเพื่อ : '.$array['donate_for'].'
          </span>';
        }

        if (!empty($array['paid_by'])) {
            if ($array['paid_by'] == 0) {
                $paid_by = 'เงินสด';
            } elseif ($array['paid_by'] == 1) {
                $paid_by = 'โอนผ่านธนาคาร';
            }
            $text_data .= '<span style="width: 100%; float:left;">
          ชำระโดย : '.$paid_by.'
          </span>';
        }

        if (!empty($array['paid_by'])) {
            if ($array['paid_by'] == 1) {
                $text_data .= '<span style="width: 100%; float:left;">';
                if (!empty($array['bank_name'])) {
                    $text_data .= 'ชื่อธนาคาร : '.$row['bank_name'];
                }
                if (!empty($array['bank_other'])) {
                    $text_data .= 'ธนาคารอื่นๆ : '.$row['bank_other'];
                }
                if (!empty($array['bank_branch'])) {
                    $text_data .= 'สาขา : '.$row['bank_branch'];
                }
                if (!empty($array['date'])) {
                    $text_data .= 'เวลา : '.DateHelper::dateToThaiFormat($array['date']);
                }
                $text_data .= '</span>';
            }
        }

        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>'.'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก'.'</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="margin: 0; padding: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#edeeef; max-width:699px;">
        <tr style="background-color:#ffb300;">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="150" align="right">
            <center>
              <a href="'.config('app.url').'"><img src="'.config('app.url').'assets/images/favicon/android-icon-192x192.png" width="100" style="padding:20px 10px;"></a>
            </center>
          </td>
          <td style="padding:0px 10px;">
            <span style="color: #082b57; font-family: Arial, sans-serif; font-size: 17px; padding-top: 25px; float:left;">
            <span style="width: 100%; font-size:1.3em; float:left;"><b>ข้อมูลการบริจาค !</b></span>
            </br>'.$text_data.'
            </br>
            <small style="width: 100%; color:#0275d8; margin:6px 0px 20px 0px; float:left;">
              <b>หมายเหตุ</b> ข้อมูลการบริจาคจะแสดงในระบบหลังบ้านด้วย
            </small>
            </span>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <hr />
            <span style="width: 100%; float:left; color: #153643; font-family: Arial, sans-serif; font-size: 15px; padding: 12px 30px 22px 30px;">
            <span style="font-size:1.1em;"><b>'.'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก'.'</b></span><br />
            '.str_replace('<br>', '<div style="clear: both"></div>', str_replace('</br>', '<div style="clear: both"></div>', $contact_address->content_th)).'<br><br />
            โทร. '.$contact_tel_1->content_th.', '.$contact_tel_2->content_th.' &nbsp;&nbsp; อีเมล : '.$contact_email->content_th.'
            </span>
          </td>
        </tr>
      </table>
    </body>
    </html>';

        // $delivery_email = 'art@nanobey.com';
        // $delivery_email = $email;
        $delivery_email = config('app.mail_donate_address');
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        // $mail->SMTPSecure = env('MAIL_ENCRYPTION'); // secure transfer enabled REQUIRED for GMail
        $mail->Host = config('app.mail_host');
        $mail->Port = config('app.mail_port'); // or 587

        $mail->SMTPOptions = array(
          'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true)
        );

        $mail->IsHTML(true);
        $mail->Username = config('app.mail_donate_address');
        $mail->Password = config('app.mail_donate_password');
        $mail->CharSet="utf-8";
        $mail->SetFrom(config('app.mail_donate_address'), config('app.mail_from_name'));
        $mail->Subject = "ข้อมูลการบริจาค ".$date;
        $mail->Body = $body;
        $mail->AddAddress($delivery_email);
        // $mail->addBcc("mitnanobey@gmail.com");

        $mail->Timeout = 6000;

        if ($mail->Send()) {
            // dd('test send mail');
            return true;
        } else {
            return false;
            // return $mail->ErrorInfo;
        }
    }
}
