<?php

namespace App\Helper;

use Auth;
use Request;

use App\AdminLog;

class AdminLibrary
{
    public static function logging(string $message)
    {
        $id = Auth::guard('admin')->user()->id;

        $log = [];
        $log['log_desc'] = $message;
        $log['log_url'] = Request::fullUrl();
        $log['log_method'] = Request::method();
        $log['log_ip'] = Request::getClientIp();
        $log['log_agent'] = Request::header('user-agent');
        $log['admin_id'] = $id;
        AdminLog::create($log);
    }
}
