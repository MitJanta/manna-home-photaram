<?php

namespace App\Helper;

use DateTime;
use File;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class GeneralLibrary
{
    public static function getErrorMessage($column, $message)
    {
        // {"name":["The name field is required."]}
        // $error_array = array();
        // array_push($error_array, $message);
        // $error = array($column => $error_array);
        // $error = json_encode($error);
        $error = [$message];
        return $error;
    }

    public static function generateTimestamp()
    {
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
        return $d->format("YmdHisu");
    }

    public static function base64StringToPNGImage($base64Image)
    {
        $content = str_replace('data:image/png;base64,', '', $base64Image);
        $content = str_replace(' ', '+', $content);
        return base64_decode($content);
    }

    public static function PNGImageToBase64String($content)
    {
        $base64 = 'data:image/png;base64,' . base64_encode($content);
        return $base64;
    }

    public static function setDateFormat($datetime)
    {
        $ex_datetime = explode(" ", $datetime);
        $date = $ex_datetime[0];
        $ex_date = explode("-", $date);
        $date_txt = $ex_date[2]."/".$ex_date[1]."/".$ex_date[0];
        return $date_txt;
    }

    public static function th_mb_strlen($str)
    {
        $new = preg_replace('/[^0-9A-Za-zก-ฮ๐-๙]/u', '', $str); // ตัดทุกอย่างนอกเหนือขอบเขตที่ระบุไว้ทิ้ง
        return mb_strlen($new, 'UTF8');
    }

    public static function th_substr($string, $size)
    {
        $split_length = 1;
        mb_internal_encoding('UTF-8');
        mb_regex_encoding('UTF-8');
        $split_length = ($split_length <= 0) ? 1 : $split_length;
        $mb_strlen = mb_strlen($string, 'utf-8');
        $array = array();
        $i = 0;
        while ($i < $mb_strlen) {
            $array[] = mb_substr($string, $i, $split_length);
            $i = $i+$split_length;
        }

        $start = 0;
        $length = $size;
        $count = 0;
        $desc = "";

        for ($i=0; $i < count($array); $i++) {
            $ascii = ord(iconv("UTF-8", "TIS-620", $array[$i]));
            if ($ascii == 209 || ($ascii >= 212 && $ascii <= 218) || ($ascii >= 231 && $ascii <= 238)) {
                $length++;
            }

            if ($i >= $start) {
                $desc .= $array[$i];
            }

            if ($i >= $length) {
                break;
            }
        }
        return $desc."...";
    }

    public static function getEndcodeWithKey($key_vc, $code)
    {
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key_vc);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($code, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

    public static function getDateQueryFomat($date, $type)
    {

        //รับ $date มาเป็น 01/01/2020 => return ออกไปใน format 2020-01-01
        $start_date_query = '';
        $end_date_query = '';
        $start_year = '';
        $end_year = '';

        if ($date != '') {
            $date_picker = explode(' - ', trim($date));
            $start_date = $date_picker[0];
            $end_date = $date_picker[1];
            $start_date_arr = explode('/', $start_date);
            $start_year = $start_date_arr[2];

            if (strlen($start_date_arr[2]) > 4) {
                $start_year =  substr($start_year, 0, 4);
            }

            $end_date_arr = explode('/', $end_date);
            $end_year = $end_date_arr[2];
            if (strlen($end_date_arr[2]) > 4) {
                $end_year =  substr($end_year, 0, 4);
            }

            $start_date_query = $start_year.'-'.$start_date_arr[1].'-'.$start_date_arr[0];
            $end_date_query = $end_year.'-'.$end_date_arr[1].'-'.$end_date_arr[0];

            if ($type == 'datetime' || $type == 'time') {
                $start_date_query.' 00:00:00';
                $end_date_query.' 23:59:59';
            }
        }
        return $start_date_query.' - '.$end_date_query;
    }

    public static function getDecodeWithKey($key_vc, $code)
    {
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key_vc);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($code), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

    public static function getProvinceName($province_code)
    {
        $province_txt = "";
        if ($province_code != "") {
            $ex_code = explode(",", $province_code);
            if (count($ex_code) > 0) {
                foreach ($ex_code as $data) {
                    $code = $data;
                    if ($code != "[]") {
                        $province = DB::table('tbl_province')->Where('province_code_txt', '=', $code)->first();
                        $province_txt .= $province->province_name.", ";
                    }
                }
                $province_txt = substr($province_txt, 0, -2);
            } else {
                $province = DB::table('tbl_province')->Where('province_code_txt', '=', $province_code)->first();
                $province_txt .= $province->province_name;
            }
        }
        return $province_txt;
    }

    public static function calculateHmac($text, $key)
    {
        $signature = strtoupper(hash_hmac('sha256', $text, $key, false));
        return $signature;
    }

    public static function generateQrcode($num, $salt)
    {
        $random = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $no = 5 - strlen($num);
        $txt = '';
        for ($i=0;$i<$no;$i++) {
            $txt = $txt.'0';
        }
        $_encoded = $txt.$num.$random;
        // $bin1 = GeneralLibrary::strigToBinary($dn);
        // $bin1 = hexbin($dn);
        // $_salt = $salt.substr($salt,0,3); // 38693164777a386931
        // $bin2 = GeneralLibrary::strigToBinary($_salt);
        // $bin3 = $bin1 | $bin2;
        // $bin3 = GeneralLibrary::ref_format($bin3, 6);
        // $bin3 = GeneralLibrary::binaryToString($bin3).PHP_EOL;
        // $bin3 = base_convert($bin3,2,10);
        return $_encoded;
    }

    public static function strigToBinary($string)
    {
        $characters = str_split($string);
        $binary = [];
        foreach ($characters as $character) {
            $data = unpack('H*', $character);
            $binary[] = base_convert($data[1], 16, 2);
        }
        return implode('', $binary);
    }

    public static function binaryToString($binary)
    {
        $binaries = explode(' ', $binary);
        $string = null;
        foreach ($binaries as $binary) {
            $string .= pack('H*', dechex(bindec($binary)));
        }
        return $string;
    }

    public static function ref_format($str, $step, $reverse = false)
    {
        if ($reverse) {
            return strrev(chunk_split(strrev($str), $step, ' '));
        }
        return chunk_split($str, $step, ' ');
    }

    public static function generateRandomString()
    {
        $allowed = str_split('23456789abcdefghjkmnpqrstuvwxyz'); // it is enough to do it once
        $res = '';
        foreach (array_rand($allowed, 4) as $k) {
            $res .= $allowed[$k];
        }
        return $res;
    }

    public static function getDateTimeNow()
    {
        $now = Carbon::now()->toDateTimeString();
        return $now;
    }

    public static function getDateTimeNext30()
    {
        $now = Carbon::now()->toDateTimeString();
        $date_time_next30 = date('Y-m-d H:m:s', strtotime("+30 day", strtotime($now)));
        return $date_time_next30;
    }

    public static function diffDate($start_date, $end_date)
    {
        $start_date = strtotime($start_date);
        $end_date = strtotime($end_date);
        $diff = $end_date - $start_date;
        $date_left = floor($diff/(60)/60/24);
        return abs($date_left);
    }

    public static function binaryArrayToInt($option_array, $value_no)
    {
        $type_binary = '';
        foreach ($option_array as $row) {
            $bt = '0';
            if (!empty($value_no)) {
                if (in_array($row->no, $value_no)) {
                    $bt = '1';
                }
            }
            $type_binary .= $bt;
        }
        $type_int = bindec((binary) $type_binary);
        return $type_int;
    }

    public static function intToBinaryText($option_array, $type_int)
    {
        $type_binary = '';
        $option_count = count($option_array);
        $type_string = (string) decbin($type_int);
        for ($i = 0; $i < $option_count; $i++) {
            $bt = '0';
            if (!empty($type_string[$option_count-(1+$i)])) {
                if ($type_string[$option_count-(1+$i)] == '1') {
                    $bt = '1';
                }
            }
            $type_binary = $bt.$type_binary;
        }
        return (string) $type_binary;
    }

    public static function hashAndReplaceSlash($data)
    {
        $hashed_data = Hash::make($data);
        $replaced_hashed = $this->ReplaceSlash($hashed_data);
        return $replaced_hashed;
    }

    public static function ReplaceSlash($data)
    {
        $replaced_hashed1 = str_replace('/', '|', $data);
        $replaced_hashed2 = str_replace('.', '-', $replaced_hashed1);
        return $replaced_hashed2;
    }

    public static function ReplaceToSlash($hashed)
    {
        $replaced_hashed1 = str_replace('|', '/', $hashed);
        $replaced_hashed2 = str_replace('-', '.', $replaced_hashed1);
        return $replaced_hashed2;
    }

    public static function BinaryToArray($binary_txt)
    {
        $ar = array();
        $size = strlen($binary_txt);
        $n = 0;
        for ($len = 1; $len <= $size; $len++) {
            $str = substr($binary_txt, $n, 1);
            if ($str == 1) {
                array_push($ar, $len);
            }
            $n++;
        }
        return $ar;
    }

    public static function arrayPaginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    // Upload base64 to image
    public static function upload_image($path, $image_base64)
    {
        list($type, $image_base64) = explode(';', $image_base64);
        list(, $image_base64)      = explode(',', $image_base64);
        $image_base64 = base64_decode($image_base64);
        file_put_contents($path, $image_base64);
    }
}
