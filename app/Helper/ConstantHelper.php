<?php

namespace App\Helper;

class ConstantHelper
{
    const TMP_FOLDERS_PATH = 'tmp_folders/';
    const NEWS_COVER_PATH = 'files_upload/news_acts/';
    const CONTENT_IMAGE_PATH = 'files_upload/contents/';
    const DONATE_ATTACHE_PATH = 'files_upload/donate_attaches/';

    const UPLOAD_PATH = 'files_upload/files/';

    public static function BASE_URL()
    {
        return config('app.url');
    }
}
