<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactSubmit extends Model
{
    protected $table = 'contact_submit';

    protected $primaryKey = 'id';

    protected $fillable = [
      'first_name',
      'last_name',
      'telephone',
      'email',
      'message',
    ];

    protected $hidden = [
      'created_at',
    ];
}
