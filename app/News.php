<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news_acts';

    protected $primaryKey = 'id';

    protected $fillable = [
      'title',
      'content',
      'author',
      'date',
      'cover',
      'gallery',
      'views',
      'enable',
      'updated_at',
  ];

    protected $hidden = [
      'created_at',
  ];
}
