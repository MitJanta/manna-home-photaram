<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationSubmit extends Model
{
    protected $table = 'donate_submit';

    protected $primaryKey = 'id';

    protected $fillable = [
      'first_name',
      'last_name',
      'email',
      'donate_for',
      'paid_by',
      'bank_id',
      'bank_other',
      'bank_branch',
      'date',
      'money_amount',
      'attache_file',
    ];

    protected $hidden = [
      'created_at',
    ];
}
