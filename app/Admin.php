<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
      'name',
      'email',
      'password',
      'permission_1',
      'permission_2',
      'permission_3',
      'permission_4'
  ];

    protected $hidden = [
      'id',
      'email',
      'type'
  ];

    // Declare constant variable for call Admin type
    const TRUE_TYPE = 1;
    const FALSE_TYPE = 0;

    public function isSuperAdmin()
    {
        if ($this->type === self::TRUE_TYPE) {
            return self::TRUE_TYPE;
        } elseif ($this->type === self::FALSE_TYPE) {
            return self::FALSE_TYPE;
        }
    }

    public function isPermission1()
    {
        return $this->permission_1 === self::TRUE_TYPE;
    }

    public function isPermission2()
    {
        return $this->permission_2 === self::TRUE_TYPE;
    }

    public function isPermission3()
    {
        return $this->permission_3 === self::TRUE_TYPE;
    }

    public function isPermission4()
    {
        return $this->permission_4 === self::TRUE_TYPE;
    }
}
