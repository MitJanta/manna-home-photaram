<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $table = 'files_upload';

    protected $fillable = [
    'description',
    'file_name',
    'enable',
    'created_at',
];

    protected $hidden = [
    'id',
    'updated_at'
];
}
