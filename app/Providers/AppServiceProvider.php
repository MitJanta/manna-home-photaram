<?php

namespace App\Providers;

use App\Content;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $footer_data = Content::where('ref_name', 'footer_data')->where('page', 'global')->first();
        View::share('footer_global_data', $footer_data);

        $contact_email = Content::where('ref_name', 'contact_email')->where('page', 'contact')->first();
        $contact_address = Content::where('ref_name', 'contact_address')->where('page', 'contact')->first();
        $contact_tel_1 = Content::where('ref_name', 'contact_tel_1')->where('page', 'contact')->first();
        $contact_tel_2 = Content::where('ref_name', 'contact_tel_2')->where('page', 'contact')->first();
        $contact_array_global_data = array(
          'email' => $contact_email->content_th,
          'address' => $contact_address->content_th,
          'tel_1' => $contact_tel_1->content_th,
          'tel_2' => $contact_tel_2->content_th,
        );
        View::share('contact_array_global_data', $contact_array_global_data);
    }
}
