<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\News;
use App\Content;

class NewsActs extends Controller
{
    public function index(int $page_num = 1)
    {
        $data['page_th'] = 'ข่าวและกิจกรรม';
        $data['page_en'] = 'news';

        $data['banner_data'] = Content::where('ref_name', 'news_banner')->where('page', 'news')->first();

        $per_page = 6;

        $data['news_list'] = News::where('enable', 1)->orderBy('created_at', 'desc');
        $num_rows = $data['news_list']->count();
        // Paginate news_list
        $data['news_list'] = $data['news_list']->paginate($per_page, ['*'], 'page', $page_num);
        $data['max_page'] = ceil($num_rows / $per_page);
        $data['page_num'] = $page_num;

        if ($page_num > $data['max_page'] && $data['max_page'] != 0) {
            return redirect()->route('news_acts', $data['max_page']);
        }

        return view('frontend.news', $data);
    }

    public function detail(int $page_num = 1, int $id)
    {
        $data['news_data'] = News::find($id);

        $data['page_th'] = $data['news_data']->news_title;
        $data['page_en'] = 'news_detail';

        $data['banner_data'] = Content::where('ref_name', 'news_banner')->where('page', 'news')->first();

        return view('frontend.news_detail', $data);
    }
}
