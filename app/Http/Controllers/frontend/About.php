<?php

namespace App\Http\Controllers\frontend;

use App\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class About extends Controller
{
    public function index()
    {
        $data['page_th'] = 'เกี่ยวกับเรา';
        $data['page_en'] = 'about';

        // banner img
        $data['banner_data'] = Content::where('ref_name', 'about_banner')->where('page', 'about')->first();
        // img & text
        $data['about_data_th'] = Content::where('ref_name', 'about_data_th')->where('page', 'about')->first();
        $data['about_data_eng'] = Content::where('ref_name', 'about_data_eng')->where('page', 'about')->first();
        // what we do
        $data['what_we_do_th'] = Content::where('ref_name', 'what_we_do_th')->where('page', 'about')->first();
        $data['what_we_do_eng'] = Content::where('ref_name', 'what_we_do_eng')->where('page', 'about')->first();
        $data['what_we_do_img_1'] = Content::where('ref_name', 'what_we_do_img_1')->where('page', 'about')->first();
        $data['what_we_do_img_2'] = Content::where('ref_name', 'what_we_do_img_2')->where('page', 'about')->first();
        $data['what_we_do_img_3'] = Content::where('ref_name', 'what_we_do_img_3')->where('page', 'about')->first();

        return view('frontend.about', $data);
    }
}
