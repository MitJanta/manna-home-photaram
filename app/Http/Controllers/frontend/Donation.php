<?php

namespace App\Http\Controllers\frontend;

use App\Bank;
use App\Content;
use App\DonationSubmit;

use App\Helper\ConstantHelper;
use App\Helper\DateHelper;
use App\Helper\MailLibrary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Donation extends Controller
{
    private $donate_attache_path = ConstantHelper::DONATE_ATTACHE_PATH;

    public function index()
    {
        $data['page_th'] = 'บริจาค';
        $data['page_en'] = 'donation';

        $data['banner_data'] = Content::where('ref_name', 'donation_banner')->where('page', 'donation')->first();
        $data['how_you_can_help'] = Content::where('ref_name', 'how_you_can_help')->where('page', 'donation')->first();
        $data['acc_data_th'] = Content::where('ref_name', 'acc_data_th')->where('page', 'donation')->first();
        $data['acc_data_en'] = Content::where('ref_name', 'acc_data_en')->where('page', 'donation')->first();

        $data['banks_data'] = Bank::where('enable', 1)->orderBy('created_at', 'asc')->get();

        return view('frontend.donation', $data);
    }

    public function donationSubmit(Request $request)
    {
        $paid_by = $request->paid_by;
        if ($paid_by == 1) {
            $create_array = array(
              'first_name' => $request->fname_input,
              'last_name' => $request->lname_input,
              'email' => $request->email_input,
              'donate_for' => $request->purpose_input,
              'money_amount' => $request->amount_input,
              'paid_by' => 0,
            );
        } elseif ($paid_by == 2) {
            $datetime = DateHelper::slashToDateFormat($request->date_input).' '.$request->time_input.':00';
            $file = $request->file('attache_file');
            $extension = $file->extension();
            $file_name = "donate_". date('dmyHis') . "." . $extension;
            $file->move($this->donate_attache_path, $file_name);

            $create_array = array(
              'first_name' => $request->fname_input,
              'last_name' => $request->lname_input,
              'email' => $request->email_input,
              'donate_for' => $request->purpose_input,
              'money_amount' => $request->amount_input,
              'paid_by' => 1,
              'bank_id' => $request->bank_name,
              'bank_other' => $request->bank_name_input,
              'bank_branch' => $request->branch_input,
              'date' => $datetime,
              'attache_file' => $file_name,
            );
        }

        if (DonationSubmit::create($create_array) && MailLibrary::sendDonationMail($create_array)) {
            return back()->with(
                array(
                        'alert'   => 'popup',
                        'type'    => 'success',
                        'message' => 'ส่งข้อมูลสำเร็จ'
                    )
            );
        } else {
            return back()->with(
                array(
                          'alert'   => 'popup',
                          'type'    => 'error',
                          'message' => 'ส่งเมลไม่สำเร็จ'
                      )
            );
        }
    }
}
