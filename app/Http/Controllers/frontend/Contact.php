<?php

namespace App\Http\Controllers\frontend;

use App\Content;
use App\ContactSubmit;
use App\Helper\MailLibrary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Contact extends Controller
{
    public function index()
    {
        $data['page_th'] = 'ติดต่อเรา';
        $data['page_en'] = 'contact';

        $data['banner_data'] = Content::where('ref_name', 'contact_banner')->where('page', 'contact')->first();
        $data['contact_email'] = Content::where('ref_name', 'contact_email')->where('page', 'contact')->first();
        $data['contact_address'] = Content::where('ref_name', 'contact_address')->where('page', 'contact')->first();
        $data['contact_tel_1'] = Content::where('ref_name', 'contact_tel_1')->where('page', 'contact')->first();
        $data['contact_tel_2'] = Content::where('ref_name', 'contact_tel_2')->where('page', 'contact')->first();

        $data['contact_google_map'] = Content::where('ref_name', 'contact_google_map')->where('page', 'contact')->first();
        $data['contact_massage_img'] = Content::where('ref_name', 'contact_massage_img')->where('page', 'contact')->first();

        return view('frontend.contact', $data);
    }

    public function contactSubmit(Request $request)
    {
        $create_array = array(
          'first_name' => $request->fname_input,
          'last_name' => $request->lname_input,
          'telephone' => $request->tel_input,
          'email' => $request->email_input,
          'message' => $request->message_input,
        );
        ContactSubmit::create($create_array);

        if (MailLibrary::sendContactMail($create_array)) {
            return back()->with(
                array(
                        'alert'   => 'popup',
                        'type'    => 'success',
                        'message' => 'ส่งข้อมูลสำเร็จ'
                    )
            );
        } else {
            return back()->with(
                array(
                        'alert'   => 'popup',
                        'type'    => 'error',
                        'message' => 'ส่งเมลไม่สำเร็จ'
                    )
            );
        }
    }
}
