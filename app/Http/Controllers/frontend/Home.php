<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\News;
use App\Content;

class Home extends Controller
{
    public function index()
    {
        $data['page_th'] = 'หน้าหลัก';
        $data['page_en'] = 'home';

        // banner & text overlay banner
        $data['banner_data'] = Content::where('ref_name', 'home_banner')->where('page', 'home')->first();
        // about us text
        $data['about_data'] = Content::where('ref_name', 'about_data')->where('page', 'home')->first();
        // help us text
        $data['help_us_data_eng'] = Content::where('ref_name', 'help_us_acc_eng')->where('page', 'home')->first();
        $data['help_us_data_th'] = Content::where('ref_name', 'help_us_acc_th')->where('page', 'home')->first();
        // image divider
        $data['image_divider'] = Content::where('ref_name', 'image_divider')->where('page', 'home')->first();

        $data['news_list'] = News::where('enable', 1)->orderBy('created_at', 'desc')->limit(4)->get();

        return view('frontend.home', $data);
    }
}
