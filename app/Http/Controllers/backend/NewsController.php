<?php

namespace App\Http\Controllers\backend;

use Auth;
use File;

use App\News;
use App\Content;

use App\Helper\AdminLibrary;
use App\Helper\GeneralLibrary;
use App\Helper\ConstantHelper;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class NewsController extends Controller
{
    private $image_cover_news_path = ConstantHelper::NEWS_COVER_PATH;
    private $image_content_path = ConstantHelper::CONTENT_IMAGE_PATH;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission3()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function newsBannerEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'Banner หน้าข่าว';
        $data['page_en'] = 'edit_news_banner_form';

        // banner img
        $data['banner_data'] = Content::where('ref_name', 'news_banner')->where('page', 'news')->first();

        return view('backend.pages.news.banner', $data);
    }

    public function newsBannerEdit(Request $request)
    {
        $this->isPermission();

        // banner img
        $banner_data = Content::where('ref_name', 'news_banner')->where('page', 'news')->first();

        $banner_image = $request->upload_input_banner;
        $banner_base64 = $request->banner_data;
        if (empty($banner_image)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                 'type' => 'info',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($banner_image)) {
                $old_image = $banner_data->image_name;
                $image_name = "news_banner_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $banner_base64);
                $banner_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                               'type' => 'error',
                               'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป Banner ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $banner_data->save();
        }

        // logging => database
        $log_msg = array(
          'menu'   => 'News',
          'page'   => 'Edit News Banner',
          'action' => 'แก้ไข Banner หน้าข่าว'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                 'type' => 'success',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function newsManage(int $page_num = 0)
    {
        $this->isPermission();

        $per_page = 10;
        $data['page_num'] = $page_num;

        $data['page_th'] = 'ข่าวสาร';
        $data['page_en'] = 'news_manage';
        $news_list = News::orderBy('created_at', 'desc');
        // dd($news_list->get());

        $num_rows = $news_list->count();
        // Paginate news_list
        $news_list = $news_list->paginate($per_page, ['*'], 'page', $page_num);
        $new_newsArray = array();
        foreach ($news_list as $row) {
            // $row['type'] = $newsTypeString;
            array_push($new_newsArray, $row);
        }
        $data['news_list'] = $new_newsArray;
        $data['max_page'] = ceil($num_rows / $per_page);

        if ($page_num > $data['max_page'] && $data['max_page'] != 0) {
            return redirect()->route('backend.news_manage', $data['max_page']);
        }

        return view('backend.pages.news.news_manage', $data);
    }

    public function addNewsForm(int $page_num)
    {
        $this->isPermission();

        $data['page_th'] = 'เพิ่มข่าวสาร';
        $data['page_en'] = 'add_news_form';
        $data['page_num'] = $page_num;

        return view('backend.pages.news.news_add', $data);
    }

    private function validator_add_news(Request $request)
    {
        //validation rules.
        $rules = [ 'th_news_name' => 'required|string',
                   // 'en_news_name' => 'nullable|string',
                   'news_detail_th' => 'nullable|string',
                   // 'news_detail_en' => 'nullable|string',
               ];

        //custom validation error messages.
        $messages = [ 'th_news_name.required' => 'กรุณากรอกชื่อข่าว (ภาษาไทย)',
                      // 'en_news_name.required' => 'กรุณากรอกชื่อข่าว (English)',
                      // 'news_detail_th.required' => 'กรุณากรอกเนื้อหาข่าว (ภาษาไทย)',
                      'news_image.required' => 'กรุณาเลือกไฟล์',
                    ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    public function addNews(Request $request)
    {
        $this->isPermission();

        $this->validator_add_news($request);

        $news_name = $request->input('th_news_name');
        $news_author_name = $request->input('author_name');
        $news_detail = $request->input('news_detail_th');
        $news_image = $request->input('news_lg_hidden_th');
        $page_num = $request->input('page_num');

        $date_now = GeneralLibrary::generateTimestamp();
        $image_name = "news_cover_". $date_now . ".jpg";
        $path = $this->image_cover_news_path.$image_name;
        GeneralLibrary::upload_image($path, $news_image);

        $gallery_path = $this->image_cover_news_path.$date_now;
        if (!File::exists($gallery_path)) { // ถ้ายังไม่มีให้สร้าง folder ก่อน (ซึ่งก็ควรจะไม่มี)
            File::makeDirectory($gallery_path, 0755, true, true);
        }

        $news = News::create([
          'title'       => $news_name,
          'author'      => $news_author_name,
          'content'     => $news_detail,
          'cover'       => $image_name,
          'gallery'     => $date_now,
        ]);

        // logging => database
        $log_msg = array(
            'menu'   => 'News',
            'page'   => 'Add news',
            'id'     => $news->id,
            'action' => 'เพิ่มข่าวใหม่'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->route('backend.news_manage', 1)->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-newspaper"> </i>&nbsp&nbsp เพิ่มข่าวสำเร็จ')
        );
    }

    public function newsEditForm(int $page_num, int $news_id)
    {
        $this->isPermission();

        $data['page_th'] = 'แก้ไขเนื้อหาข่าว';
        $data['page_en'] = 'edit_news_form';

        $data['news'] = News::where('id', '=', $news_id)->first();
        $data['page_num'] = $page_num;

        return view('backend.pages.news.news_edit', $data);
    }

    private function validator_edit_news(Request $request)
    {
        //validation rules.
        $rules = [  'th_news_name' => 'required|string',
                    'author_name' => 'required|string',
                    'news_detail_th' => 'nullable|string',
                    'news_image' => 'nullable',
    ];

        //custom validation error messages.
        $messages = [ 'th_news_name.required' => 'กรุณากรอกชื่อข่าว',
        'author_name.required' => 'กรุณากรอกชื่อผู้เขียนข่าว',
    ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    public function newsEdit(Request $request)
    {
        $this->isPermission();

        $this->validator_edit_news($request);

        $news_id = $request->input('id');
        $news_name_th = $request->input('th_news_name');
        $news_author = $request->input('author_name');
        $news_detail_th = $request->input('news_detail_th');
        $news_image = $request->input('news_lg_hidden_th');

        // Get old news data
        $old_news = News::find($news_id);

        if ($old_news->title != $news_name_th || $old_news->author != $news_author || $old_news->content != $news_detail_th || $request->hasFile('news_image')) {
            if ($old_news->title != $news_name_th) {
                $old_news->title = $news_name_th;
            }

            if ($old_news->author != $news_author) {
                $old_news->author = $news_author;
            }

            if ($old_news->content != $news_detail_th) {
                $old_news->content = $news_detail_th;
            }

            if ($request->hasFile('news_image')) {
                $old_image = $old_news->cover;
                $image_name = "news_cover_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_cover_news_path.$image_name;
                GeneralLibrary::upload_image($path, $news_image);
                $old_news->cover = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_cover_news_path.$old_image;
                    if (File::exists($image_path)) {
                        // File::delete($image_path);
                        if (File::delete($image_path)) {
                            // dd('pass');
                        } else {
                            // dd('not pass');
                        }
                        // unlink(config('app.url').$image_path);
                    }
                }
            }

            $old_news->save(); // Update new data to table

            // logging => database
            $log_msg = array(
            'menu'   => 'News',
            'page'   => 'Edit news',
            'id'     => $old_news->id,
            'action' => 'แก้ไขข่าว'
          );
            $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
            AdminLibrary::logging($log_msg);

            return Redirect::back()->with(
                array('alert' => 'popup',
                       'type' => 'success',
                       'message' => '<i class="fas fa-newspaper"> </i>&nbsp&nbsp แก้ไขข่าวสำเร็จ')
            );
        }

        return Redirect::back()->with(
            array('alert' => 'popup',
              'type' => 'info',
              'message' => '&nbsp&nbsp ไม่มีการแก้ไขข้อมูลใหม่')
        );
    }

    public function showNews(int $page_num, int $id)
    {
        $this->isPermission();

        $news = News::find($id);
        $news->enable = 1;
        $news->save();

        // Add log to database
        $log_msg = array(
            'menu'    => 'News',
            'page'    => 'News Manage',
            'id'      => $id,
            'action'  => 'เปิดการซ่อนข่าว'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return Redirect::back()->with(
            array('alert' => 'toast',
          'type' => 'info',
          'message' => '&nbsp&nbsp เปิดข้อมูลข่าว')
        );
    }

    public function hideNews(int $page_num, int $id)
    {
        $this->isPermission();

        $news = News::find($id);
        $news->enable = 0;
        $news->save();

        // Add log to database
        $log_msg = array(
            'menu'    => 'News',
            'page'    => 'News Manage',
            'id'      => $id,
            'action'  => 'ซ่อนข่าว'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return Redirect::back()->with(
            array('alert' => 'toast',
          'type' => 'info',
          'message' => '&nbsp&nbsp ซ่อนข้อมูลข่าว')
        );
    }

    public function removeNews(int $page_num, int $id)
    {
        $this->isPermission();

        $news = News::find($id);
        $image_name = $news->cover;

        // remove image news
        $image_path = $this->image_cover_news_path.$image_name;  // Value is not URL but directory file path
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        // Loop remove slide images
        $filesInFolder = false;
        $folderPath = $this->image_cover_news_path.$news->gallery;
        if (File::exists($folderPath)) {
            $filesInFolder = File::files($folderPath);
        }
        if (false !== $filesInFolder) {
            foreach ($filesInFolder as $file) {
                if ('.' !=  $file && '..' != $file) {
                    if (!empty($file)) {
                        $filename = $file;
                        if (unlink($filename)) {
                            // return true;
                        } else {
                            return redirect()->route('backend.news_manage', $page_num)->with(
                                array('alert' => 'popup',
                                    'type' => 'error',
                                    'message' => '<i class="fas fa-trash-alt"></i>&nbsp ลบรูปสไลด์ข่าวไม่สำเร็จ')
                            );
                        }
                    }
                }
            }
        }

        if (File::exists($folderPath)) {
            File::deleteDirectory($folderPath);
        }

        // Remove news data from DB
        $news->delete();

        // Add log
        $log_msg = array(
            'menu'    => 'News',
            'page'    => 'News edit',
            'id'      => $id,
            'action'  => 'ลบข่าว'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->route('backend.news_manage', $page_num)->with(
            array('alert' => 'popup',
                  'type' => 'success',
                  'message' => '<i class="fas fa-trash-alt"></i>&nbsp ลบข่าวแล้ว')
        );
    }

    public function uploadNewsImageDropzone(Request $request)
    {
        $folder = $request->gallery;
        $folderPath = $this->image_cover_news_path.$folder;
        // return 'select_id = '.$request->select_id.'///'.$folderPath;
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $time_stamp = DateHelper::dateTimeInMicro();
            $file_name = 'news_image_'.$time_stamp.'.'.$image->extension();
            // $image = $request->file('file');

            $path = $folderPath;
            if (!File::exists($path)) { // ถ้า Folder ยังไม่มีให้สร่างก่อน
                File::makeDirectory($path, 0755, true, true);
            }

            // นับจำนวนไฟล์
            $files = File::files($path);

            if ($files !== false) {
                $filecount = count($files);
            } else {
                $filecount = 0;
            }
            // return response()->json([ 'count' => $filecount ]);
            // ถ้ามากกว่า 5 ไฟล์จะไม่อัปโหลด
            if ($filecount < 20) {
                $image->move($path, $file_name);
                return response()->json([ 'success' => $file_name ]);
            } else {
                return response()->json([ 'error' => 'ไฟล์มี 20 ไฟล์แล้ว' ]);
            }
        } else { // กรณีเอาไว้ Refrash ข้อมูลรายการผลิตภัณฑ์
            return response()->json([ 'error' => 'xxxxxxxxxxxx' ]);
        }
    }

    public function setImageListPreview(Request $request)
    {
        $folder_name = $request->gallery;
        $folderPath = $this->image_cover_news_path.$folder_name;
        $filesInFolder = false;
        if (File::exists($folderPath)) {
            $filesInFolder = File::files($folderPath);
        }

        $output = '<div id="grid2" class="row" align="center">';
        $product_amount = 0;
        if (false !== $filesInFolder) {
            foreach ($filesInFolder as $file) {
                if ('.' !=  $file && '..' != $file) {
                    $output .= '<div class="col-lg-3 col-md-4 col-sm-6" style="margin-top: 3px;">
                    <div class="file">
                       <a class="image"  href="'.config('app.url').$file.'" target="_blank" style="cursor: zoom-in">
                          <img  class="img-thumbnail" src="'.config('app.url').$file.'"
                          alt="Not Found" title="'.config('app.url').$file.'"/>
                        </a>
                        <a href="#n" id="'.$file.'" gallery_name="'.$folder_name.'" class="remove_image" title="ลบรูปภาพ">
                            <i class="fas fa-trash-alt delete-product-btn text-danger"></i>
                          </a>
                        </div>
                      </div>';
                    $product_amount++;
                }
            }
        } else {
            $output .= '<p class="text-center w-100" align="center">ยังไม่มีรูปภาพ</p>';
        }
        $output .= '</div>';

        $amount = '<div align="center">
          <h5>ภาพสไลด์ ('.$product_amount.'/20 รูป)</br></h5>
        </div>';
        return $amount.$output;
    }

    public function removeNewsGalleryImage(Request $request)
    {
        $name = $request->name;
        if (!empty($name)) {
            $filename = $name;
            if (unlink($filename)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
