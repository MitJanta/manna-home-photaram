<?php

namespace App\Http\Controllers\backend;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function isPermission()
    {
        if (!Auth::guard('admin')->user()) {
            return Redirect::to('@backend/dashboard')->send()
            ->with(
                array(
              'alert' => 'toast',
              'type' => 'warning',
              'message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง')
            );
        }
    }

    public function index()
    {
        $this->isPermission();

        $data['page_th'] = 'แผงควบคุม';
        $data['page_en'] = 'dashboard';
        $data['admin_name'] = Auth::guard('admin')->user()->name;

        return view('backend.pages.dashboard', $data);
    }
}
