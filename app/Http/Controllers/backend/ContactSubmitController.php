<?php

namespace App\Http\Controllers\backend;

use Auth;

use App\ContactSubmit;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactSubmitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission4()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function contactList()
    {
        $this->isPermission();

        // $per_page = 10;
        // $data['page_num'] = $page_num;

        $data['page_th'] = 'ข้อความการติดต่อ';
        $data['page_en'] = 'contact_list';
        $contact_list = ContactSubmit::orderBy('created_at', 'desc')->get();
        // dd($news_list->get());

        // $num_rows = $news_list->count();
        // // Paginate news_list
        // $news_list = $news_list->paginate($per_page, ['*'], 'page', $page_num);
        $new_contactArray = array();
        foreach ($contact_list as $row) {
            $row['created_datetime'] = DateHelper::dateToThaiFormat($row['created_at']);
            array_push($new_contactArray, $row);
        }
        $data['contact_list'] = $new_contactArray;
        // $data['max_page'] = ceil($num_rows / $per_page);
        //
        // if ($page_num > $data['max_page'] && $data['max_page'] != 0) {
        //     return redirect()->route('backend.news_manage', $data['max_page']);
        // }

        return view('backend.pages.contact_submit.contact_list', $data);
    }
}
