<?php

namespace App\Http\Controllers\backend;

use Auth;
use File;

use App\Content;

use App\Helper\AdminLibrary;
use App\Helper\GeneralLibrary;
use App\Helper\ConstantHelper;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class HomeController extends Controller
{
    private $image_content_path = ConstantHelper::CONTENT_IMAGE_PATH;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission2()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function bannerEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'Banner หน้าหลัก';
        $data['page_en'] = 'edit_home_banner_form';

        // banner & text overlay banner
        $data['banner_data'] = Content::where('ref_name', 'home_banner')->where('page', 'home')->first();

        return view('backend.pages.home.banner', $data);
    }

    private function validator_banner(Request $request)
    {
        //validation rules.
        $rules = [ 'upload_input_home_banner' => 'nullable|string',
        'banner_title' => 'nullable|string',
        'banner_text' => 'nullable|string',
               ];

        //custom validation error messages.
        $messages = [];

        //validate the request.
        $request->validate($rules, $messages);
    }

    public function bannerEdit(Request $request)
    {
        $this->isPermission();

        // banner & text overlay banner
        $banner_data = Content::where('ref_name', 'home_banner')->where('page', 'home')->first();

        $banner_image = $request->upload_input_home_banner;
        $banner_base64 = $request->home_banner_data;
        $banner_title = $request->banner_title;
        $banner_text = $request->banner_text;
        if (empty($banner_image) && $banner_title == $banner_data->title_en && $banner_text == $banner_data->content_en) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($banner_image)) {
                $old_image = $banner_data->image_name;
                $image_name = "home_banner_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $banner_base64);
                $banner_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป Banner ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if ($banner_data->title_en != $banner_title) {
                $banner_data->title_en = $banner_title;
            }

            if ($banner_data->content_en != $banner_text) {
                $banner_data->content_en = $banner_text;
            }

            $banner_data->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'Home',
            'page'   => 'Edit Home Banner',
            'action' => 'แก้ไข Banner หน้า Home'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function aboutEditForm(Request $request)
    {
        $this->isPermission();

        $data['page_th'] = 'About us หน้าหลัก';
        $data['page_en'] = 'edit_home_about_form';

        // about us text
        $data['about_data'] = Content::where('ref_name', 'about_data')->where('page', 'home')->first();

        return view('backend.pages.home.about', $data);
    }

    public function aboutEdit(Request $request)
    {
        $this->isPermission();

        // about & text overlay about
        $about_data = Content::where('ref_name', 'about_data')->where('page', 'home')->first();

        $about_image_1 = $request->upload_input_about_us_1;
        $about_base64_1 = $request->about_us_1_data;
        $about_text_th = $request->about_us_th_text;
        $about_text_en = $request->about_us_en_text;
        $about_image_2 = $request->upload_input_about_us_2;
        $about_base64_2 = $request->about_us_2_data;
        if (empty($about_image_1)
        && $about_text_th == $about_data->content_th
        && $about_text_en == $about_data->content_en
        && empty($about_image_2)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($about_image_1)) {
                $old_image = $about_data->image_name;
                $image_name = "home_about_1_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $about_base64_1);
                $about_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป About 1 ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if ($about_data->content_th != $about_text_th) {
                $about_data->content_th = $about_text_th;
            }

            if ($about_data->content_en != $about_text_en) {
                $about_data->content_en = $about_text_en;
            }

            if (!empty($about_image_2)) {
                $old_image = $about_data->link_url;
                $image_name = "home_about_2_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $about_base64_2);
                $about_data->link_url = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป About 2 ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $about_data->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'Home',
            'page'   => 'Edit Home about',
            'action' => 'แก้ไข about หน้า Home'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function helpEditForm(Request $request)
    {
        $this->isPermission();

        $data['page_th'] = 'Help us หน้าหลัก';
        $data['page_en'] = 'edit_home_help_form';

        // help us text
        $data['help_us_data_eng'] = Content::where('ref_name', 'help_us_acc_eng')->where('page', 'home')->first();
        $data['help_us_data_th'] = Content::where('ref_name', 'help_us_acc_th')->where('page', 'home')->first();

        return view('backend.pages.home.help', $data);
    }

    public function helpEdit(Request $request)
    {
        $this->isPermission();

        // help us text
        $help_us_data_eng = Content::where('ref_name', 'help_us_acc_eng')->where('page', 'home')->first();
        $help_us_data_th = Content::where('ref_name', 'help_us_acc_th')->where('page', 'home')->first();

        $help_image = $request->upload_input_help_us;
        $help_base64 = $request->help_us_data;
        $help_acc_num_en = $request->acc_num_en_text;
        $help_bank_name_en = $request->bank_name_en_text;
        $help_acc_name_en = $request->acc_name_en_text;
        $help_acc_num_th = $request->acc_num_th_text;
        $help_bank_name_th = $request->bank_name_th_text;
        $help_acc_name_th = $request->acc_name_th_text;

        // ถ้าไม่มีการเปลี่ยนแปลงข้อมูล
        if (empty($help_image)
        && $help_acc_num_en == $help_us_data_eng->title_th
        && $help_bank_name_en == $help_us_data_eng->title_en
        && $help_acc_name_en == $help_us_data_eng->content_th
        && $help_acc_num_th == $help_us_data_th->title_th
        && $help_bank_name_th == $help_us_data_th->title_en
        && $help_acc_name_th == $help_us_data_th->content_th) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($help_image)) {
                $old_image = $help_us_data_eng->image_name;
                $image_name = "home_help_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $help_base64);
                $help_us_data_eng->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป Help us ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if ($help_us_data_eng->title_th != $help_acc_num_en) {
                $help_us_data_eng->title_th = $help_acc_num_en;
            }

            if ($help_us_data_eng->title_en != $help_bank_name_en) {
                $help_us_data_eng->title_en = $help_bank_name_en;
            }

            if ($help_us_data_eng->content_th != $help_acc_name_en) {
                $help_us_data_eng->content_th = $help_acc_name_en;
            }

            if ($help_us_data_th->title_th != $help_acc_num_th) {
                $help_us_data_th->title_th = $help_acc_num_th;
            }

            if ($help_us_data_th->title_en != $help_bank_name_th) {
                $help_us_data_th->title_en = $help_bank_name_th;
            }

            if ($help_us_data_th->content_th != $help_acc_name_th) {
                $help_us_data_th->content_th = $help_acc_name_th;
            }

            $help_us_data_eng->save();
            $help_us_data_th->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'Home',
            'page'   => 'Edit Home Help us',
            'action' => 'แก้ไข Help us หน้า Home'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function dividerEditForm(Request $request)
    {
        $this->isPermission();

        $data['page_th'] = 'ภาพเส้นแบ่งส่วน หน้าหลัก';
        $data['page_en'] = 'edit_home_divider_form';

        // image divider
        $data['image_divider'] = Content::where('ref_name', 'image_divider')->where('page', 'home')->first();

        return view('backend.pages.home.divider', $data);
    }

    public function dividerEdit(Request $request)
    {
        $this->isPermission();

        // image divider
        $image_divider = Content::where('ref_name', 'image_divider')->where('page', 'home')->first();

        $divider_image = $request->upload_input_divider;
        $divider_base64 = $request->divider_data;

        // ถ้าไม่มีการเปลี่ยนแปลงข้อมูล
        if (empty($divider_image)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($divider_image)) {
                $old_image = $image_divider->image_name;
                $image_name = "home_divider_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $divider_base64);
                $image_divider->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปภาพเส้นแบ่งส่วน ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $image_divider->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'Home',
            'page'   => 'Edit Home divider',
            'action' => 'แก้ไขภาพเส้นแบ่งส่วน หน้า Home'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }
}
