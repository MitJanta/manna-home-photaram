<?php

namespace App\Http\Controllers\backend;

use Auth;
use File;

use App\Content;

use App\Helper\AdminLibrary;
use App\Helper\GeneralLibrary;
use App\Helper\ConstantHelper;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class DonationController extends Controller
{
    private $image_content_path = ConstantHelper::CONTENT_IMAGE_PATH;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission2()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function donationBannerEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'Banner หน้า Donation';
        $data['page_en'] = 'edit_donation_banner_form';

        // banner img
        $data['banner_data'] = Content::where('ref_name', 'donation_banner')->where('page', 'donation')->first();

        return view('backend.pages.donation.banner', $data);
    }

    public function donationBannerEdit(Request $request)
    {
        $this->isPermission();

        // banner img
        $banner_data = Content::where('ref_name', 'donation_banner')->where('page', 'donation')->first();

        $banner_image = $request->upload_input_banner;
        $banner_base64 = $request->banner_data;
        if (empty($banner_image)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                 'type' => 'info',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($banner_image)) {
                $old_image = $banner_data->image_name;
                $image_name = "donation_banner_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $banner_base64);
                $banner_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                               'type' => 'error',
                               'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป Banner ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $banner_data->save();
        }

        // logging => database
        $log_msg = array(
          'menu'   => 'Donation',
          'page'   => 'Edit Donation Banner',
          'action' => 'แก้ไข Banner หน้า Donation'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                 'type' => 'success',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function donationHowYouCanHelpEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'ส่วน How you can help';
        $data['page_en'] = 'edit_donation_how_you_can_help_form';

        $data['donation'] = Content::where('ref_name', 'how_you_can_help')->where('page', 'donation')->first();
        $data['acc_data_th'] = Content::where('ref_name', 'acc_data_th')->where('page', 'donation')->first();
        $data['acc_data_en'] = Content::where('ref_name', 'acc_data_en')->where('page', 'donation')->first();

        return view('backend.pages.donation.donation', $data);
    }

    public function donationHowYouCanHelpEdit(Request $request)
    {
        $this->isPermission();

        $donation = Content::where('ref_name', 'how_you_can_help')->where('page', 'donation')->first();
        $acc_data_th = Content::where('ref_name', 'acc_data_th')->where('page', 'donation')->first();
        $acc_data_en = Content::where('ref_name', 'acc_data_en')->where('page', 'donation')->first();

        $th_text_input = $request->th_text_text;
        $en_text_input = $request->en_text_text;
        $acc_num_th_text_input = $request->acc_num_th_text;
        $acc_name_th_text_input = $request->acc_name_th_text;
        $acc_th_text_input = $request->acc_th_text;
        $acc_num_en_text_input = $request->acc_num_en_text;
        $acc_name_en_text_input = $request->acc_name_en_text;
        $acc_en_text_input = $request->acc_en_text;
        $image_1 = $request->upload_input_image_1;
        $base64_1 = $request->image_data_1;
        $image_2 = $request->upload_input_image_2;
        $base64_2 = $request->image_data_2;
        $image_3 = $request->upload_input_image_3;
        $base64_3 = $request->image_data_3;
        $image_4 = $request->upload_input_image_4;
        $base64_4 = $request->image_data_4;

        if ($th_text_input == $donation->content_th
        && $en_text_input == $donation->content_en
        && $acc_num_th_text_input == $acc_data_th->title_th
        && $acc_name_th_text_input == $acc_data_th->title_en
        && $acc_th_text_input == $acc_data_th->content_th
        && $acc_num_en_text_input == $acc_data_en->title_th
        && $acc_name_en_text_input == $acc_data_en->title_en
        && $acc_en_text_input == $acc_data_en->content_th
        && empty($image_1) && empty($image_2) && empty($image_3) && empty($image_4)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if ($donation->content_th != $th_text_input) {
                $donation->content_th = $th_text_input;
            }

            if ($donation->content_en != $en_text_input) {
                $donation->content_en = $en_text_input;
            }

            if ($acc_data_th->title_th != $acc_num_th_text_input) {
                $acc_data_th->title_th = $acc_num_th_text_input;
            }

            if ($acc_data_th->title_en != $acc_name_th_text_input) {
                $acc_data_th->title_en = $acc_name_th_text_input;
            }

            if ($acc_data_th->content_th != $acc_th_text_input) {
                $acc_data_th->content_th = $acc_th_text_input;
            }

            if ($acc_data_en->title_th != $acc_num_en_text_input) {
                $acc_data_en->title_th = $acc_num_en_text_input;
            }

            if ($acc_data_en->title_en != $acc_name_en_text_input) {
                $acc_data_en->title_en = $acc_name_en_text_input;
            }

            if ($acc_data_en->content_th != $acc_en_text_input) {
                $acc_data_en->content_th != $acc_en_text_input;
            }

            if (!empty($image_1)) {
                $old_image = $acc_data_th->image_name;
                $image_name = "donation_acc_1_desktop_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_1);
                $acc_data_th->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if (!empty($image_2)) {
                $old_image = $acc_data_th->image_name;
                $image_name = "donation_acc_1_mobile_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_2);
                $acc_data_th->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if (!empty($image_3)) {
                $old_image = $acc_data_en->image_name;
                $image_name = "donation_acc_2_desktop_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_3);
                $acc_data_en->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if (!empty($image_4)) {
                $old_image = $acc_data_en->image_name;
                $image_name = "donation_acc_2_mobile_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_4);
                $acc_data_en->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $donation->save();
            $acc_data_th->save();
            $acc_data_en->save();
        }

        // logging => database
        $log_msg = array(
          'menu'   => 'Donation',
          'page'   => 'Edit Donation How you can help',
          'action' => 'แก้ไขข้อมูลหน้า Donation ส่วน How you can help'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                 'type' => 'success',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }
}
