<?php

namespace App\Http\Controllers\backend;

use Auth;
use File;

use App\Content;

use App\Helper\AdminLibrary;
use App\Helper\GeneralLibrary;
use App\Helper\ConstantHelper;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class ContactController extends Controller
{
    private $image_content_path = ConstantHelper::CONTENT_IMAGE_PATH;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission2()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function contactBannerEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'Banner หน้า Contact';
        $data['page_en'] = 'edit_contact_banner_form';

        // banner img
        $data['banner_data'] = Content::where('ref_name', 'contact_banner')->where('page', 'contact')->first();

        return view('backend.pages.contact.banner', $data);
    }

    public function contactBannerEdit(Request $request)
    {
        $this->isPermission();

        // banner img
        $banner_data = Content::where('ref_name', 'contact_banner')->where('page', 'contact')->first();

        $banner_image = $request->upload_input_banner;
        $banner_base64 = $request->banner_data;
        if (empty($banner_image)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                 'type' => 'info',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($banner_image)) {
                $old_image = $banner_data->image_name;
                $image_name = "contact_banner_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $banner_base64);
                $banner_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                               'type' => 'error',
                               'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป Banner ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $banner_data->save();
        }

        // logging => database
        $log_msg = array(
          'menu'   => 'Contact',
          'page'   => 'Edit Contact Banner',
          'action' => 'แก้ไข Banner หน้า Contact us'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                 'type' => 'success',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function contactDataEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'แก้ไขข้อมูลหน้า Contact us';
        $data['page_en'] = 'edit_contact_form';

        $data['contact_email'] = Content::where('ref_name', 'contact_email')->where('page', 'contact')->first();
        $data['contact_address'] = Content::where('ref_name', 'contact_address')->where('page', 'contact')->first();
        $data['contact_tel_1'] = Content::where('ref_name', 'contact_tel_1')->where('page', 'contact')->first();
        $data['contact_tel_2'] = Content::where('ref_name', 'contact_tel_2')->where('page', 'contact')->first();

        $data['contact_google_map'] = Content::where('ref_name', 'contact_google_map')->where('page', 'contact')->first();
        $data['contact_massage_img'] = Content::where('ref_name', 'contact_massage_img')->where('page', 'contact')->first();

        return view('backend.pages.contact.contact', $data);
    }

    public function contactDataEdit(Request $request)
    {
        $this->isPermission();

        $contact_email = Content::where('ref_name', 'contact_email')->where('page', 'contact')->first();
        $contact_address = Content::where('ref_name', 'contact_address')->where('page', 'contact')->first();
        $contact_tel_1 = Content::where('ref_name', 'contact_tel_1')->where('page', 'contact')->first();
        $contact_tel_2 = Content::where('ref_name', 'contact_tel_2')->where('page', 'contact')->first();

        $contact_google_map = Content::where('ref_name', 'contact_google_map')->where('page', 'contact')->first();
        $contact_massage_img = Content::where('ref_name', 'contact_massage_img')->where('page', 'contact')->first();

        $contact_email_input = $request->contact_email_text;
        $contact_address_input = $request->contact_address_text;
        $contact_tel_1_input = $request->contact_tel_1_text;
        $contact_tel_2_input = $request->contact_tel_2_text;
        $contact_map_input = $request->contact_map_text;
        $map_ex_1 = explode('width="', $contact_map_input);
        $map_ex_2 = explode('"', $map_ex_1[1], 2);
        $contact_map_input = $map_ex_1[0].'width="100%"'.$map_ex_2[1];
        $image_1 = $request->upload_input_image;
        $base64_1 = $request->image_data;

        if ($contact_email_input == $contact_email->content_th
        && $contact_address_input == $contact_address->content_th
        && $contact_tel_1_input == $contact_tel_1->content_th
        && $contact_tel_2_input == $contact_tel_2->content_th
        && $contact_map_input == $contact_google_map->content_th
        && empty($image_1)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if ($contact_email->content_th != $contact_email_input) {
                $contact_email->content_th = $contact_email_input;
            }

            if ($contact_address->content_th != $contact_address_input) {
                $contact_address->content_th = $contact_address_input;
            }

            if ($contact_tel_1->content_th != $contact_tel_1_input) {
                $contact_tel_1->content_th = $contact_tel_1_input;
            }

            if ($contact_tel_2->content_th != $contact_tel_2_input) {
                $contact_tel_2->content_th = $contact_tel_2_input;
            }

            if ($contact_google_map->content_th != $contact_map_input) {
                $contact_google_map->content_th = $contact_map_input;
            }

            if (!empty($image_1)) {
                $old_image = $contact_massage_img->image_name;
                $image_name = "contact_img_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_1);
                $contact_massage_img->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $contact_email->save();
            $contact_address->save();
            $contact_tel_1->save();
            $contact_tel_2->save();
            $contact_google_map->save();
            $contact_massage_img->save();
        }

        // logging => database
        $log_msg = array(
          'menu'   => 'Contact',
          'page'   => 'Edit Contact',
          'action' => 'แก้ไขข้อมูลหน้า Contact us'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                 'type' => 'success',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function footerEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'แก้ไขส่วน Footer';
        $data['page_en'] = 'edit_footer_form';

        $data['footer_data'] = Content::where('ref_name', 'footer_data')->where('page', 'global')->first();

        return view('backend.pages.contact.footer', $data);
    }

    public function footerEdit(Request $request)
    {
        $this->isPermission();

        $footer_data = Content::where('ref_name', 'footer_data')->where('page', 'global')->first();

        $acc_num_input = $request->acc_num_text;
        $facebook_link_input = $request->facebook_link_text;
        $image_1 = $request->upload_input_image;
        $base64_1 = $request->image_data;

        if ($acc_num_input == $footer_data->title_th
        && $facebook_link_input == $footer_data->title_en
        && empty($image_1)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if ($footer_data->title_th != $acc_num_input) {
                $footer_data->title_th = $acc_num_input;
            }

            if ($footer_data->title_en != $facebook_link_input) {
                $footer_data->title_en = $facebook_link_input;
            }

            if (!empty($image_1)) {
                $old_image = $footer_data->image_name;
                $image_name = "footer_bank_logo_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_1);
                $footer_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $footer_data->save();
        }

        // logging => database
        $log_msg = array(
          'menu'   => 'Contact',
          'page'   => 'Edit Footer',
          'action' => 'แก้ไขข้อมูล Footer'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                 'type' => 'success',
                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }
}
