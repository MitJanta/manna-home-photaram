<?php

namespace App\Http\Controllers\BackEnd;

use Auth;
use DateTime;
use File;
use App\Upload;
use App\Http\Controllers\Controller;
use App\Helper\AdminLibrary;
use App\Helper\ConstantHelper;
use App\Helper\DateHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class FileUploadController extends Controller
{
    private $file_upload_path = ConstantHelper::UPLOAD_PATH;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission2() || !Auth::guard('admin')->user()->isPermission3()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function index()
    {
        $this->isPermission();

        $data['page_th'] = 'อัปโหลดไฟล์';
        $data['page_en'] = 'files_upload';
        $files_list = Upload::get();

        $date_new_format = array();
        foreach ($files_list as $fileObject) {
            $array_file = array(
                            'description' => $fileObject->description,
                            'file_name' => $fileObject->file_name,
                            'id' => $fileObject->id,
                            'created_at' => $fileObject->created_at,
                            'new_format_datetime' => DateHelper::dateToThaiFormat($fileObject->created_at),
                            'enable' => $fileObject->enable,
                        );
            array_push($date_new_format, $array_file);
        }

        $data['files_new_list'] = $date_new_format;
        $data['count'] = Upload::count();
        return view('backend.pages.file_upload', $data);
    }

    public function fileUpload(Request $request)
    {
        $this->isPermission();

        $file_desc = $request->input('description');
        $file = $request->file('file_upload');
        $extension = $file->extension();
        $file_name = "file_". date('dmyHis') . "." . $extension;
        $file->move($this->file_upload_path, $file_name);
        $file_upload = Upload::create([
                    'description' => $file_desc,
                    'file_name' => $file_name,
                  ]);

        // logging => database
        $log_msg = array(
                    'menu' => 'Upload File',
                    'page' => 'Upload file manage',
                    'id' => $file_upload->id,
                    'action' => 'ผู้ดูแลอัปโหลดไฟล์'
                );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->withInput()->with(
            array('alert' => 'popup',
                          'type' => 'success',
                          'message' => '&nbsp&nbsp อัปโหลดไฟล์สำเร็จ')
        );
    }

    public function downloadFile(Int $file_id)
    {
        $file_data = Upload::find($file_id);
        list($file_name, $file_extension) = explode('.', $file_data->file_name);
        $file = "files_upload/files/".$file_data->file_name;
        // return $file;
        $headers = array('Content-Type: application/'.$file_extension);

        // logging => database
        $log_msg = array(
                  'menu' => 'Upload File',
                  'page' => 'Upload file manage',
                  'id' => $file_id,
                  'action' => 'ผู้ดูแลดาวน์โหลดไฟล์'
              );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return response()->download($file, $file_data->file_name, $headers);
    }

    public function removeFile(int $id)
    {
        $this->isPermission();

        $file = Upload::find($id);
        // remove image news
        $file_path = $this->file_upload_path.$file->file_name;  // Value is not URL but directory file path
        if (File::exists($file_path)) {
            File::delete($file_path);
        }
        $file->delete();

        // Add log
        $log_msg = array(
                    'menu' => 'Upload File',
                    'page' => 'Upload file manage',
                    'id' => $id,
                    'action' => 'Removed a file'
                );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return Redirect::back()->with(
            array('alert' => 'popup',
          'type' => 'error',
          'message' => '<i class="fas fa-trash-alt"></i>&nbsp ลบไฟล์แล้ว')
        );
    }
}
