<?php

namespace App\Http\Controllers\backend;

use Auth;
use File;

use App\Content;

use App\Helper\AdminLibrary;
use App\Helper\GeneralLibrary;
use App\Helper\ConstantHelper;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;

class AboutController extends Controller
{
    private $image_content_path = ConstantHelper::CONTENT_IMAGE_PATH;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission2()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function aboutBannerEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'Banner หน้า About us';
        $data['page_en'] = 'edit_about_banner_form';

        // banner img
        $data['banner_data'] = Content::where('ref_name', 'about_banner')->where('page', 'about')->first();

        return view('backend.pages.about.banner', $data);
    }

    public function aboutBannerEdit(Request $request)
    {
        $this->isPermission();

        // banner img
        $banner_data = Content::where('ref_name', 'about_banner')->where('page', 'about')->first();

        $banner_image = $request->upload_input_banner;
        $banner_base64 = $request->banner_data;
        if (empty($banner_image)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($banner_image)) {
                $old_image = $banner_data->image_name;
                $image_name = "about_banner_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $banner_base64);
                $banner_data->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูป Banner ไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $banner_data->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'About',
            'page'   => 'Edit About Banner',
            'action' => 'แก้ไข Banner หน้า About us'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function aboutWhoWeAreEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'แก้ไขส่วน Who we are หน้า About us';
        $data['page_en'] = 'edit_about_who_we_are_form';

        // img & text
        $data['about_data_th'] = Content::where('ref_name', 'about_data_th')->where('page', 'about')->first();
        $data['about_data_eng'] = Content::where('ref_name', 'about_data_eng')->where('page', 'about')->first();

        return view('backend.pages.about.who_we_are', $data);
    }

    public function aboutWhoWeAreEdit(Request $request)
    {
        $this->isPermission();

        // img & text
        $about_data_th = Content::where('ref_name', 'about_data_th')->where('page', 'about')->first();
        $what_we_do_eng = Content::where('ref_name', 'about_data_eng')->where('page', 'about')->first();

        $image_1 = $request->upload_input_about_us_1;
        $base64_1 = $request->about_us_1_data;
        $text_th = $request->about_us_th_text;
        $image_2 = $request->upload_input_about_us_2;
        $base64_2 = $request->about_us_2_data;
        $text_en = $request->about_us_en_text;
        if (empty($image_1) && $text_th == $about_data_th->content_th && empty($image_2) && $text_en == $what_we_do_eng->content_en) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if (!empty($image_1)) {
                $old_image = $about_data_th->image_name;
                $image_name = "about_img_1_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_1);
                $about_data_th->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if ($about_data_th->content_th != $text_th) {
                $about_data_th->content_th = $text_th;
            }

            if (!empty($image_2)) {
                $old_image = $what_we_do_eng->image_name;
                $image_name = "about_img_2_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_2);
                $what_we_do_eng->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if ($what_we_do_eng->content_en != $text_en) {
                $what_we_do_eng->content_en = $text_en;
            }

            $about_data_th->save();
            $what_we_do_eng->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'About',
            'page'   => 'Edit About Who we are',
            'action' => 'แก้ไข Who we are หน้า About us'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }

    public function aboutWhatWeDoEditForm()
    {
        $this->isPermission();

        $data['page_th'] = 'แก้ไขส่วน What we do หน้า About us';
        $data['page_en'] = 'edit_about_what_we_do_form';

        // what we do
        $data['what_we_do_th'] = Content::where('ref_name', 'what_we_do_th')->where('page', 'about')->first();
        $data['what_we_do_eng'] = Content::where('ref_name', 'what_we_do_eng')->where('page', 'about')->first();
        $data['what_we_do_img_1'] = Content::where('ref_name', 'what_we_do_img_1')->where('page', 'about')->first();
        $data['what_we_do_img_2'] = Content::where('ref_name', 'what_we_do_img_2')->where('page', 'about')->first();
        $data['what_we_do_img_3'] = Content::where('ref_name', 'what_we_do_img_3')->where('page', 'about')->first();

        return view('backend.pages.about.what_we_do', $data);
    }

    public function aboutWhatWeDoEdit(Request $request)
    {
        $this->isPermission();

        // what we do
        $what_we_do_th = Content::where('ref_name', 'what_we_do_th')->where('page', 'about')->first();
        $what_we_do_eng = Content::where('ref_name', 'what_we_do_eng')->where('page', 'about')->first();
        $what_we_do_img_1 = Content::where('ref_name', 'what_we_do_img_1')->where('page', 'about')->first();
        $what_we_do_img_2 = Content::where('ref_name', 'what_we_do_img_2')->where('page', 'about')->first();
        $what_we_do_img_3 = Content::where('ref_name', 'what_we_do_img_3')->where('page', 'about')->first();

        $text_th = $request->about_us_th_text;
        $text_en = $request->about_us_en_text;
        $image_1 = $request->upload_input_about_us_1;
        $base64_1 = $request->about_us_1_data;
        $image_2 = $request->upload_input_about_us_2;
        $base64_2 = $request->about_us_2_data;
        $image_3 = $request->upload_input_about_us_3;
        $base64_3 = $request->about_us_3_data;
        if ($text_th == $what_we_do_th->content_th && $text_en == $what_we_do_eng->content_en && empty($image_1) && empty($image_2) && empty($image_3)) {
            return redirect()->back()->with(
                array('alert' => 'popup',
                   'type' => 'info',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ไม่มีการเปลี่ยนแปลงข้อมูล')
            );
        } else {
            if ($what_we_do_th->content_th != $text_th) {
                $what_we_do_th->content_th = $text_th;
            }

            if ($what_we_do_eng->content_en != $text_en) {
                $what_we_do_eng->content_en = $text_en;
            }

            if (!empty($image_1)) {
                $old_image = $what_we_do_img_1->image_name;
                $image_name = "about_what_we_do_1_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_1);
                $what_we_do_img_1->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if (!empty($image_2)) {
                $old_image = $what_we_do_img_2->image_name;
                $image_name = "about_what_we_do_2_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_2);
                $what_we_do_img_2->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            if (!empty($image_3)) {
                $old_image = $what_we_do_img_3->image_name;
                $image_name = "about_what_we_do_3_". GeneralLibrary::generateTimestamp() . ".jpg";
                $path = $this->image_content_path.$image_name;
                GeneralLibrary::upload_image($path, $base64_3);
                $what_we_do_img_3->image_name = $image_name;
                if (!empty($old_image)) {
                    $image_path = $this->image_content_path.$old_image;
                    if (File::exists($image_path)) {
                        if (File::delete($image_path)) {
                        } else {
                            return redirect()->back()->with(
                                array('alert' => 'popup',
                                 'type' => 'error',
                                 'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp ลบรูปไม่สำเร็จ')
                            );
                        }
                    }
                }
            }

            $what_we_do_th->save();
            $what_we_do_eng->save();
            $what_we_do_img_1->save();
            $what_we_do_img_2->save();
            $what_we_do_img_3->save();
        }

        // logging => database
        $log_msg = array(
            'menu'   => 'About',
            'page'   => 'Edit About What we do',
            'action' => 'แก้ไข What we do หน้า About us'
          );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(
            array('alert' => 'popup',
                   'type' => 'success',
                   'message' => '<i class="fas fa-home"> </i>&nbsp&nbsp แก้ไขข้อมูลสำเร็จ')
        );
    }
}
