<?php

namespace App\Http\Controllers\backend;

use Auth;

use App\DonationSubmit;
use App\Bank;
use App\Helper\DateHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DonationSubmitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission4()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'error','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function donationList()
    {
        $this->isPermission();

        // $per_page = 10;
        // $data['page_num'] = $page_num;

        $data['page_th'] = 'ข้อมูลการบริจาค';
        $data['page_en'] = 'donation_list';
        $donation_list = DonationSubmit::orderBy('created_at', 'desc')->get();
        // dd($news_list->get());

        // $num_rows = $news_list->count();
        // // Paginate news_list
        // $news_list = $news_list->paginate($per_page, ['*'], 'page', $page_num);
        $new_donationArray = array();
        foreach ($donation_list as $row) {
            $row['created_datetime'] = DateHelper::dateToThaiFormat($row['created_at']);
            if ($row['paid_by'] == 1 && !empty($row['bank_id'])) {
                $bank_data = Bank::where('code', $row['bank_id'])->first();
                // dd($bank_data);
                if (!empty($bank_data)) {
                    $row['bank_name'] = $bank_data->name.' '.$bank_data->code;
                }
            }
            array_push($new_donationArray, $row);
        }
        $data['donation_list'] = $new_donationArray;
        // $data['max_page'] = ceil($num_rows / $per_page);
        //
        // if ($page_num > $data['max_page'] && $data['max_page'] != 0) {
        //     return redirect()->route('backend.news_manage', $data['max_page']);
        // }

        return view('backend.pages.donation_submit.donation_list', $data);
    }

    public function donationDetail(int $id = 1)
    {
        $this->isPermission();

        $data['page_th'] = 'รายละเอียดการบริจาค';
        $data['page_en'] = 'donation_detail';
        $data['donation_detail'] = DonationSubmit::where('id', $id)->first();
        $data['created_datetime'] = DateHelper::dateToThaiFormat($data['donation_detail']->created_at);
        if ($data['donation_detail']->paid_by == 1 && !empty($data['donation_detail']->bank_id)) {
            $bank_data = Bank::where('code', $data['donation_detail']->bank_id)->first();
            // dd($bank_data);
            if (!empty($bank_data)) {
                $data['bank_name'] = $bank_data->name.' '.$bank_data->code;
            }
        }

        $data['donation_detail']->mark_as_read = 1;
        $data['donation_detail']->save();

        return view('backend.pages.donation_submit.donation_detail', $data);
    }

    public function donationSubmitEmail(int $id = 1)
    {
        $this->isPermission();

        $data['donation_detail'] = DonationSubmit::where('id', $id)->first();
        $data['created_datetime'] = DateHelper::dateToThaiFormat($data['donation_detail']->created_at);
        if ($data['donation_detail']->paid_by == 1 && !empty($data['donation_detail']->bank_id)) {
            $bank_data = Bank::where('code', $data['donation_detail']->bank_id)->first();
            // dd($bank_data);
            if (!empty($bank_data)) {
                $data['bank_name'] = $bank_data->name.' '.$bank_data->code;
            }
        }

        return 0;
    }
}
