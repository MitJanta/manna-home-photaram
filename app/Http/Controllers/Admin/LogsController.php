<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Admin;
use App\AdminLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission1()) {
            return Redirect::to('@backend/dashboard')->send()->with(array('alert' => 'toast','type' => 'warning','message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง'));
        }
    }

    public function index(Int $admin_id = 1)
    {
        $this->isPermission();

        $data['page_th'] = 'บันทึกการใช้งาน';
        $data['page_en'] = 'logging';
        $data['admin_name'] = Admin::where('id', '=', $admin_id)->first();
        $data['logs_list'] = AdminLog::where('admin_id', '=', $admin_id)->orderBy('created_at', 'DESC')->get();
        $data['count'] = AdminLog::where('admin_id', '=', $admin_id)->count();
        return view('backend.pages.admin.logging', $data);
    }
}
