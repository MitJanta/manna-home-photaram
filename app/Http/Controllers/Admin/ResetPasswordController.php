<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Admin;
use App\Helper\AdminLibrary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    // Global variables
    public $user;
    public $admin;

    public function __construct()
    {
        // ...
    }

    public function isPermission()
    {
        $this->admin = Auth::guard('admin')->user();
    }

    public function showResetPasswordForm()
    {
        $this->isPermission();
        $data['page_th'] = 'ตั้งรหัสผ่านใหม่';
        $data['page_en'] = 'reset_password';
        $data['admin_name'] = $this->admin->name;
        $data['updated_at'] = $this->admin->updated_at;
        return view('backend.pages.admin.reset_password', $data);
    }

    protected function update(Request $request)
    {
        $this->isPermission();
        $this->validator($request);

        // return Auth::guard('admin')->user()->password;

        if (!Hash::check($request->admin_old_password, $this->admin->password)) {
            return redirect()->back()->withInput()->with('password', 'รหัสผ่านเดิมไม่ถูกต้อง!');
        }
        $admin_update = $this->admin;
        $admin_update->password = Hash::make($request->admin_password);
        $admin_update->save();

        // logging => database
        $log_msg = array(
          'menu' => 'Admin',
          'page' => 'ChangePassword',
          'id' => $this->admin->id,
          'action' => 'เปลี่ยนรหัสผ่นใหม่'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->route('backend.dashboard')->with(
            array('alert' => 'popup',
          'type' => 'success',
          'message' => '<i class="fas fa-key"> </i>&nbsp&nbsp เปลี่ยนรหัสผ่านใหม่สำเร็จ')
        );
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [ 'admin_old_password' => 'required|string|min:6|max:255',
        'admin_password' => 'required|string|min:6|max:255',
        'confirm_password' => 'required|string|min:6|max:255|same:admin_password',
        ];

        //custom validation error messages.
        $messages = [ 'admin_old_password.required' => 'กรุณากรอกรหัสผ่านเก่า',
                      'admin_password.required' => 'กรุณากรอกรหัสผ่านใหม่',
                      'confirm_password.required' => 'กรุณากรอกยืนยันรหัสผ่าน',
                      'admin_old_password.min' => 'รหัสผ่านต้องมีอย่างน้อย 6 ตัว',
                      'confirm_password.same' => ':confirm_password ไม่ตรงกับ :admin_password',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }
}
