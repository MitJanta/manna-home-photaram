<?php

namespace App\Http\Controllers\Admin\Auth;

use Auth;
use App\Admin;
use App\Helper\AdminLibrary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        $data['page_th'] = 'ลงชื่อเข้าสู่ระบบ';
        $data['page_en'] = 'login';
        return view('backend.auth.login', [
          'title' => 'Admin Login',
          'loginRoute' => 'admin.login',
          'forgotPasswordRoute' => 'admin.password.request',
      ], $data);
    }

    public function login(Request $request)
    {
        $this->validator($request);

        if (Auth::guard('admin')->attempt($request->only('email', 'password'), $request->filled('remember'))) {
            //Authentication passed...
            // Admin::where('id','=',Auth::guard('admin')->user()->id)->first();

            // logging => database
            $log_msg = array(
                'menu' => 'Admin authentication',
                'page_th' => 'Login',
                'id' => Auth::guard('admin')->user()->id,
                'action' => 'เข้าสู่ระบบ'
            );
            $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
            AdminLibrary::logging($log_msg);

            return redirect()->intended(route('backend.dashboard'))->with(array('alert' => 'toast','type' => 'success','message' => '&nbsp&nbsp&nbsp&nbsp ยินดีต้อนรับเข้าสู่ระบบ'));
        }

        //Authentication failed...
        return $this->loginFailed();
    }

    public function logout()
    {
        // logging => database
        $log_msg = array(
          'menu' => 'Admin authentication',
          'page' => '-',
          'id' => Auth::guard('admin')->user()->id,
          'action' => 'ออกจากระบบ'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        Auth::guard('admin')->logout();

        return redirect()->route('auth.login')->with('status', 'ออกจากระบบแล้ว!');
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [ 'email'    => 'required|email|exists:admins|min:5|max:191',
                   'password' => 'required|string|min:6|max:255',
        ];

        //custom validation error messages.
        $messages = [ 'email.exists' => 'ไม่มีผู้ดูแลเว็บใช้อีเมลนี้.',
                      'required' => 'กรุณากรอกข้อมูลด้วย',
                      'password.min' => 'รหัสผ่านต้องมีอย่างน้อย 6 ตัว',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function loginFailed()
    {
        return redirect()->back()->withInput()->with('error', 'รหัสไม่ถูกต้อง!');
    }
}
