<?php

namespace App\Http\Controllers\Admin;

use App;
use Auth;
use App\Admin;
use App\Helper\AdminLibrary;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ManageAdminsController extends Controller
{
    // Global variables
    public $user;
    public $admin;

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isPermission()
    {
        if (!Auth::guard('admin')->user()->isPermission1()) {
            return Redirect::to('@backend/dashboard')->send()
                   ->with(
                       array('alert' => 'toast',
                             'type' => 'warning',
                             'message' => '&nbsp&nbsp คุณไม่ได้รับสิทธ์ให้เข้าถึง')
                   );
        }
    }

    public function index()
    {
        $this->isPermission();

        $data['page_th'] = 'จัดการผู้ดูแลเว็บ';
        $data['page_en'] = 'admin_manage';
        $data['admin_list'] = Admin::orderBy('updated_at', 'desc')->get();

        $data['admins_num'] = Admin::count();
        return view('backend.pages.admin.admin_manage', $data);
    }

    public function createForm()
    {
        $this->isPermission();

        $data['page_th'] = 'เพิ่มผู้ดูแลเว็บ';
        $data['page_en'] = 'add_admin_form';
        return view('backend.pages.admin.admin_create', $data);
    }

    public function create(Request $request)
    {
        $this->isPermission();

        $this->validator_create($request);

        if ($request->input('admin_chk1') == 'on') {
            $pm_1 = 1;
        } else {
            $pm_1 = 0;
        }
        if ($request->input('admin_chk2') == 'on') {
            $pm_2 = 1;
        } else {
            $pm_2 = 0;
        }
        if ($request->input('admin_chk3') == 'on') {
            $pm_3 = 1;
        } else {
            $pm_3 = 0;
        }
        if ($request->input('admin_chk4') == 'on') {
            $pm_4 = 1;
        } else {
            $pm_4 = 0;
        }

        // Check if no select any permission (Must select less 1 permission)
        if (!($pm_1 || $pm_2 || $pm_3 || $pm_4)) {
            return redirect()->back()->withInput()->with(array('permission' => 'กรุณาเลือกสิทธิ์อย่างน้อย 1 สิทธิ์'));
        }

        // Create Admin
        $admin_create = Admin::create([
          'name' => $request->input('name'),
          'email' => $request->input('email'),
          'password' => Hash::make($request->input('password')),
          'type' => 0,
          'permission_1' => $pm_1,
          'permission_2' => $pm_2,
          'permission_3' => $pm_3,
          'permission_4' => $pm_4,
      ]);

        // logging => database
        $log_msg = array(
            'menu' => 'Admins manage',
            'page' => 'เพิ่มผู้ดูแลเว็บ',
            'id' => $admin_create->id,
            'action' => 'เพิ่มผู้ดูแลเว็บไซต์สำเร็จ'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->route('admin.admin_manage')->with(
            array('alert' => 'popup',
                'type' => 'success',
                'message' => '<i class="fas fa-user-plus"> </i>&nbsp&nbsp เพิ่มผู้ดูแลเว็บไซต์สำเร็จ')
        );
    }

    public function editForm(int $id)
    {
        $this->isPermission();

        $data['page_th'] = 'แก้ไขข้อมูลผู้ดูแลเว็บไซต์';
        $data['page_en'] = 'edit_admin_form';
        $data['admin_detail'] = Admin::find($id);
        return view('backend.pages.admin.admin_edit', $data);
    }

    public function edit(Request $request)
    {
        $this->isPermission();

        $this->validator_edit($request);

        if ($request->input('admin_chk1') == 'on') {
            $pm_1 = 1;
        } else {
            $pm_1 = 0;
        }
        if ($request->input('admin_chk2') == 'on') {
            $pm_2 = 1;
        } else {
            $pm_2 = 0;
        }
        if ($request->input('admin_chk3') == 'on') {
            $pm_3 = 1;
        } else {
            $pm_3 = 0;
        }
        if ($request->input('admin_chk4') == 'on') {
            $pm_4 = 1;
        } else {
            $pm_4 = 0;
        }

        // Check if no select any permission (Must select less 1 permission)
        if (!($pm_1 || $pm_2 || $pm_3 || $pm_4)) {
            return redirect()->back()->withInput()->with(array('permission' => 'กรุณาเลือกสิทธิ์อย่างน้อย 1 สิทธิ์'));
        }

        // update data
        $admin = Admin::find($request->input('id'));
        // Check if not change
        if (!($admin->name != $request->input('admin_name') || $admin->permission_1 != $pm_1 || $admin->permission_2 != $pm_2 || $admin->permission_3 != $pm_3 || $admin->permission_4 != $pm_4)) {
            // return redirect()->route('admin.admin_manage')->with(array('alert' => 'popup','type' => 'info','message' => '<i class="fas fa-user-edit"> </i> ไม่มีการแก้ไขข้อมูลผู้ดูแลเว็บไซต์'));
            return redirect()->route('admin.admin_manage');
        }
        $admin->name = $request->input('admin_name');
        $admin->permission_1 = $pm_1;
        $admin->permission_2 = $pm_2;
        $admin->permission_3 = $pm_3;
        $admin->permission_4 = $pm_4;
        $admin->save();

        // logging => database
        $log_msg = array(
            'menu' => 'Admins manage',
            'page' => 'Edit admin detail',
            'id' => $admin->id,
            'action' => 'แก้ไขข้อมูลผู้ดูแล'
        );
        $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        AdminLibrary::logging($log_msg);

        return redirect()->back()->with(array('alert' => 'popup','type' => 'success','message' => '<i class="fas fa-user-edit"> </i>&nbsp&nbsp แก้ไขข้อมูลผู้ดูแลเว็บไซต์สำเร็จ'));
    }

    private function validator_create(Request $request)
    {
        //validation rules.
        $rules = [ 'name' => 'required|string|unique:admins|min:3|max:199',
        'email' => 'required|email|unique:admins',
        'password' => 'required|string|min:6|max:20',
        'confirm_password' => 'required|string|min:6|max:20',
        ];

        //custom validation error messages.
        $messages = [ 'name.required' => 'กรุณากรอกชื่อผู้ดูแลเว็บไซต์',
                      'name.unique' => 'ชื่อนี้ถูกใช้แล้ว',
                      'name.min' => 'ชื่อผู้ดูแลเว็บไซต์ต้องมีอย่างน้อย 6 ตัว',
                      'name.max' => 'ชื่อผู้ดูแลเว็บไซต์ต้องไม่เกิน 199 ตัว',
                      'email.required' => 'กรุณากรอกอีเมล',
                      'email' => 'กรุณากรอกอีเมลที่ถูกต้อง',
                      'email.unique' => 'บัญชีอีเมลนี้ถูกใช้แล้ว',
                      'password.required' => 'กรุณากรอกรหัสผ่าน',
                      'password.min' => 'รหัสผ่านต้องมีอย่างน้อย 6 ตัว',
                      'password.max' => 'รหัสผ่านต้องไม่เกิน 20 ตัว',
                      'confirm_password.required' => 'กรุณากรอกยืนยันรหัสผ่าน',
                      'confirm_password.min' => 'ยืนยันรหัสผ่านต้องมีอย่างน้อย 6 ตัว',
                      'confirm_password.max' => 'ยืนยันรหัสผ่านต้องไม่เกิน 20 ตัว',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function validator_edit(Request $request)
    {
        //validation rules.
        $rules = [ 'admin_name' => 'required|string|min:3|max:199',
        ];

        //custom validation error messages.
        $messages = [ 'admin_name.required' => 'กรุณากรอกชื่อผู้ดูแลเว็บไซต์',
                      'admin_name.min' => 'ชื่อผู้ดูแลเว็บไซต์ต้องมีอย่างน้อย 6 ตัว',
                      'admin_name.max' => 'ชื่อผู้ดูแลเว็บไซต์ต้องไม่เกิน 199 ตัว',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }
}
