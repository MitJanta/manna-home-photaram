<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthAdmins
{
    public function handle($request, Closure $next)
    {
        // if (false == Auth::guard('admin')->check()) {
        //     if ($request->ajax()) {
        //         return response('Unauthorized.', 401);
        //     } else {
        //         return redirect()->route('auth.login');
        //     }

        if (false == Auth::guard('admin')->check()) {
            return redirect()->route('auth.login');
        }
        return $next($request);
        // }
    }
}
