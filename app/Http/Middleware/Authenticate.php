<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Route;

class Authenticate extends Middleware
{
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            if (Route::is('auth.*') || Route::is('backend.*')) {
                return route('auth.login');
            }
            return route('home');
        }
    }
}
