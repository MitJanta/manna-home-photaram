<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthUsers
{
    public function handle($request, Closure $next)
    {
        if (false == Auth::check()) {
            return redirect()->route('auth.login');
            //redirect User to login page
        }

        return $next($request);
    }
}
