<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents';

    protected $primaryKey = 'id';

    protected $fillable = [
      'title_th',
      'title_en',
      'content_th',
      'content_en',
      'image_name',
      'link_url',
    ];

    protected $hidden = [
      'created_at',
    ];
}
