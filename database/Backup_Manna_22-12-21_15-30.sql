-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2021 at 08:36 AM
-- Server version: 5.7.33
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mannah_website`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_1` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'สิทธิ์สร้าง SubAdmin ใหม่',
  `permission_2` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'สิทธิ์จัดการข้อมูลทั่วไป',
  `permission_3` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'สิทธิ์จัดการข่าวและกิจกรรม',
  `permission_4` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'สิทธิ์เข้าถึงข้อความติดต่อและบริจาค',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = SuperAdmin, 0 = SubAdmin (default)',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `permission_1`, `permission_2`, `permission_3`, `permission_4`, `type`, `created_at`, `updated_at`) VALUES
(1, 'NNB Dev', 'art@nanobey.com', '$2y$10$p1YezeQ4sN8V5yGvmujx1u3X9hz.INCCwtIqXsNnKQWFCOUZjB2KK', NULL, 1, 1, 1, 1, 1, '2021-12-13 15:41:24', '2021-12-13 15:42:47'),
(2, 'Mit test', 'godlovemit777@gmail.com', '$2y$10$zIIl20gnaSUXW.8v9qNehex7zyQJ8e/2V3YYqbgq4kmiqeU1SirXu', NULL, 1, 1, 1, 1, 0, '2021-12-14 07:30:18', '2021-12-17 01:49:26'),
(3, 'ถุงแป้ง test', 'tungpaeng@nanobey.com', '$2y$10$IQyP1P1HDQ0/t08Sj9mGEOuUfwJIfKp.TBOH03RYMMsrP2j.KMUAS', NULL, 0, 1, 0, 0, 0, '2021-12-17 11:23:12', '2021-12-20 07:14:28');

-- --------------------------------------------------------

--
-- Table structure for table `admins_logs`
--

CREATE TABLE IF NOT EXISTS `admins_logs` (
  `id` bigint(20) unsigned NOT NULL,
  `admin_id` bigint(20) NOT NULL,
  `log_ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins_logs`
--

INSERT INTO `admins_logs` (`id`, `admin_id`, `log_ip`, `log_url`, `log_method`, `log_desc`, `log_agent`, `created_at`, `updated_at`) VALUES
(1, 1, '127.0.0.1', 'http://localhost:8000/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 01:10:11', '2021-12-14 01:10:11'),
(2, 1, '127.0.0.1', 'http://localhost:8000/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 04:13:03', '2021-12-14 04:13:03'),
(3, 1, '127.0.0.1', 'http://localhost:8000/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 05:46:46', '2021-12-14 05:46:46'),
(4, 1, '184.82.204.129', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 06:31:44', '2021-12-14 06:31:44'),
(5, 1, '184.82.204.129', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-14 06:58:34', '2021-12-14 06:58:34'),
(6, 1, '184.82.204.129', 'http://mannahomephotharam.com/@backend/admin_manage/create', 'POST', '{"menu":"Admins manage","page":"\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1c\\u0e39\\u0e49\\u0e14\\u0e39\\u0e41\\u0e25\\u0e40\\u0e27\\u0e47\\u0e1a","id":1,"action":"\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e1c\\u0e39\\u0e49\\u0e14\\u0e39\\u0e41\\u0e25\\u0e40\\u0e27\\u0e47\\u0e1a\\u0e44\\u0e0b\\u0e15\\u0e4c\\u0e2a\\u0e33\\u0e40\\u0e23\\u0e47\\u0e08"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 07:30:18', '2021-12-14 07:30:18'),
(7, 1, '184.82.204.129', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":1,"action":"Logout successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 07:30:26', '2021-12-14 07:30:26'),
(8, 2, '184.82.204.129', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":2,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', '2021-12-14 07:30:32', '2021-12-14 07:30:32'),
(9, 1, '184.82.204.129', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-15 06:27:41', '2021-12-15 06:27:41'),
(10, 1, '110.169.146.13', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-16 02:50:36', '2021-12-16 02:50:36'),
(11, 1, '110.169.146.13', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":1,"action":"Logout successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-16 02:55:45', '2021-12-16 02:55:45'),
(12, 1, '184.82.197.61', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-16 06:08:00', '2021-12-16 06:08:00'),
(13, 1, '184.82.197.61', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":1,"action":"Logout successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-16 07:48:17', '2021-12-16 07:48:17'),
(14, 1, '110.169.146.13', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-16 18:12:57', '2021-12-16 18:12:57'),
(15, 1, '110.169.146.13', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"Login successfull"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:05:49', '2021-12-17 01:05:49'),
(16, 10, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/add', 'POST', '{"menu":"News","page":"Add news","id":10,"action":"\\u0e40\\u0e1e\\u0e34\\u0e48\\u0e21\\u0e02\\u0e48\\u0e32\\u0e27\\u0e43\\u0e2b\\u0e21\\u0e48"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:28:16', '2021-12-17 01:28:16'),
(17, 10, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/1/hide/10', 'GET', '{"menu":"News","page":"News Manage","id":10,"action":"\\u0e0b\\u0e48\\u0e2d\\u0e19\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:42:52', '2021-12-17 01:42:52'),
(18, 9, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":9,"action":"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e02\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:43:16', '2021-12-17 01:43:16'),
(19, 7, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":7,"action":"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e02\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:46:35', '2021-12-17 01:46:35'),
(20, 8, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":8,"action":"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e02\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:47:00', '2021-12-17 01:47:00'),
(21, 6, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":6,"action":"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e02\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:47:26', '2021-12-17 01:47:26'),
(22, 6, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":6,"action":"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e02\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:47:53', '2021-12-17 01:47:53'),
(23, 1, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":1,"action":"\\u0e41\\u0e01\\u0e49\\u0e44\\u0e02\\u0e02\\u0e48\\u0e32\\u0e27"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:48:29', '2021-12-17 01:48:29'),
(24, 1, '110.169.146.13', 'http://mannahomephotharam.com/@backend/admin_manage/edit', 'POST', '{"menu":"Admins manage","page":"Edit admin detail","id":1,"action":"Edited an admin detail"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 01:49:26', '2021-12-17 01:49:26'),
(25, 10, '110.169.146.13', 'http://mannahomephotharam.com/@backend/news_manage/1/show/10', 'GET', '{"menu":"News","page":"News Manage","id":10,"action":"เปิดการซ่อนข่าว"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 03:03:02', '2021-12-17 03:03:02'),
(26, 1, '49.228.16.226', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:28:38', '2021-12-17 10:28:38'),
(27, 1, '49.228.16.226', 'http://mannahomephotharam.com/@backend/file_upload/upload', 'POST', '{"menu":"Upload File","page":"Upload file manage","id":1,"action":"Edited brochure file"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:31:18', '2021-12-17 10:31:18'),
(28, 1, '49.228.16.226', 'http://mannahomephotharam.com/@backend/file_upload/download/1', 'GET', '{"menu":"Upload File","page":"Upload file manage","id":1,"action":"Admin download a file"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:31:50', '2021-12-17 10:31:50'),
(29, 1, '49.228.16.226', 'http://mannahomephotharam.com/@backend/file_upload/download/1', 'GET', '{"menu":"Upload File","page":"Upload file manage","id":1,"action":"ผู้ดูแลดาวน์โหลดไฟล์"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:35:28', '2021-12-17 10:35:28'),
(30, 1, '49.228.16.226', 'http://mannahomephotharam.com/@backend/file_upload/remove/1', 'GET', '{"menu":"Upload File","page":"Upload file manage","id":1,"action":"Removed a file"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:35:39', '2021-12-17 10:35:39'),
(31, 10, '49.228.16.226', 'http://mannahomephotharam.com/@backend/news_manage/1/hide/10', 'GET', '{"menu":"News","page":"News Manage","id":10,"action":"ซ่อนข่าว"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:39:31', '2021-12-17 10:39:31'),
(32, 2, '49.228.16.226', 'http://mannahomephotharam.com/@backend/file_upload/upload', 'POST', '{"menu":"Upload File","page":"Upload file manage","id":2,"action":"ผู้ดูแลอัปโหลดไฟล์"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:43:47', '2021-12-17 10:43:47'),
(33, 10, '49.228.16.226', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":10,"action":"แก้ไขข่าว"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:44:42', '2021-12-17 10:44:42'),
(34, 10, '49.228.16.226', 'http://mannahomephotharam.com/@backend/news_manage/1/show/10', 'GET', '{"menu":"News","page":"News Manage","id":10,"action":"เปิดการซ่อนข่าว"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-17 10:44:58', '2021-12-17 10:44:58'),
(35, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:21:03', '2021-12-17 11:21:03'),
(36, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/admin_manage/create', 'POST', '{"menu":"Admins manage","page":"เพิ่มผู้ดูแลเว็บ","id":1,"action":"เพิ่มผู้ดูแลเว็บไซต์สำเร็จ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:23:12', '2021-12-17 11:23:12'),
(37, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/admin_manage/edit', 'POST', '{"menu":"Admins manage","page":"Edit admin detail","id":1,"action":"แก้ไขข้อมูลผู้ดูแล"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:23:54', '2021-12-17 11:23:54'),
(38, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":1,"action":"ออกจากระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:24:07', '2021-12-17 11:24:07'),
(39, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:25:07', '2021-12-17 11:25:07'),
(40, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":1,"action":"ออกจากระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:26:00', '2021-12-17 11:26:00'),
(41, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:27:02', '2021-12-17 11:27:02'),
(42, 11, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/add', 'POST', '{"menu":"News","page":"Add news","id":11,"action":"เพิ่มข่าวใหม่"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:30:00', '2021-12-17 11:30:00'),
(43, 11, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/1/hide/11', 'GET', '{"menu":"News","page":"News Manage","id":11,"action":"ซ่อนข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:30:40', '2021-12-17 11:30:40'),
(44, 10, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/1/hide/10', 'GET', '{"menu":"News","page":"News Manage","id":10,"action":"ซ่อนข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:30:42', '2021-12-17 11:30:42'),
(45, 11, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":11,"action":"แก้ไขข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:31:12', '2021-12-17 11:31:12'),
(46, 11, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/1/show/11', 'GET', '{"menu":"News","page":"News Manage","id":11,"action":"เปิดการซ่อนข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:33:00', '2021-12-17 11:33:00'),
(47, 11, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/1/hide/11', 'GET', '{"menu":"News","page":"News Manage","id":11,"action":"ซ่อนข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:33:27', '2021-12-17 11:33:27'),
(48, 11, '49.228.101.119', 'http://mannahomephotharam.com/@backend/news_manage/1/remove/11', 'GET', '{"menu":"News","page":"News edit","id":11,"action":"ลบข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-17 11:33:44', '2021-12-17 11:33:44'),
(49, 3, '110.169.145.138', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":3,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-20 07:14:11', '2021-12-20 07:14:11'),
(50, 3, '110.169.145.138', 'http://mannahomephotharam.com/@backend/reset_password/update', 'POST', '{"menu":"Admin","page":"ChangePassword","id":3,"action":"เปลี่ยนรหัสผ่นใหม่"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-20 07:14:28', '2021-12-20 07:14:28'),
(51, 3, '110.169.145.138', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":3,"action":"ออกจากระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-20 07:14:34', '2021-12-20 07:14:34'),
(52, 3, '110.169.145.138', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":3,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-20 07:14:46', '2021-12-20 07:14:46'),
(53, 3, '110.169.145.138', 'http://mannahomephotharam.com/@backend/logout', 'POST', '{"menu":"Admin authentication","page":"-","id":3,"action":"ออกจากระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-20 07:16:28', '2021-12-20 07:16:28'),
(54, 1, '49.237.23.141', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-20 11:28:32', '2021-12-20 11:28:32'),
(55, 12, '49.237.23.141', 'http://mannahomephotharam.com/@backend/news_manage/add', 'POST', '{"menu":"News","page":"Add news","id":12,"action":"เพิ่มข่าวใหม่"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-20 12:59:57', '2021-12-20 12:59:57'),
(56, 12, '49.237.23.141', 'http://mannahomephotharam.com/@backend/news_manage/edit', 'POST', '{"menu":"News","page":"Edit news","id":12,"action":"แก้ไขข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-20 13:02:55', '2021-12-20 13:02:55'),
(57, 12, '49.237.23.141', 'http://mannahomephotharam.com/@backend/news_manage/1/hide/12', 'GET', '{"menu":"News","page":"News Manage","id":12,"action":"ซ่อนข่าว"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-20 13:05:27', '2021-12-20 13:05:27'),
(58, 1, '49.228.101.119', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15', '2021-12-20 14:39:42', '2021-12-20 14:39:42'),
(59, 1, '110.169.145.138', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-20 15:48:10', '2021-12-20 15:48:10'),
(60, 1, '27.55.75.173', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-21 04:12:42', '2021-12-21 04:12:42'),
(61, 1, '184.82.197.61', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-21 09:10:19', '2021-12-21 09:10:19'),
(62, 1, '184.82.197.61', 'http://mannahomephotharam.com/@backend/home/divider_edit', 'POST', '{"menu":"Home","page":"Edit Home divider","action":"แก้ไขภาพเส้นแบ่งส่วน หน้า Home"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-21 09:17:18', '2021-12-21 09:17:18'),
(63, 1, '110.169.145.138', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-22 03:07:35', '2021-12-22 03:07:35'),
(64, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/login', 'POST', '{"menu":"Admin authentication","page_th":"Login","id":1,"action":"เข้าสู่ระบบ"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:17:04', '2021-12-22 07:17:04'),
(65, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/banner_edit', 'POST', '{"menu":"Home","page":"Edit Home Banner","action":"แก้ไข Banner หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:18:18', '2021-12-22 07:18:18'),
(66, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/banner_edit', 'POST', '{"menu":"Home","page":"Edit Home Banner","action":"แก้ไข Banner หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:19:03', '2021-12-22 07:19:03'),
(67, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/banner_edit', 'POST', '{"menu":"Home","page":"Edit Home Banner","action":"แก้ไข Banner หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:20:51', '2021-12-22 07:20:51'),
(68, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/banner_edit', 'POST', '{"menu":"Home","page":"Edit Home Banner","action":"แก้ไข Banner หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:21:06', '2021-12-22 07:21:06'),
(69, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/banner_edit', 'POST', '{"menu":"Home","page":"Edit Home Banner","action":"แก้ไข Banner หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:21:24', '2021-12-22 07:21:24'),
(70, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/banner_edit', 'POST', '{"menu":"Home","page":"Edit Home Banner","action":"แก้ไข Banner หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 07:22:49', '2021-12-22 07:22:49'),
(71, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/about_edit', 'POST', '{"menu":"Home","page":"Edit Home about","action":"แก้ไข about หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:04:55', '2021-12-22 08:04:55'),
(72, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/about_edit', 'POST', '{"menu":"Home","page":"Edit Home about","action":"แก้ไข about หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:05:29', '2021-12-22 08:05:29'),
(73, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/about_edit', 'POST', '{"menu":"Home","page":"Edit Home about","action":"แก้ไข about หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:05:54', '2021-12-22 08:05:54'),
(74, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/about_edit', 'POST', '{"menu":"Home","page":"Edit Home about","action":"แก้ไข about หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:06:40', '2021-12-22 08:06:40'),
(75, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/help_edit', 'POST', '{"menu":"Home","page":"Edit Home Help us","action":"แก้ไข Help us หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:08:14', '2021-12-22 08:08:14'),
(76, 1, '184.82.197.61', 'http://mannahomephotharam.com/@backend/home/about_edit', 'POST', '{"menu":"Home","page":"Edit Home about","action":"แก้ไข about หน้า Home"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', '2021-12-22 08:08:31', '2021-12-22 08:08:31'),
(77, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/help_edit', 'POST', '{"menu":"Home","page":"Edit Home Help us","action":"แก้ไข Help us หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:08:58', '2021-12-22 08:08:58'),
(78, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/divider_edit', 'POST', '{"menu":"Home","page":"Edit Home divider","action":"แก้ไขภาพเส้นแบ่งส่วน หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:10:01', '2021-12-22 08:10:01'),
(79, 1, '49.237.18.80', 'http://mannahomephotharam.com/@backend/home/divider_edit', 'POST', '{"menu":"Home","page":"Edit Home divider","action":"แก้ไขภาพเส้นแบ่งส่วน หน้า Home"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', '2021-12-22 08:10:24', '2021-12-22 08:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `id` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ชื่อธนาคาร',
  `code` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'รหัสย่อธนาคาร',
  `enable` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'เปิด-ปิดใช้งาน',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `name`, `code`, `enable`, `created_at`, `updated_at`) VALUES
(1, 'ธนาคารกรุงเทพ', 'BBL', 1, '2021-12-10 11:16:59', '2021-12-10 11:16:59'),
(2, 'ธนาคารกสิกรไทย', 'KBANK', 1, '2021-12-10 11:16:59', '2021-12-10 11:16:59'),
(3, 'ธนาคารกรุงไทย', 'KTB', 1, '2021-12-10 11:17:20', '2021-12-10 11:17:20'),
(4, 'ธนาคารทหารไทยธนชาต', 'TTB', 1, '2021-12-10 11:17:20', '2021-12-10 11:17:20'),
(5, 'ธนาคารไทยพาณิชย์', 'SCB', 1, '2021-12-10 11:17:41', '2021-12-10 11:17:41'),
(6, 'ธนาคารกรุงศรีอยุธยา', 'BAY', 1, '2021-12-10 11:17:41', '2021-12-10 11:17:41'),
(7, 'ธนาคารเกียรตินาคินภัทร', 'KKP', 1, '2021-12-10 11:18:02', '2021-12-10 11:18:02'),
(8, 'ธนาคารซีไอเอ็มบีไทย', 'CIMBT', 1, '2021-12-10 11:18:02', '2021-12-10 11:18:02'),
(9, 'ธนาคารทิสโก้', 'TISCO', 1, '2021-12-10 11:18:25', '2021-12-10 11:18:25'),
(10, 'ธนาคารยูโอบี', 'UOBT', 1, '2021-12-10 11:18:25', '2021-12-10 11:18:25'),
(11, 'ธนาคารไทยเครดิตเพื่อรายย่อย', 'TCD', 1, '2021-12-10 11:18:53', '2021-12-10 11:18:53'),
(12, 'ธนาคารแลนด์ แอนด์ เฮ้าส์', 'LHFG', 1, '2021-12-10 11:18:53', '2021-12-10 11:18:53'),
(13, 'ธนาคารไอซีบีซี (ไทย)', 'ICBCT', 1, '2021-12-10 11:19:15', '2021-12-10 11:19:15'),
(14, 'ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย', 'SME', 1, '2021-12-10 11:19:15', '2021-12-10 11:19:15'),
(15, 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร', 'BAAC', 1, '2021-12-10 11:19:36', '2021-12-10 11:19:36'),
(16, 'ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย', 'EXIM', 1, '2021-12-10 11:19:36', '2021-12-10 11:19:36'),
(17, 'ธนาคารออมสิน', 'GSB', 1, '2021-12-10 11:19:57', '2021-12-10 11:19:57'),
(18, 'ธนาคารอาคารสงเคราะห์', 'GHB', 1, '2021-12-10 11:19:57', '2021-12-10 11:19:57'),
(19, 'ธนาคารอิสลามแห่งประเทศไทย', 'ISBT', 1, '2021-12-10 11:20:30', '2021-12-10 11:20:30'),
(20, 'Other', 'OT', 1, '2021-12-10 11:20:30', '2021-12-10 11:20:30');

-- --------------------------------------------------------

--
-- Table structure for table `contact_submit`
--

CREATE TABLE IF NOT EXISTS `contact_submit` (
  `id` bigint(20) unsigned NOT NULL,
  `first_name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ชื่อ',
  `last_name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'นามสกุล',
  `telephone` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'เบอร์โทร',
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'อีเมล',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ข้อความ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_submit`
--

INSERT INTO `contact_submit` (`id`, `first_name`, `last_name`, `telephone`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'qrqer', 'qerqer', '0895603301', 'test@gmail.com', 'qerqerqe', '2021-12-21 10:53:28', '2021-12-21 10:53:28'),
(2, 'aefaeqf', 'afqefq', '0895603301', 'test@gmail.com', 'qefqegeqgeqg', '2021-12-21 11:02:40', '2021-12-21 11:02:40'),
(3, 'qefqe', 'fqefqef', '0895603301', 'test@gmail.com', 'qefqefqef', '2021-12-21 11:04:03', '2021-12-21 11:04:03'),
(4, 'ewqqer', 'qweqwe', '0895603301', 'test@gmail.com', 'qweqwe', '2021-12-21 11:07:56', '2021-12-21 11:07:56'),
(5, 'เนรมิตร', 'จันตา', '0895603301', 'test@gmail.com', 'rty35', '2021-12-21 11:08:38', '2021-12-21 11:08:38'),
(6, 'เนรมิตร', 'จันตา', '0895603301', 'test@gmail.com', '1331413r', '2021-12-21 11:12:03', '2021-12-21 11:12:03'),
(7, 'เนรมิตร', 'จันตา', '0895603301', 'test@gmail.com', 'qeqgqeg', '2021-12-21 11:19:05', '2021-12-21 11:19:05'),
(8, 'เนรมิตร', 'จันตา', '0895603301', 'godlovemit777@gmail.com', 'test massage', '2021-12-21 18:00:25', '2021-12-21 18:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` bigint(20) unsigned NOT NULL,
  `ref_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'อ้างอิงว่ามาจากหน้าไหน?',
  `title_th` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่อหัวข้อ TH',
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่อหัวข้อ EN (อาจได้ใช้)',
  `content_th` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหา TH',
  `content_en` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหา EN (อาจได้ใช้)',
  `image_name` text COLLATE utf8mb4_unicode_ci COMMENT 'ไฟล์ภาพ (banner, ภาพประกอบ ฯ)',
  `link_url` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหา กรณีเป็นลิงก์',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `ref_name`, `page`, `title_th`, `title_en`, `content_th`, `content_en`, `image_name`, `link_url`, `created_at`, `updated_at`) VALUES
(1, 'home_banner', 'home', NULL, 'WELCOME TO', NULL, 'Manna Home Photharam</br>\r\nfor Woman & Children</br>\r\nFoundation', 'home_banner_20211222142051489404.jpg', NULL, '2021-12-20 06:09:55', '2021-12-22 07:22:49'),
(2, 'about_data', 'home', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก มีความตั้งใจที่จะสร้างที่พักพิงให้กับเด็กที่ถูกทอดทิ้ง ถูกล่วงละเมิดทั้งทางร่างกาย จิตใจ เพศ หรือพ่อและแม่ไม่สามารถดูแลได้', 'The mission of Manna Home Photharam for Woman & Children Foundation is to build a shelter for children who are abused physically, emotionally and sexually or children whose parents are not able to take care of them.', 'home_about_1_20211222150640837960.jpg', 'home_about_2_20211222150831455970.jpg', '2021-12-20 06:15:42', '2021-12-22 08:08:31'),
(3, 'help_us_acc_eng', 'home', '078-2-33270-4', 'Bank name: TMBThanachart Bank', 'Bank Account name: Manna Home Photharam for Woman & Children Foundation', NULL, 'home_help_20211222150858440623.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:08:58'),
(4, 'help_us_acc_th', 'home', '078-2-33270-4', 'ธนาคารทหารไทยธนชาต', 'ชื่อบัญชี มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก', NULL, NULL, NULL, '2021-12-20 06:15:42', '2021-12-22 08:08:58'),
(5, 'image_divider', 'home', NULL, NULL, NULL, NULL, 'home_divider_20211222151024843508.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:10:24'),
(6, 'about_banner', 'about', NULL, NULL, NULL, NULL, 'about_banner_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL),
(7, 'news_banner', 'news', NULL, NULL, NULL, NULL, 'news_banner_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL),
(8, 'contact_banner', 'contact', NULL, NULL, NULL, NULL, 'contact_banner_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL),
(9, 'donation_banner', 'donation', NULL, NULL, NULL, NULL, 'donation_banner_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL),
(10, 'about_data_th', 'about', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็กก่อตั้งขึ้นในปี ค.ศ. 2007 โดยการระดมทุนและความร่วมมือจากองค์กรเอกชน ห้างร้าน อาสาสมัครจากต่างประเทศ และองค์การที่อยู่อาศัยเพื่อมนุษยชาติสากล (HABITAT FOR HUMANITY)<br></br>ซึ่งก่อสร้างและดำเนินกิจกรรมตามวัตถุประสงค์ของมูลนิธิฯ โดยมีความตั้งใจที่จะสร้างที่พักพิงให้กับเด็กที่ถูกทอดทิ้ง ถูกล่วงละเมิดทั้งทางร่างกาย จิตใจ เพศ หรือพ่อและแม่ไม่สามารถดูแลได้<br></br>บ้านมานาโพธารามตั้งอยู่บนเนื้อที่ 3 ไร่ มีอาคารที่พัก 3 หลัง มีบริเวณส่วนกลางสำหรับทำกิจกรรม ทานอาหารและสันทนาการ', NULL, 'about_img_1_211220130901.jpg', NULL, '2021-12-20 06:15:42', '2021-12-20 09:49:57'),
(11, 'about_data_eng', 'about', NULL, NULL, NULL, 'Manna Home Photharam for Woman & Children Foundation was established in 2007 with the cooperation from private corporates, volunteer from USA and Habitat for Humanity. The mission was to build a shelter for children who are abused physically, emotionally and sexually or children whose parents are not able to take care of them. Built on a 3-Rai property, Baan Manna Potharam has 3 main buildings. There are two dormitories and one main central building that include canteen, activity space, and recreational area.', 'about_img_2_211220130901.jpg', NULL, '2021-12-20 06:15:42', '2021-12-20 09:39:39'),
(12, 'what_we_do_th', 'about', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็กรับอุปการะเด็กที่มีอายุระหว่าง 4 ถึง 18 ปี โดยเด็กทุกคนจะได้รับการศึกษาที่โรงเรียนในอำเภอโพธาราม กิจกรรมที่ทำ เป็นประจำทุกวันรวมถึงการนมัสการร่วมกัน อธิษฐานเผื่อกัน และทุกวันอาทิตย์จะมีการนมัสการที่ ห้องประชุมมูลนิธิบ้านมานาฯ เรามีวิสัยทัศน์ที่จะสร้างคนดีสู่สังคม โดยให้เขาได้มีโอกาสรู้จักกับพระเจ้า พระผู้ช่วยให้รอด ให้มีการศึกษาเพื่อที่จะสามารถประกอบอาชีพเลี้ยงดูตนเองได้ และเป็นคนที่มีจริยธรรม ศีลธรรมอันดีงาม\r\n\r\n', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(13, 'what_we_do_eng', 'about', NULL, NULL, NULL, 'Baan Manna Potharam is devoted to take care of children from age 4 to 18 years old. Everyone is enrolled in schools in Potharam city. The daily chores while they stay here aside from homework and cooking include worshipping together and praying for each other. There is a worship service every Sunday. Our mission is to nurture these disadvantaged children with the best possible care so that they could grow into a responsible adult. Not only physical care, but we also nurture their spiritual well- being in hope that they would come to know the Lord Jesus Christ. Education is important, and we make sure that each of them gets the education he/she deserves. We hope that, as they grow into adulthood, they are ready for life, independent and able to earn a living on their own. We want to make a difference in these children’s lives, and we believe that they would become a blessing to the others.', NULL, NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(14, 'what_we_do_img_1', 'about', NULL, NULL, NULL, NULL, 'about_what_we_do_1_211220130901.png', NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(15, 'what_we_do_img_2', 'about', NULL, NULL, NULL, NULL, 'about_what_we_do_2_211220130901.png', NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(16, 'what_we_do_img_3', 'about', NULL, NULL, NULL, NULL, 'about_what_we_do_3_211220130901.png', NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(17, 'contact_email', 'contact', NULL, NULL, 'info@mannahomephotharam.com', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(18, 'contact_address', 'contact', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก</br>42 ม.10 ต.คลองตาคต อ.โพธาราม ราชบุรี 70120<br></br>Manna Home Photharam for Woman & Children Foundation</br>42 Moo 10 Klongtakod, Photharam Ratchburi 70120', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(19, 'contact_tel_1', 'contact', NULL, NULL, '66(0)-8954-22617', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(20, 'contact_tel_2', 'contact', NULL, NULL, '66(0)-8654-88762', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(21, 'contact_google_map', 'contact', NULL, NULL, '<iframe\r\n      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3876.5625408102987!2d99.8391601146534!3d13.68434379039111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e321d578a2d353%3A0xcc1730d96984cbeb!2z4Lih4Li54Lil4LiZ4Li04LiY4Li04Lia4LmJ4Liy4LiZ4Lih4Liy4LiZ4Liy4LmC4Lie4LiY4Liy4Lij4Liy4Lih4LmA4Lie4Li34LmI4Lit4Liq4LiV4Lij4Li14LmB4Lil4Liw4LmA4LiU4LmH4LiB!5e0!3m2!1sth!2sth!4v1638560222241!5m2!1sth!2sth"\r\n      width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy">\r\n    </iframe>', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(22, 'contact_massage_img', 'contact', NULL, NULL, NULL, NULL, 'contact_img_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `donate_submit`
--

CREATE TABLE IF NOT EXISTS `donate_submit` (
  `id` bigint(20) unsigned NOT NULL,
  `first_name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ชื่อ',
  `last_name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'นามสกุล',
  `email` text COLLATE utf8mb4_unicode_ci COMMENT 'อีเมล (กรณีถ้าต้องการเอกสาร)',
  `donate_for` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'บริจาคเพื่อ',
  `paid_by` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ชำระโดย; 0 = เงินสด, 1 = โอนผ่านธนาคาร',
  `bank_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'โอนผ่านธนาคาร อ้างอิงผ่าน ID',
  `bank_other` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ชื่อธนาคารอื่นๆ',
  `bank_branch` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'สาขาธนาคาร',
  `date` datetime NOT NULL COMMENT 'เวลาที่โอน',
  `money_amount` int(11) NOT NULL COMMENT 'จำนวนเงินที่โอน',
  `attache_file` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ไฟล์แนบ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donate_submit`
--

INSERT INTO `donate_submit` (`id`, `first_name`, `last_name`, `email`, `donate_for`, `paid_by`, `bank_id`, `bank_other`, `bank_branch`, `date`, `money_amount`, `attache_file`, `created_at`, `updated_at`) VALUES
(1, 'เนรมิตร', 'จันตา', 'test@gmail.com', 'rherherh', 1, 'GHB', '', 'test', '2021-12-01 01:27:00', 46356536, 'donate_221221093219.jpg', '2021-12-22 02:32:19', '2021-12-22 02:33:12');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files_upload`
--

CREATE TABLE IF NOT EXISTS `files_upload` (
  `id` bigint(20) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files_upload`
--

INSERT INTO `files_upload` (`id`, `description`, `file_name`, `enable`, `created_at`, `updated_at`) VALUES
(2, 'test file upload', 'file_171221174347.jpg', 1, '2021-12-17 10:43:47', '2021-12-17 10:43:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_12_08_091628_create_news_acts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_acts`
--

CREATE TABLE IF NOT EXISTS `news_acts` (
  `id` bigint(20) unsigned NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'หัวข้อข่าว',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหาข่าว',
  `author` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ผู้เขียน',
  `date` date DEFAULT NULL COMMENT 'ข่าววันที่',
  `cover` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ภาพปกข่าว',
  `gallery` text COLLATE utf8mb4_unicode_ci COMMENT 'ชื่อ folder gallery รูป',
  `views` bigint(20) DEFAULT NULL COMMENT 'ยอดวิว (เผื่อใช้)',
  `enable` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'เปิด-ปิดใช้งาน',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_acts`
--

INSERT INTO `news_acts` (`id`, `title`, `content`, `author`, `date`, `cover`, `gallery`, `views`, `enable`, `created_at`, `updated_at`) VALUES
(1, 'กิจกรรม ปลูกปักสวนครัว ปลอดสารพิษ และพืชฟอกอากาศ', '<p>บ้านมานาฯ เปิดรับการสนับสนุนองค์ความรู้เกี่ยวกับการปลูกพืชผักสวนครัว การผสมดิน การบำรุงดิน การเพาะชำต้นไม้ การกำจัดศัตรูพืช โดยทำฮอร์โมน และจุลินทรีย์จากธรรมชาติ ซึ่งองค์ความรู้ดังกล่าวที่เด็ก ๆ ได้เรียนรู้ และได้รับการสนับสนุนเมล็ดผัก ดิน ปุ๋ย มูลวัว อุปกรณ์ทางการเกษตร นอกจากจะนำมาประกอบอาหารเองแล้ว บ้านมานาฯ เชื่อว่า ทักษะเหล่านี้จะสามารถเป็นอาชีพเสริมและอาชีพหลักให้แก่เด็ก ๆ เมื่อเขาเหล่านั้นออกจากบ้านมานาฯไป\r\n</p>', 'Admin', '2021-12-08', 'news_cover_20211217084829525949.jpg', 'news_acts_20211208171100', 0, 1, '2021-12-08 09:58:25', '2021-12-17 01:48:29'),
(6, 'กิจกรรม ต้อนรับผู้มาเยี่ยมเยียน เลี้ยงอาหาร บริจาค และจัดกิจกรรมอื่น ๆ', '<p>บ้านมานาฯเปิดรับคณะผู้ที่สนใจจัดกิจกรรมจิตอาสาฯ / กิจกรรม CSR / กิจกรรม Walk Rally / กิจกรรมสันทนาการ / กิจกรรมกีฬา / กิจกรรมบรรยายให้ความรู้ด้านวิชาการและอื่น ๆ / การแสดงดนตรี จากกลุ่มนักเรียน นักศึกษา ห้างร้านบริษัท คณะบุคคล ศิลปิน / ดารา หรือผู้ที่สนใจ โดยจัดกิจกรรมแบบ New Normal, Social distance และคณะผู้จัดกิจกรรมต้องแสดงหลักฐานการฉีดวัคซีนครบ 2 เข็ม บ้านมานาฯมีห้องประชุมรองรับ 100 คน ระบบเครื่องเสียง เครื่องดนตรี ลานกิจกรรม ห้องครัว ลานจอดรถ เพื่ออำนวยความสะดวกแก่ทุก ๆ ท่าน\r\n</p>', 'Admin', '2021-12-08', 'news_cover_20211217084726506172.jpg', 'news_acts_20211208171101', 0, 1, '2021-12-08 10:02:59', '2021-12-17 01:47:53'),
(7, 'กิจกรรม วันสำคัญ และเทศกาล ต่าง ๆ เช่น วันพ่อ วันแม่ วันคริสต์มาส วันหยุดตามราชการ', NULL, 'Admin', '2021-12-08', 'news_cover_20211217084635799962.jpg', 'news_acts_20211208171102', 0, 1, '2021-12-08 10:04:13', '2021-12-17 01:46:35'),
(8, 'การฉีดวัคซีนโควิด 19 ตามคำสั่งของรัฐบาลผ่านโรงเรียน', 'เด็ก ๆบ้านมานาฯที่อายุ 12 ปีขึ้นไป ได้รับวัคซีน Pfizer ครบ 2 เข็มกว่า 90%แล้ว', 'Admin', '2021-12-08', 'news_cover_20211217084700520993.jpg', 'news_acts_20211208171103', 0, 1, '2021-12-08 10:04:13', '2021-12-17 01:47:00'),
(9, 'กิจกรรม การเรียนออนไลน์ที่บ้าน ตามคำสั่งของรัฐบาลที่กำหนดตามเขตพื้นที่ควบคุม', 'บ้านมานาฯรับบริจาคคอมพิวเตอร์/ไอแพด ทั้งเครื่องใหม่และเครื่องมือสองสภาพดีเพื่อใช้ในการเรียน การศึกษา ค้นคว้าตามอัธยาศัย และการพักผ่อนตามอัธยาศัย', 'Admin', '2021-12-08', 'news_cover_20211217084316234028.jpg', 'news_acts_20211208171105', 0, 1, '2021-12-08 10:04:41', '2021-12-17 01:43:16'),
(10, 'test title', '<p>test content&nbsp;<a href="http://mannahomephotharam.com/files_upload/files/file_171221174347.jpg" target="_blank">ลิงก์รูปภาพ</a></p><p><img src="http://mannahomephotharam.com/files_upload/files/file_171221174347.jpg" style="width: 50%;"><br></p>', 'test author', NULL, 'news_cover_20211217082816429364.jpg', '20211217082816429364', NULL, 0, '2021-12-17 01:28:16', '2021-12-17 11:30:42'),
(12, 'test test', NULL, 'test 1', NULL, 'news_cover_20211220195957102420.jpg', '20211220195957102420', NULL, 0, '2021-12-20 12:59:57', '2021-12-20 13:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admins_logs`
--
ALTER TABLE `admins_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_submit`
--
ALTER TABLE `contact_submit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contents_ref_name_unique` (`ref_name`);

--
-- Indexes for table `donate_submit`
--
ALTER TABLE `donate_submit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files_upload`
--
ALTER TABLE `files_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_acts`
--
ALTER TABLE `news_acts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admins_logs`
--
ALTER TABLE `admins_logs`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `contact_submit`
--
ALTER TABLE `contact_submit`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `donate_submit`
--
ALTER TABLE `donate_submit`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `files_upload`
--
ALTER TABLE `files_upload`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news_acts`
--
ALTER TABLE `news_acts`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
