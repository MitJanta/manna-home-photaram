<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref_name', 255)->unique();
            $table->string('page', 255)->comment('หน้าเพจ');
            $table->string('title_th', 255)->nullable()->comment('ชื่อหัวข้อ TH');
            $table->string('title_en', 255)->nullable()->comment('ชื่อหัวข้อ EN (อาจได้ใช้)');
            $table->text('content_th')->nullable()->comment('เนื้อหา TH');
            $table->text('content_en')->nullable()->comment('เนื้อหา EN (อาจได้ใช้)');
            $table->text('image_name')->nullable()->comment('ไฟล์ภาพ (banner, ภาพประกอบ ฯ)');
            $table->text('link_url')->nullable()->comment('เนื้อหา กรณีเป็นลิงก์');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
