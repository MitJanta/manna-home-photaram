<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonateSubmitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donate_submit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('first_name')->comment('ชื่อ');
            $table->text('last_name')->comment('นามสกุล');
            $table->text('donate_for')->comment('บริจาคเพื่อ');
            $table->tinyInteger('paid_by')->default(0)->comment('ชำระโดย; 0 = เงินสด, 1 = โอนผ่านธนาคาร');
            $table->string('bank_id', 255)->comment('โอนผ่านธนาคาร อ้างอิงผ่าน ID');
            $table->text('bank_other')->comment('ชื่อธนาคารอื่นๆ');
            $table->text('bank_branch')->comment('สาขาธนาคาร');
            $table->dateTime('date')->comment('เวลาที่โอน');
            $table->integer('money_amount')->comment('จำนวนเงินที่โอน');
            $table->text('attache_file')->comment('ไฟล์แนบ');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donate_submit');
    }
}
