<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMarkAsReadAndSentMailToDonateSubmitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donate_submit', function (Blueprint $table) {
            $table->tinyInteger('mark_as_read')->after('attache_file')->default(0)->comment('ระบุว่าอ่านข้อมูลหรือยัง 0 = ยัง, 1 = อ่านแล้ว');
            $table->tinyInteger('sent_mail')->after('mark_as_read')->default(0)->comment('ระบุว่าส่งเมลกลับหรือยัง 0 = ยัง, 1 = ส่งแล้ว');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donate_submit', function (Blueprint $table) {
            //
        });
    }
}
