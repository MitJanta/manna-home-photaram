<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailToDonateSubmitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donate_submit', function (Blueprint $table) {
            $table->text('email')->after('last_name')->comment('อีเมล (กรณีต้องการเอกสาร)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donate_submit', function (Blueprint $table) {
            //
        });
    }
}
