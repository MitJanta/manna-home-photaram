<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('permission_1')->default(0)->comment('สิทธิ์สร้าง SubAdmin ใหม่');
            $table->boolean('permission_2')->default(0)->comment('สิทธิ์จัดการข้อมูลทั่วไป');
            $table->boolean('permission_3')->default(0)->comment('สิทธิ์จัดการข่าวและกิจกรรม');
            $table->boolean('permission_4')->default(0)->comment('สิทธิ์เข้าถึงข้อความติดต่อและบริจาค');
            $table->boolean('type')->default(0)->comment('1 = SuperAdmin, 0 = SubAdmin (default)');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
