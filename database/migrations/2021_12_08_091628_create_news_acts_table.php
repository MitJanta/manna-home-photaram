<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsActsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_acts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title')->comment('หัวข้อข่าว');
            $table->text('content')->nullable()->comment('เนื้อหาข่าว');
            $table->text('author')->defualt('Admin')->comment('ผู้เขียน');
            $table->date('date')->nullable()->comment('ข่าววันที่');
            $table->text('cover')->comment('ภาพปกข่าว');
            $table->text('gallery')->nullable()->comment('ชื่อ folder gallery รูป');
            $table->bigInteger('views')->nullable()->comment('ยอดวิว (เผื่อใช้)');
            $table->tinyInteger('enable')->default(1)->comment('เปิด-ปิดใช้งาน');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_acts');
    }
}
