-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2021 at 02:33 PM
-- Server version: 5.7.33
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mannah_website`
--

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` bigint(20) unsigned NOT NULL,
  `ref_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'อ้างอิงว่ามาจากหน้าไหน?',
  `title_th` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่อหัวข้อ TH',
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่อหัวข้อ EN (อาจได้ใช้)',
  `content_th` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหา TH',
  `content_en` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหา EN (อาจได้ใช้)',
  `image_name` text COLLATE utf8mb4_unicode_ci COMMENT 'ไฟล์ภาพ (banner, ภาพประกอบ ฯ)',
  `link_url` text COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อหา กรณีเป็นลิงก์',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `ref_name`, `page`, `title_th`, `title_en`, `content_th`, `content_en`, `image_name`, `link_url`, `created_at`, `updated_at`) VALUES
(1, 'home_banner', 'home', NULL, 'WELCOME TO', NULL, 'Manna Home Photharam</br>\r\nfor Woman & Children</br>\r\nFoundation', 'home_banner_20211222142051489404.jpg', NULL, '2021-12-20 06:09:55', '2021-12-22 07:22:49'),
(2, 'about_data', 'home', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก มีความตั้งใจที่จะสร้างที่พักพิงให้กับเด็กที่ถูกทอดทิ้ง ถูกล่วงละเมิดทั้งทางร่างกาย จิตใจ เพศ หรือพ่อและแม่ไม่สามารถดูแลได้', 'The mission of Manna Home Photharam for Woman & Children Foundation is to build a shelter for children who are abused physically, emotionally and sexually or children whose parents are not able to take care of them.', 'home_about_1_20211222150640837960.jpg', 'home_about_2_20211222150831455970.jpg', '2021-12-20 06:15:42', '2021-12-22 08:08:31'),
(3, 'help_us_acc_eng', 'home', '078-2-33270-4', 'Bank name: TMBThanachart Bank', 'Bank Account name: Manna Home Photharam for Woman & Children Foundation', NULL, 'home_help_20211222150858440623.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:08:58'),
(4, 'help_us_acc_th', 'home', '078-2-33270-4', 'ธนาคารทหารไทยธนชาต', 'ชื่อบัญชี มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก', NULL, NULL, NULL, '2021-12-20 06:15:42', '2021-12-22 08:08:58'),
(5, 'image_divider', 'home', NULL, NULL, NULL, NULL, 'home_divider_20211222151024843508.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:10:24'),
(6, 'about_banner', 'about', NULL, NULL, NULL, NULL, 'about_banner_20211222154326548615.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:43:26'),
(7, 'news_banner', 'news', NULL, NULL, NULL, NULL, 'news_banner_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL),
(8, 'contact_banner', 'contact', NULL, NULL, NULL, NULL, 'contact_banner_20211222154624664525.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:46:24'),
(9, 'donation_banner', 'donation', NULL, NULL, NULL, NULL, 'donation_banner_20211222154913094379.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 08:49:13'),
(10, 'about_data_th', 'about', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็กก่อตั้งขึ้นในปี ค.ศ. 2007 โดยการระดมทุนและความร่วมมือจากองค์กรเอกชน ห้างร้าน อาสาสมัครจากต่างประเทศ และองค์การที่อยู่อาศัยเพื่อมนุษยชาติสากล (HABITAT FOR HUMANITY)<br></br>ซึ่งก่อสร้างและดำเนินกิจกรรมตามวัตถุประสงค์ของมูลนิธิฯ โดยมีความตั้งใจที่จะสร้างที่พักพิงให้กับเด็กที่ถูกทอดทิ้ง ถูกล่วงละเมิดทั้งทางร่างกาย จิตใจ เพศ หรือพ่อและแม่ไม่สามารถดูแลได้<br></br>บ้านมานาโพธารามตั้งอยู่บนเนื้อที่ 3 ไร่ มีอาคารที่พัก 3 หลัง มีบริเวณส่วนกลางสำหรับทำกิจกรรม ทานอาหารและสันทนาการ', NULL, 'about_img_1_211220130901.jpg', NULL, '2021-12-20 06:15:42', '2021-12-20 09:49:57'),
(11, 'about_data_eng', 'about', NULL, NULL, NULL, 'Manna Home Photharam for Woman & Children Foundation was established in 2007 with the cooperation from private corporates, volunteer from USA and Habitat for Humanity. The mission was to build a shelter for children who are abused physically, emotionally and sexually or children whose parents are not able to take care of them. Built on a 3-Rai property, Baan Manna Potharam has 3 main buildings. There are two dormitories and one main central building that include canteen, activity space, and recreational area.', 'about_img_2_211220130901.jpg', NULL, '2021-12-20 06:15:42', '2021-12-20 09:39:39'),
(12, 'what_we_do_th', 'about', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็กรับอุปการะเด็กที่มีอายุระหว่าง 4 ถึง 18 ปี โดยเด็กทุกคนจะได้รับการศึกษาที่โรงเรียนในอำเภอโพธาราม กิจกรรมที่ทำ เป็นประจำทุกวันรวมถึงการนมัสการร่วมกัน อธิษฐานเผื่อกัน และทุกวันอาทิตย์จะมีการนมัสการที่ ห้องประชุมมูลนิธิบ้านมานาฯ เรามีวิสัยทัศน์ที่จะสร้างคนดีสู่สังคม โดยให้เขาได้มีโอกาสรู้จักกับพระเจ้า พระผู้ช่วยให้รอด ให้มีการศึกษาเพื่อที่จะสามารถประกอบอาชีพเลี้ยงดูตนเองได้ และเป็นคนที่มีจริยธรรม ศีลธรรมอันดีงาม\r\n\r\n', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(13, 'what_we_do_eng', 'about', NULL, NULL, NULL, 'Baan Manna Potharam is devoted to take care of children from age 4 to 18 years old. Everyone is enrolled in schools in Potharam city. The daily chores while they stay here aside from homework and cooking include worshipping together and praying for each other. There is a worship service every Sunday. Our mission is to nurture these disadvantaged children with the best possible care so that they could grow into a responsible adult. Not only physical care, but we also nurture their spiritual well- being in hope that they would come to know the Lord Jesus Christ. Education is important, and we make sure that each of them gets the education he/she deserves. We hope that, as they grow into adulthood, they are ready for life, independent and able to earn a living on their own. We want to make a difference in these children’s lives, and we believe that they would become a blessing to the others.', NULL, NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(14, 'what_we_do_img_1', 'about', NULL, NULL, NULL, NULL, 'about_what_we_do_1_211220130901.png', NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(15, 'what_we_do_img_2', 'about', NULL, NULL, NULL, NULL, 'about_what_we_do_2_211220130901.png', NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(16, 'what_we_do_img_3', 'about', NULL, NULL, NULL, NULL, 'about_what_we_do_3_211220130901.png', NULL, '2021-12-20 06:15:42', '2021-12-20 09:51:27'),
(17, 'contact_email', 'contact', NULL, NULL, 'info@mannahomephotharam.com', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(18, 'contact_address', 'contact', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก</br>42 ม.10 ต.คลองตาคต อ.โพธาราม ราชบุรี 70120<br></br>Manna Home Photharam for Woman & Children Foundation</br>42 Moo 10 Klongtakod, Photharam Ratchburi 70120', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(19, 'contact_tel_1', 'contact', NULL, NULL, '66(0)-8954-22617', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(20, 'contact_tel_2', 'contact', NULL, NULL, '66(0)-8654-88762', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(21, 'contact_google_map', 'contact', NULL, NULL, '<iframe\r\n      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3876.5625408102987!2d99.8391601146534!3d13.68434379039111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e321d578a2d353%3A0xcc1730d96984cbeb!2z4Lih4Li54Lil4LiZ4Li04LiY4Li04Lia4LmJ4Liy4LiZ4Lih4Liy4LiZ4Liy4LmC4Lie4LiY4Liy4Lij4Liy4Lih4LmA4Lie4Li34LmI4Lit4Liq4LiV4Lij4Li14LmB4Lil4Liw4LmA4LiU4LmH4LiB!5e0!3m2!1sth!2sth!4v1638560222241!5m2!1sth!2sth"\r\n      width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy">\r\n    </iframe>', NULL, NULL, NULL, '2021-12-20 06:15:42', NULL),
(22, 'contact_massage_img', 'contact', NULL, NULL, NULL, NULL, 'contact_img_211220130901.jpg', NULL, '2021-12-20 06:15:42', NULL),
(23, 'footer_data', 'global', '078-2-33270-4', 'https://www.facebook.com/mannahomethai/', NULL, NULL, 'footer_bank_logo_211220130901.jpg', NULL, '2021-12-20 06:15:42', '2021-12-22 12:22:14'),
(24, 'how_you_can_help', 'donation', NULL, NULL, 'มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็กดำเนินกิจการด้วยเงินถวาย เงินบริจาคจากผู้ที่มีภาระใจอยากช่วยเหลือเด็กเหล่านี้ เพื่อสร้างให้พวกเขามีอนาคต มีการศึกษาและสามารถช่วยเหลือตัวเองได้ต่อไป หากท่านมีภาระใจที่จะมีส่วนร่วมกับพีนธกิจของมูลนิธิ สามารถบริจาคเงินโดย', 'Baan Manna Potharam funds come from the gifts given by those who want to be part of our mission – to help these children get access to good education, home, and food so that they can grow into a responsible Christ-like adult. If you want to be a part of our mission, you can donate through these channels:', NULL, NULL, '2021-12-20 06:15:42', NULL),
(25, 'acc_data_th', 'donation', '078-2-33270-4', 'ธนาคารทหารไทยธนชาต</br>\r\nชื่อบัญชี มูลนิธิบ้านมานาโพธารามเพื่อสตรีและเด็ก', 'เมื่อทำการโอนเงินแล้วกรุณาส่งสลิปเพื่อขอรับใบเสร็จได้ทางอีเมล</br>\r\ninfo@mannahomephotharam.com', NULL, 'donation_acc_1_desktop_211220130901.jpg', 'donation_acc_1_mobile_211220130901.jpg', '2021-12-20 06:15:42', '2021-12-22 12:25:15'),
(26, 'acc_data_en', 'donation', '078-2-33270-4', 'Bank name: TMBThanachart Bank', 'Bank Account name: Manna Home Photharam for Woman & Children Foundation', NULL, 'donation_acc_2_desktop_211220130901.jpg', 'donation_acc_2_mobile_211220130901.jpg', '2021-12-20 06:15:42', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contents_ref_name_unique` (`ref_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
